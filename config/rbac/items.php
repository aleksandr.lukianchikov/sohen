<?php

return [
    /** Действия */
    'site/logout' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Выход из системы',
    ],

    '/user/reset'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Если забыл пароль, то переход на форму для ввода имейла',
    ],

    'site/error' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Страница ошибки',
    ],

    'site/about' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Страница о нас',
    ],

    'site/contact' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Наши контактные данные',
    ],
    'site/set-locale' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Изменение языка',
    ],

    'default/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Список продюсеров',
    ],
    'product/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Список товаров продюсера',
    ],
    'order/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр ордеров',
    ],

    'order/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр ордера',
    ],
    'cart/add' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Добавление в корзину',
    ],
    'cart/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр корзины',
    ],
    'cart/delete' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление из  корзины',
    ],

    'cart/repeat' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Занесение в корзину товаров из конкретного заказа из истории ',
    ],
    'orderproduct/create' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Добавление к корзине акций и показ пользователю окончательный набор к оплате',
    ],


    'orderproduct/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр продуктов из корзины из истории',
    ],


    'orderproduct/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр продуктов из корзин из истории',
    ],

    'orderproduct/delete' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр продуктов из корзин из истории',
    ],

    'orderproduct/pay' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Создание счета и занесение в бд его и продуктов и скидок',
    ],

    'orderproduct/payform' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Предоставление юзеру формы оплаты',
    ],
    'orderproduct/send' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Отправка админу  смс-сообщения о том что получен новый заказ',
    ],

    'orderdiscount/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр скидок из конкретной корзины из истории',
    ],


    'orderdiscount/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр скидок из корзин из истории',
    ],


    'productreturn/create' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Возврат продукта',
    ],

    'productreturn/upload-file' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Загрузка фото возвращаемого продукта',
    ],

    'productreturn/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр заявки на возвращаемый продукт',
    ],

    'productreturn/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр заявок на возврат продукта',
    ],
    'product/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр продукта',
    ],

    'discount/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр акции',
    ],

    'discount/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр акций',
    ],

    'private-office/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Личный кабинет покупателя',
    ],


    'user/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр регистрационных данных',
    ],


    'user/update' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Изменение регистрационных данных юзером',
    ],

    'producer/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Информация о пользователе',
    ],

    'producer/send-email' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'форма для отправки сообщения производителю',
    ],

    'producer/contacts' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Контакты производителя',
    ],

    /** Действия администратора */

    'admin/site/logout' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => '"Выход" для администраторов',
    ],
    'admin/site/error' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Страница ошибки',
    ],

    'admin/site/about' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Страница о нас для админов',
    ],

    'admin/site/contact' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Наши контактные данные для админов',
    ],


    'admin/default/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Кабинет  администратора',
    ],

    'admin/product/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Список товаров',
    ],
    'admin/producer/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Список продюсеров',
    ],
    'admin/producer/create' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Создание нового производителя',
    ],
    'admin/producer/delete' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление производителя',
    ],

    'admin/producer/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр производителя',
    ],

    'admin/producer/update' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование производителя',
    ],

    'admin/producer/upload-file' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Загрузка фотографий производителя',
    ],

    'admin/producer/delete-file' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление фотографий производителя',
    ],


    'admin/product/create' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Добавление продукта',
    ],

    'admin/product/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр продукта',
    ],
    'admin/product/update' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование продукта',
    ],
    'admin/product/delete' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление продукта',
    ],

    'admin/product/upload-file' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Загрузка фотографий продукта',
    ],

    'admin/product/delete-file' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление фотографий продукта',
    ],

    'admin/discount/create' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Добавление акции',
    ],


    'admin/discount/image' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Добавление просто картинки в слайдер ',
    ],
    'admin/discount/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр акции',
    ],
    'admin/discount/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр акций',
    ],
    'admin/discount/actuali' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр актуальных акций',
    ],

    'admin/discount/update' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование акции',
    ],

    'admin/discount/update_img' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование акции типа только картинка',
    ],

    'admin/discount/view_img' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Отображение акции типа только картинка',
    ],

    'admin/discount/upload-file' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Загрузка баннера акции',
    ],

    'admin/discount/delete-file' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление баннера акции',
    ],

    'admin/discount/delete' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление акции',
    ],

    'admin/storageconditions/create' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Создание нового условия хранения',
    ],

    'admin/storageconditions/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр  условий хранения',
    ],

    'admin/storageconditions/update' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование условий хранения',
    ],
    'admin/storageconditions/delete' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление условия хранения',
    ],

    'admin/reason/create' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Создание новой причины возврата',
    ],

    'admin/reason/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр причины возврата',
    ],

    'admin/reason/update' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование причин возврата',
    ],
    'admin/reason/delete' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление причин возврата',
    ],

    'admin/packtype/create' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Создание нового типа упаковки',
    ],

    'admin/packtype/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр типа упаковки',
    ],

    'admin/packtype/update' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование типа упаковки',
    ],
    'admin/packtype/delete' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление типа упаковки',
    ],

    'admin/producttype/create' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Создание нового типа упаковки',
    ],

    'admin/producttype/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр типа упаковки',
    ],

    'admin/producttype/update' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование типа упаковки',
    ],
    'admin/producttype/delete' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление типа упаковки',
    ],

    'admin/producttype/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление типа упаковки',
    ],


    'admin/productreturn/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр заявки на возвращаемый продукт',
    ],

    'admin/productreturn/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр заявок на возврат продукта',
    ],

    'admin/productreturn/update' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование заявок на возврат продукта',
    ],

    'admin/compensation/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр заявок на возврат продукта',
    ],


    'admin/compensation/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр заявок на возврат продукта',
    ],


    'admin/compensation/update' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование заявок на возврат продукта',
    ],


    'admin/order/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр ордера',
    ],


    'admin/order/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр ордеров',
    ],


    'admin/orderproduct/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр продуктов из корзины из истории',
    ],


    'admin/orderproduct/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр продуктов из корзин из истории',
    ],


    'admin/orderdiscount/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр скидок из конкретной корзины из истории',
    ],


    'admin/orderdiscount/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр скидок из корзин из истории',
    ],


    'admin/notificationday/index' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр дней недели рассылок для покупателей',
    ],


    'admin/notificationday/view' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр дней недели рассылок конкретного покупателя',
    ],


    'admin/notificationday/update' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование дней недели рассылок конкретного покупателя',
    ],


    'admin/notificationday/delete' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление  конкретного покупателя из рассылок',
    ],

    'admin/user/index' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр  покупателей',
    ],

    'admin/user/view' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр  покупателя',
    ],
    'admin/user/viewme' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр  админом своего профиля',
    ],

    'admin/user/updateme' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Внесение изменений  админом в свой профиль',
    ],
    'admin/user/update' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Внесение изменений  админом в некоторое данные юзера',
    ],
    'admin/user/register' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'форма для админа для внесения своих личных данных',
    ],

    'admin/notificationtext/view' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр текста сообщения',
    ],
    'admin/notificationtext/index' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр текстов сообщений',
    ],

    'admin/notificationtext/create' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Создание нового текста сообщения',
    ],

    'admin/notificationtext/update' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование текста сообщения',
    ],

    'admin/notificationtext/delete' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление текста сообщения',
    ],

    /** Действия супер-администратора */

    'superadmin/site/about' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'пока просто так',
    ],

    'superadmin/default/view' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Кабинет супер-админа',
    ],


    'superadmin/site/contact' =>[
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'пока не понятно для чего супер-админа',
    ],
    'superadmin/site/logout' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => '"Выход" для администраторов',
    ],
    'superadmin/site/error' => [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Страница ошибки',
    ],

    'superadmin/producer/index'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Все производители',
    ],
    'superadmin/producer/create'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Добавить нового производителя',
    ],
    'superadmin/producer/update'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактировать данные производителя',
    ],
    'superadmin/producer/view'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Данные конкретного производителя',
    ],
    'superadmin/producer/delete'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удалить конкретного производителя',
    ],
    'superadmin/producer/delete-file'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление картинки файла по производителю',
    ],
    'superadmin/producer/upload-file'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Загрузка файла картинки по производителю',
    ],
    'superadmin/storageconditions/create'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Добавление нового условия хранения продуктов',
    ],
    'superadmin/storageconditions/update'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование конкретного условия хранения продуктов',
    ],
    'superadmin/storageconditions/view'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр конкретного условия хранения продуктов',
    ],
    'superadmin/storageconditions/delete'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление конкретного условия хранения продуктов',
    ],
    'superadmin/storageconditions/index'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр всех условий хранения продуктов',
    ],

    'superadmin/reason/create'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Добавление новой причины возврата',
    ],
    'superadmin/reason/update'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование причины возврата',
    ],
    'superadmin/reason/view'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр конкретной причины возврата',
    ],
    'superadmin/reason/index'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр всех причин возврата',
    ],
    'superadmin/reason/delete'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление причины возврата',
    ],
    'superadmin/packtype/create'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Создание нового типа упаковки',
    ],
    'superadmin/packtype/update'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование типа упаковки',
    ],
    'superadmin/packtype/view'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр типа упаковки',
    ],
    'superadmin/packtype/delete'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление типа упаковки',
    ],
    'superadmin/packtype/index'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр всех типов упаковки',
    ],


    'superadmin/user/view'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр конкретного покупателя',
    ],
    'superadmin/user/index'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Просмотр всех покупателей',
    ],
    'superadmin/user/update'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Редактирование покупателей',
    ],
    'superadmin/user/create'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Добавление нового покупателя',
    ],
    'superadmin/user/delete'=> [
        'type' => \yii\rbac\Item::TYPE_PERMISSION,
        'description' => 'Удаление покупателя',
    ],

    /** Роли */
    'user' => [
        'description' => 'Пользователь',
        'type' => \yii\rbac\Item::TYPE_ROLE,
        'children' => [
            'site/logout',
            'site/error',
            'site/about',
            'site/contact',
            'site/set-locale',
            '/user/reset',
        ],
    ],

    'customer' => [
        'description' => 'Cупер администратор',
        'type' => \yii\rbac\Item::TYPE_ROLE,
        'children' => [
            'user',
            'product/index',
            'default/index',
            'order/index',
            'order/create',
            'order/view',
            'cart/add',
            'cart/index',
            'cart/delete',
            'cart/repeat',
            'orderproduct/create',
            'orderproduct/index',
            'orderproduct/delete',
            'orderproduct/view',
            'orderdiscount/create',
            'orderdiscount/index',
            'orderdiscount/view',
            'orderproduct/pay',
            'orderproduct/payform',
            'productreturn/create',
            'product/view',
            'productreturn/create',
            'discount/index',
            'discount/view',
            'private-office/view',
            'productreturn/upload-file',
            'productreturn/view',
            'user/view',
            'user/update',
            'productreturn/index',
            'compensation/view',
            'compensation/index',
            'compensation/create',
            'site/about',
            'site/contact',
            'producer/view',
            'producer/send-email',
            'producer/contacts',
            'orderproduct/send',




        ]
    ],

    'producer_admin' => [
        'description' => 'Администратор продюсера',
        'type' => \yii\rbac\Item::TYPE_ROLE,
        'children' => [
            'user',
            'admin/product/index',
            'admin/product/create',
            'admin/product/view',
            'admin/product/update',
            'admin/product/delete',
            'admin/discount/create',
            'admin/discount/index',
            'admin/discount/actuali',
            'admin/discount/view',
            'admin/discount/update',
            'admin/discount/view_img',
            'admin/discount/update_img',
            'admin/discount/upload-file',
            'admin/discount/delete-file',
            'admin/discount/delete',
            'admin/discount/image',
            'admin/product/upload-file',
            'admin/product/delete-file',
            'admin/default/view',
            'admin/producttype/create',
            'admin/producttype/update',
            'admin/producttype/view',
            'admin/producttype/index',
            'admin/producttype/delete',
            'admin/productreturn/index',
            'admin/productreturn/view',
            'admin/productreturn/update',
            'admin/compensation/view',
            'admin/compensation/index',
            'admin/compensation/update',
            'admin/order/index',
            'admin/order/view',
            'admin/orderproduct/index',
            'admin/orderproduct/view',
            'admin/notificationday/view',
            'admin/notificationday/index',
            'admin/notificationday/update',
            'admin/notificationday/delete',
            'admin/user/view',
            'admin/user/index',
            'admin/user/viewme',
            'admin/user/updateme',
            'admin/user/update',
            'admin/user/register',
            'admin/site/contact',
            'admin/site/about',
            'admin/site/error',
            'admin/site/logout',
            'admin/notificationtext/delete',
            'admin/notificationtext/update',
            'admin/notificationtext/index',
            'admin/notificationtext/create',
            'admin/notificationtext/view',

        ]
    ],

    'super_admin' => [
        'description' => 'Cупер администратор',
        'type' => \yii\rbac\Item::TYPE_ROLE,
        'children' => [
            'user',
            'customer',
            'producer_admin',
            'superadmin/producer/index',
            'superadmin/producer/create',
            'superadmin/producer/update',
            'superadmin/producer/view',
            'superadmin/producer/delete',
            'superadmin/producer/delete-file',
            'superadmin/producer/upload-file',
            'superadmin/storageconditions/create',
            'superadmin/storageconditions/update',
            'superadmin/storageconditions/view',
            'superadmin/storageconditions/delete',
            'superadmin/storageconditions/index',
            'superadmin/reason/create',
            'superadmin/reason/update',
            'superadmin/reason/view',
            'superadmin/reason/index',
            'superadmin/reason/delete',
            'superadmin/packtype/create',
            'superadmin/packtype/update',
            'superadmin/packtype/view',
            'superadmin/packtype/delete',
            'superadmin/packtype/index',
            'superadmin/default/view',
            'superadmin/site/contact',
            'superadmin/site/error',
            'superadmin/site/logout',
            'superadmin/site/about',
            'superadmin/user/view',
            'superadmin/user/index',
            'superadmin/user/update',
            'superadmin/user/create',
            'superadmin/user/delete',

        ]
    ],
];
