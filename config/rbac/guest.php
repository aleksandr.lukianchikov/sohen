<?php
return [
    'site/login',
    'user/auth',
    'site/error',
    'user/register',
    'user/reset',
    'user/set-password',
    'site/set-locale'
];