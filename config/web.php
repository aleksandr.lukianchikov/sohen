<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

Yii::setAlias('@uploads', realpath(__DIR__ . '/../web/uploads/') . '/');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'defaultRoute' => 'default/index',
    //'language' => 'ru-RU',
    'as locale' => [
        'class' => 'app\behaviors\LocaleBehavior',
        'enablePreferredLanguage' => true
    ],
    'sourceLanguage' => 'ru-RU',
    'timezone' => 'Asia/Jerusalem',
    'bootstrap' => ['log'],
    'on beforeRequest' => function () {
        if (Yii::$app->user->identity) {
            $rights = Yii::$app->user->identity->getAccessRightsAsArray();
            if (in_array(\app\models\User::ROLE_CUSTOMER, $rights)) {
                Yii::$app->setHomeUrl(\yii\helpers\Url::to(['/default/index']));
            } elseif (in_array(\app\models\User::ROLE_PRODUCER_ADMIN, $rights)) {
                Yii::$app->setHomeUrl(\yii\helpers\Url::to(['/admin/default/view']));
            }elseif (in_array(\app\models\User::SUPER_ADMIN, $rights)) {
                Yii::$app->setHomeUrl(\yii\helpers\Url::to(['/superadmin/default/view']));
                Yii::$app->errorHandler->errorAction = 'superadmin/site/error';
            }
        }
    },
    'components' => [
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
            'bundles' => [
                // Мы регистрируем это специально,
                // потому что JuiAsset (из виджета UploadAsset) добавляют стили которые перекрывают dateTimePicker и ломают его
                // А таким образом мы сначала загружаем стили JuiAsset а потом dateTimePicker минуя перекрытие
                'dosamigos\datepicker\DatePickerAsset' => [
                    'depends' => [
                        '\yii\jui\JuiAsset',
                        'yii\bootstrap\BootstrapPluginAsset',
                    ],
                ]
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'o75CD_nqfB5atdLXCEvuwfeFbhLzsbox',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            //'useFileTransport' => true,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.sohen.co.il',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                //'username' => '_mainaccount@sohen.co.il',
                'username' => 'noreply@sohen.co.il',
                'password' => 'YNvp1UQ,(LyD',
                'port' => '587', // Port 25 is a very common port too
                //'encryption' => 'tls', // It is often used, check your provider or mail server specs
                'timeout' => 2000
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info', 'error'],
                    'logVars' => [],
                    'logFile' => '@runtime/logs/my.log',
                    'categories' => ['app.*'],
                ],
            ],
        ],
        'db' => $db,
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        'authManager' => [
            'class' => 'app\components\AuthManager',
            'itemFile' => '@app/config/rbac/items.php',
            'assignmentFile' => '@runtime/rbac/assignments.php',
            'ruleFile' => '@runtime/rbac/rules.php',
            'guestFile' => '@app/config/rbac/guest.php',
        ],

        'fileStorage' => [
            'class' => 'trntv\filekit\Storage',
            'baseUrl' => '@web/uploads',
            'filesystem' => function () {
                $adapter = new \League\Flysystem\Adapter\Local(__DIR__ . '/../web/uploads');
                return new League\Flysystem\Filesystem($adapter);
            }
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],

        'superadmin' => [
            'class' => 'app\modules\superadmin\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.102'],
    ];
}

/*
if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
    $config['modules']['debug']['allowedIPs'] = ['*'];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}
*/

return $config;
