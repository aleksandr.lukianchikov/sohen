<?php

return [
    'adminEmail' => 'bramnik@hotmail.com',
    'noreplyEmail' => 'noreply@sohen.co.il',
    'availableLocales' => [
        'ru-RU' => 'Русский',
        'en-US' => 'English',
        'he-IL' => 'עברית',
    ],
    'gateway_data' => [
        'TranzilaPW' => 'gagarin197',
        'cred_type' => '1',
        'currency' => '1',
        'supplier' => 'test233',
        'tranzila_api_path' => 'https://secure5.tranzila.com/cgi-bin/tranzila71u.cgi',
    ],
];
