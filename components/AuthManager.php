<?php
namespace app\components;

use yii\db\Query;
use yii\helpers\Json;
use yii\rbac\Assignment;
use yii\rbac\Item;
use yii\rbac\Permission;
use yii\rbac\PhpManager;
use yii\rbac\Role;

class AuthManager extends PhpManager
{
    public $guestFile;
    protected $guestActions = [];

    protected function load()
    {
        $this->children = [];
        $this->rules = [];
        $this->assignments = [];
        $this->items = [];
        $this->guestActions = $this->loadFromFile(\Yii::getAlias($this->guestFile));
        $items = $this->loadFromFile($this->itemFile);
        $itemsMtime = @filemtime($this->itemFile);
        $assignments = $this->loadAssignments();
        $rules = $this->loadFromFile($this->ruleFile);

        foreach ($items as $name => $item) {
            $class = $item['type'] == Item::TYPE_PERMISSION ? Permission::className() : Role::className();

            $this->items[$name] = new $class(
                [
                    'name' => $name,
                    'description' => isset($item['description']) ? $item['description'] : null,
                    'ruleName' => isset($item['ruleName']) ? $item['ruleName'] : null,
                    'data' => isset($item['data']) ? $item['data'] : null,
                    'createdAt' => $itemsMtime,
                    'updatedAt' => $itemsMtime,
                ]
            );
        }

        foreach ($items as $name => $item) {
            if (isset($item['children'])) {
                foreach ($item['children'] as $childName) {
                    if (isset($this->items[$childName])) {
                        $this->children[$name][$childName] = $this->items[$childName];
                    }
                }
            }
        }

        foreach ($assignments as $userId => $roles) {
            foreach ($roles as $role) {
                $this->assignments[$userId][$role] = new Assignment(
                    [
                        'userId' => $userId,
                        'roleName' => $role,
                        'createdAt' => time(),
                    ]
                );
            }
        }

        foreach ($rules as $name => $ruleData) {
            $this->rules[$name] = unserialize($ruleData);
        }
    }

    protected function loadAssignments()
    {
        $users = (new Query())->select('id, access_rights')->from('user')->indexBy('id')->all();
        foreach ($users as $id => $user) {
            $users[$id] = Json::decode($user['access_rights']);
            if (!$users[$id]) {
                unset($users[$id]);
            }
        }
        return $users;
    }

    protected function saveAssignments()
    {
        $assignmentData = [];
        foreach ($this->assignments as $userId => $assignments) {
            foreach ($assignments as $name => $assignment) {
                /* @var $assignment Assignment */
                $assignmentData[$userId][] = $assignment->roleName;
            }
            $x = \Yii::$app->db->createCommand()->update(
                'user',
                ['access_rights' => Json::encode($assignmentData[$userId])],
                'id = :id',
                [':id' => $userId]
            )->execute();
        }
    }

    public function getGuestActions()
    {
        return $this->guestActions;
    }

}
