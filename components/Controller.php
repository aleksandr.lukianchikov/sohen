<?php

namespace app\components;

use yii\filters\AccessControl;
use \Yii;
use yii\helpers\Url;

/**
 * Class Controller
 * @package app\components
 * @property View $view
 */
class Controller extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \Yii::$app->user->can(self::getFullRoute());
                        }
                    ],
                    [
                        'allow' => true,
                        'roles' => ['?'],
                        'matchCallback' => function ($rule, $action)  {
                            $guestActions = \Yii::$app->authManager->getGuestActions();
                            return !empty($guestActions) && in_array(self::getFullRoute(), $guestActions, true);
                        }
                    ]
                ],
            ],
        ];
    }

    public static function getFullRoute()
    {
        $action = \Yii::$app->controller->action->id;
        $controller = \Yii::$app->controller->id;
        $module = \Yii::$app->controller->module->getUniqueId();
        if ($module) {
            $route = "$module/$controller/$action";
        } else {
            $route = "$controller/$action";
        }

        return $route;
    }
}