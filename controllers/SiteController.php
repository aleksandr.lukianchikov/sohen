<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\filters\AccessControl;
use app\components\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $parentBehaviors = parent::behaviors();
        return ArrayHelper::merge($parentBehaviors, [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'set-locale'=>[
                'class'=>'app\actions\SetLocaleAction',
                'locales'=>array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        return $this->render('about');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if(isset($_POST['register-button']))
        {
            return $this->redirect(['user/register']);
        }
        else {
            // Если НЕ гость
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            $model = new LoginForm();
            $model->load(Yii::$app->request->post());
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->login()) {
                $rights = Yii::$app->user->identity->getAccessRightsAsArray();
                if (in_array(User::ROLE_CUSTOMER, $rights)) {
                    Yii::$app->user->setReturnUrl(['default/index']);
                } elseif (in_array(User::ROLE_PRODUCER_ADMIN, $rights)) {
                    Yii::$app->user->setReturnUrl(['admin/default/view']);
                }elseif (in_array(User::SUPER_ADMIN, $rights)) {
                    Yii::$app->user->setReturnUrl(['superadmin/default/view']);
                }
                return $this->goBack();
            }

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionReset()
    {
        return $this->redirect(['user/reset']);
        // Этот токен надо будет записать в базу когда нажмут на кнопку
        // $token = md5(Yii::$app->security->generateRandomString());
    }
}
