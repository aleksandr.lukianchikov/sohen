<?php
namespace app\controllers;

use app\components\Controller;
use app\models\PasswordsForm;
use app\models\RegisterForm;
use app\models\ResetForm;
use app\models\User;
use \Yii;
use yii\helpers\Url;
use yii\log\Logger;
use yii\web\HttpException;

class UserController extends Controller
{
    public function actionRegister()
    {
        $modelRegisterForm = new RegisterForm();
        if ($modelRegisterForm->load(Yii::$app->request->post()) && $modelRegisterForm->validate()) {
            $userModel = new User();
            $userModel->setAttributes($modelRegisterForm->getAttributes());
            $userModel->setPass($userModel->password);
            $userModel->delivery_address = $modelRegisterForm->street . ' ' . $modelRegisterForm->number_house;
            $userModel->access_rights = json_encode(['customer']);
            $userModel->status = 'moderate';
            if($userModel->save()) {
                return $this->redirect(['site/login']);
            }
        }
        return $this->render('register', [
            'modelRegisterForm' => $modelRegisterForm,
        ]);
    }
    public function actionView(){

        return $this->render('view', [
            'model' => $this->findModel(Yii::$app->user->identity->id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdate()
    {
        $modelRegisterForm = new RegisterForm();
        $model = $this->findModel(Yii::$app->user->identity->id);
        $modelRegisterForm->shop_name = $model->shop_name;
        $modelRegisterForm->email = $model->email;
        //$modelRegisterForm->password = $model->password;
        //$modelRegisterForm->password_confirm = $model->password;
        $modelRegisterForm->tel = $model->tel;
        $modelRegisterForm->city = $model->city;
        $modelRegisterForm->street = $model->street;
        $modelRegisterForm->number_house = $model->number_house;
        $modelRegisterForm->language = $model->language;


        if ($modelRegisterForm->load(Yii::$app->request->post()) && $modelRegisterForm->validate()) {
            $model->setAttributes($modelRegisterForm->getAttributes());
            $model->setPass($model->password);
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'modelRegisterForm' => $modelRegisterForm, 'model' => $model

        ]);
    }

    public function actionReset()
    {
        $modelResetForm = new ResetForm();

        if ($modelResetForm->load(Yii::$app->request->post()) && $modelResetForm->validate()){
            $email = $modelResetForm->email;
            if(($model = User::find()->where(['email' => $email])->one()) !== null){
                $model->token = md5(Yii::$app->security->generateRandomString());
                if($model->save(false)) {
                    $url = Url::toRoute(['/user/set-password', 'token' => $model->token], true);
                    Yii::$app->mailer->compose('user/reset', ['url' => $url])
                        ->setFrom(Yii::$app->params['adminEmail'])
                        ->setTo($email)
                        ->setSubject(t('Восстановление пароля'))
                        ->send();
                    Yii::$app->session->setFlash('success', t('Следуйте инструкциям в отправленном на вашу почту письме.')  . ' '. $email);
                    return $this->refresh();
                }
            }
            Yii::$app->session->setFlash('danger', t('Указанная почта не найдена в системе.'));
        }

        return $this->render('reset', [
            'modelResetForm' => $modelResetForm,
        ]);

    }

    public function actionSetPassword($token){
        // SQL injection
        //var_dump("SELECT * FROM user WHERE token = '$token'"); die;
        // x2' OR '1
        // SELECT * FROM user WHERE token = 'x2' OR '1'
        // $result = Yii::$app->db->createCommand("SELECT * FROM user WHERE token = :token")->bindParam(':token', $token)->query()->read();

        $model = User::findOne(['token' => $token]);
        if (!$model) {
            throw new HttpException(403);
        }
        $modelPasswordsForm = new PasswordsForm();
        if ($modelPasswordsForm->load(Yii::$app->request->post()) && $modelPasswordsForm->validate()){
            $model->password = Yii::$app->security->generatePasswordHash($modelPasswordsForm->password);
            if ($model->save(false)) {
                $model->token = '';
                $model->save(false);
            }
            return $this->redirect(['site/login']);
        }

        return $this->render('passwords', [
            'modelPasswordsForm' => $modelPasswordsForm,
        ]);
    }
}