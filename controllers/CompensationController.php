<?php

namespace app\controllers;

use Yii;
use app\models\Compensation;
use app\models\CompensationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Producer;

/**
 * CompensationController implements the CRUD actions for Compensation model.
 */
class CompensationController extends Controller
{
    /**
     * @inheritdoc
     */
  /*  public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
  */

    /**
     * Lists all Compensation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompensationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $producerModels = Producer::find()->all();


        $menuItemsComp = [];
        foreach ($producerModels as $producer) {

            if($producer->compensation == true) {
                $menuItemsComp[] = ['label' => $producer->name,
                    'url' => ['/compensation/create', 'producer_id' => $producer->id],
                    'options' => ['class' => 'add-producer']];
            }
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'menuItemsComp' => $menuItemsComp,
                ]);
            }

    /**
     * Displays a single Compensation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Compensation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($producer_id)
    {

        $model = new Compensation();
        $visitdate = new \DateTime('now', new \DateTimeZone('UTC'));
        $model->statement_date = $visitdate->format('Y-m-d H:i:s');
        $model->is_approved = 0;
        $model->completed = 0;
        $model->producer_id = $producer_id;
        $model->user_id = Yii::$app->user->identity->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Compensation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Compensation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Compensation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Compensation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Compensation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
