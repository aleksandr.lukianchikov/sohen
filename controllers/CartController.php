<?php

namespace app\controllers;

use app\models\Producer;
use app\models\ShopCart;
use app\models\Order;
use app\models\OrderProduct;
use Yii;
use app\models\Discount;
use yii\base\Model;
use yii\db\ActiveQuery;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Request;

/**
 * DiscountController implements the CRUD actions for Discount model.
 */
class CartController extends Controller
{
    public function actionAdd($id, $amount = 1)
    {

        $userId = Yii::$app->user->id;
        $productModel = ShopCart::findOne([
            'product_id' => $id,
            'user_id' => $userId,
        ]);
        if (!$productModel) {
            $productModel = new ShopCart();
            $productModel->product_id = $id;
            $productModel->user_id = $userId;
            $productModel->amount = $amount;
        } else {
            $productModel->amount = $amount;
        }
        $productModel->save();
    }

    public function actionIndex($producer_id)
    {
        $producerModel = Producer::findOne($producer_id);
        $userId = Yii::$app->user->id;
        $cartProductModels = ShopCart::find()
            ->joinWith([
                'product' => function (ActiveQuery $query) use ($producerModel) {
                    $query->andWhere(['product.producer_id' => $producerModel->id]);
            }], true, 'INNER JOIN')
            ->where(['shop_cart.user_id' => $userId])
            ->all();

        if (Model::loadMultiple($cartProductModels, Yii::$app->request->post())) {

            foreach ($cartProductModels as $model) {
                if(!$model->save()) {
                    // Здесь можно доработать, и выводить ошибки,
                    // например в случае, слишком большого числа
                }
            }
            if (Yii::$app->request->getIsAjax()) {
                return;
            }
            return $this->refresh();
        }
        return $this->render('index', ['cartProductModels' => $cartProductModels, 'producerModel' => $producerModel]);
        // Нужно для получения чистого SQL запроса
        // Обратить внимание, что вконце конструктора запроса нет методов all() one() и.д.
        /*$shopQuery = ShopCart::find()
            ->joinWith([
                'product' => function (ActiveQuery $query) use ($producer_id) {
                    $query->andWhere(['product.producer_id' => $producer_id]);
            }], true, 'INNER JOIN')
            ->where(['shop_cart.user_id' => $userId]);
        var_dump($shopQuery->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql); die;
        */


    }

    /**
     * Deletes an existing Discount model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
    }

    /**
     * Finds the Discount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopCart the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopCart::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRepeat($id)
    {
        $model = Order::findOne($id);
        $producerId = $model->producer_id;
        $producerModel =  Producer::findOne($producerId);
        $modelOrderProducts = OrderProduct::find()->where(['order_id' => $id])->all();

        foreach ($modelOrderProducts as $orderProductModel) {
            $cartModel = new ShopCart();
            $cartModel->product_id = $orderProductModel->product_id;
            $cartModel->amount = $orderProductModel->amount;
            $cartModel->user_id = $model->user_id;
            $cartModel->save();
        }
        $cartProductModels = ShopCart::find()
            ->joinWith([
                'product' => function (ActiveQuery $query) use ($producerModel) {
                    $query->andWhere(['product.producer_id' => $producerModel->id]);
                }], true, 'INNER JOIN')
            ->where(['shop_cart.user_id' => $model->user_id])
            ->all();

        return $this->render('index', ['cartProductModels' => $cartProductModels, 'producerModel' => $producerModel]);
    }
}
