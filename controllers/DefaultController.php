<?php

namespace app\controllers;

use app\models\Producer;
use Yii;
use app\models\Product;
use yii\data\ActiveDataProvider;
use app\components\Controller;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $producerQuery = Producer::find();
        $dataProvider =  new ActiveDataProvider([
            'query' => $producerQuery,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);

         return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
