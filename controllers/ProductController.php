<?php

namespace app\controllers;

use Yii;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use app\models\Product;
use app\models\customer\ProductSearch;
use app\models\Producer;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex($producer_id)
    {
        $searchModel = new ProductSearch();
        $searchModel->producer_id = $producer_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $producerModel = Producer::findOne($producer_id);

         return $this->render('index', [
            'searchModel' => $searchModel,
            'producerModel' => $producerModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
