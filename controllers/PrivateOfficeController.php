<?php
/**
 * Created by PhpStorm.
 * User: Bramnik
 * Date: 9/6/2017
 * Time: 4:36 PM
 */

namespace app\controllers;
use app\components\Controller;
use app\models\Producer;
use Yii;

class PrivateOfficeController extends Controller
{
    public function actionView()
    {
        $producerModels = Producer::find()->all();
        $menuItemsReturn = [];
        $menuItems = [];
        foreach ($producerModels as $producer) {
            $menuItems[] = ['label' => $producer->name ,
                            'url' => ['/cart/index', 'producer_id' => $producer->id],
                            'options' => ['class' => 'add-producer']];

            $menuItemsReturn[] = ['label' => $producer->name ,
                'url' => ['/productreturn/create', 'producer_id' => $producer->id],
                'options' => ['class' => 'add-producer']];

            if($producer->compensation == true) {
                $menuItemsComp[] = ['label' => $producer->name,
                    'url' => ['/compensation/create', 'producer_id' => $producer->id],
                    'options' => ['class' => 'add-producer']];
            }
        }

        return $this->render('view',['menuItems' => $menuItems,'menuItemsComp' => $menuItemsComp,'menuItemsReturn' => $menuItemsReturn ]);
    }

}