<?php
/**
 * Created by PhpStorm.
 * User: Bramnik
 * Date: 8/7/2017
 * Time: 3:47 PM
 */

namespace app\controllers;

use app\models\enums\Language;
use app\models\Product;
use app\models\PayForm;
use Yii;
use app\components\Controller;
use yii\db\ActiveQuery;
use app\models\OrderProduct;
use app\models\OrderDiscount;
use app\models\Order;
use app\models\ShopCart;
use app\models\Producer;
use app\models\Discount;
use yii\helpers\Url;

class OrderproductController extends Controller
{
    private $modelOrderProducts = [];
    private $modelOrderDiscounts = [];

    public function actionCreate($producer_id)
    {
        // как заставить контроллеры общаться мужду собой чтобы не делать одно и то же два раза
        $producerModel = Producer::findOne($producer_id);
        $userId = Yii::$app->user->id;
        $cartProductModels = ShopCart::find()
            ->joinWith([
                'product' => function (ActiveQuery $query) use ($producerModel) {
                    $query->andWhere(['product.producer_id' => $producerModel->id]);
                }], true, 'INNER JOIN')
            ->where(['shop_cart.user_id' => $userId])
            ->all();

        if (empty($cartProductModels)) {
            $this->redirect(['/order/index']);
        }

        // Create order
        $modelOrder = new Order();
        $modelOrder->user_id = $userId;
        $visitdate = new \DateTime('now', new \DateTimeZone('UTC'));
        $modelOrder->order_date = $visitdate->format('Y-m-d H:i:s');
        $modelOrder->orderstatus = 'created';
        $modelOrder->producer_id = $producer_id;
        $modelOrder->save();
        $orderId = $modelOrder->id;

        // Create order products by shop cart
        //$this->modelOrderProducts =  array();
        $totalProducts = 0;
        foreach ($cartProductModels as $modelCartProduct){
            $modelOrderProduct = new OrderProduct();
            $modelOrderProduct->order_id = $orderId;
            $modelOrderProduct->product_id = $modelCartProduct->product_id;
            $modelOrderProduct->amount = $modelCartProduct->amount;
            $modelProduct = Product::findOne($modelCartProduct->product_id);
            //считаем сумму продукта в заданном количестве пока без акций
            $modelOrderProduct->sum = $modelProduct->price * $modelCartProduct->amount;
            // сумма всех товаров в корзине без учета скидок
            $totalProducts = $totalProducts + $modelOrderProduct->sum;
            //var_dump($totalProducts);die;

            $modelOrderProduct->save();
           // var_dump($modelOrderProduct);die;

            $this->modelOrderProducts[] = $modelOrderProduct;
        }

        // Get current date&time
        $visitdate = new \DateTime('now', new \DateTimeZone('UTC'));

        $totalDiscountByProd = 0;
        // Save original order products list without discounts
        $modelOrderProductsOrg = $this->modelOrderProducts;
        foreach ($modelOrderProductsOrg as $modelOrderProduct) {
            $discountModelByProd = Discount::find()->where(['discount.producer_id' => $producerModel->id, 'discount.type' => 'prod_assoc', 'discount.product_id' => $modelOrderProduct->product_id])
                ->andwhere(['>=', 'date_end', $visitdate->format('Y-m-d H:i:s')])
                ->andwhere(['<=', 'date_start', $visitdate->format('Y-m-d H:i:s')])->one();
            //var_dump($discountModelByProd); die;
            if ($discountModelByProd and ($discountModelByProd->qnt_product_gift > 0)) {
                // First of all calculate amount of gift pruduct by current product
                $giftProductAmount = intval($modelOrderProduct->amount / $discountModelByProd->min_qnt_product * $discountModelByProd->qnt_product_gift);
                if($giftProductAmount >= 1) {
                    $productGift_id = $discountModelByProd->product_gift_id;
                    $modelProduct = Product::findOne($productGift_id);
                    if($modelProduct != null) {
                        // Find product gift in current order
                        $modelOrderProductAsGift = OrderProduct::find()->where(['order_id' => $orderId, 'product_id' => $productGift_id])->one();
                        if ($modelOrderProductAsGift != null) {
                            $modelOrderProductAsGift->amount += $giftProductAmount;
                            $totalProducts -= $modelOrderProductAsGift->sum;
                            $modelOrderProductAsGift->sum = $modelProduct->price * $modelOrderProductAsGift->amount;
                            $totalProducts += $modelOrderProductAsGift->sum;
                            $modelOrderProductAsGift->update();
                        } else {
                            $giftModelOrderProduct = new OrderProduct();
                            $giftModelOrderProduct->order_id = $orderId;
                            $giftModelOrderProduct->product_id = $productGift_id;
                            $giftModelOrderProduct->amount = $giftProductAmount;
                            $giftModelOrderProduct->sum = $modelProduct->price * $giftProductAmount;
                            $totalProducts = $totalProducts + $giftModelOrderProduct->sum;
                            $giftModelOrderProduct->save();
                            $this->modelOrderProducts[] = $giftModelOrderProduct;
                        }

                        // Add discount by order
                        $orderDiscount = new OrderDiscount();
                        $orderDiscount->order_id = $orderId;
                        $orderDiscount->sum = $modelProduct->price * $giftProductAmount * $discountModelByProd->percent / 100;
                        $totalDiscountByProd = $totalDiscountByProd + $orderDiscount->sum;
                        $orderDiscount->discount_id = $discountModelByProd->id;
                        $orderDiscount->save();
                        $this->modelOrderDiscounts[] = $orderDiscount;
                    }
                }
            }
        }

        // Start order product calculation
        $totalOrder = $totalProducts - $totalDiscountByProd;

        //        $discountModels = Discount::find()->where(['discount.producer_id' => $producerModel->id, 'discount.user_id' => [null, $userId]])->all();
        //$discountModels = Discount::find()->where(['discount.producer_id' => $producerModel->id, 'discount.user_id' => null])->all();
       // $visitdate = new DateTime();
         //var_dump($visitdate);die;


 //       $discountModels = Discount::find()->where(['discount.producer_id' => $producerModel->id, 'discount.type' => 'sum_assoc'])->andFilterCompare('discount.sum', '<='.$totalProducts)->andFilterCompare('discount.date-start',$visitdate ,'betweens','discount.date.end')->all();
 //       $discountModels = Discount::find()->where(['>=', 'date_end', $visitdate->format('Y-m-d H:i:s')])->all();
        //$discountModels = Discount::find()->where(['between', $visitdate->format('Y-m-d H:i:s'), 'discount.date_start','discount.date_end'])->all();
        //$discountModels = Discount::find()->where(['>=', 'date_end', $visitdate->format('Y-m-d H:i:s')])->andwhere(['<=', 'date_start', $visitdate->format('Y-m-d H:i:s')])->all();
        $discountModelBySum = Discount::find()->where(['discount.producer_id' => $producerModel->id, 'discount.type' => 'sum_assoc'])
            ->andFilterCompare('discount.sum', '<='.$totalProducts)
            ->andwhere(['>=', 'date_end', $visitdate->format('Y-m-d H:i:s')])
            ->andwhere(['<=', 'date_start', $visitdate->format('Y-m-d H:i:s')])
            ->orderBy("sum DESC")->one();
       // var_dump($discountModel->percent);die;

        //$this->modelOrderDiscounts =  array();
        if($discountModelBySum){
            $orderDiscount = new OrderDiscount();
            $orderDiscount->order_id = $orderId;
            $orderDiscount->sum = $discountModelBySum->percent * $totalProducts/100;
            $totalOrder = $totalOrder - $orderDiscount->sum;
            $orderDiscount->discount_id = $discountModelBySum->id;
            $orderDiscount->save();
            $this->modelOrderDiscounts[] = $orderDiscount;
        }
        $discountModelByUser = Discount::find()->where(['discount.producer_id' => $producerModel->id,'discount.user_id' => $userId, 'discount.type' => 'user_assoc'])
            ->andwhere(['>=', 'date_end', $visitdate->format('Y-m-d H:i:s')])
            ->andwhere(['<=', 'date_start', $visitdate->format('Y-m-d H:i:s')])->one();
        if($discountModelByUser){
            $orderDiscount = new OrderDiscount();
            $orderDiscount->order_id = $orderId;
            $orderDiscount->sum = $discountModelByUser->percent * $totalProducts/100 ;
            $totalOrder = $totalOrder - $orderDiscount->sum;
            $orderDiscount->discount_id = $discountModelByUser->id;
            $orderDiscount->save();
            $this->modelOrderDiscounts[] = $orderDiscount;
        }
        $discountModelByFeast = Discount::find()->where(['discount.producer_id' => $producerModel->id,'discount.type' => 'feast_assoc'])
            ->andwhere(['>=', 'date_end', $visitdate->format('Y-m-d H:i:s')])
            ->andwhere(['<=', 'date_start', $visitdate->format('Y-m-d H:i:s')])->one();
        if($discountModelByFeast){
            $orderDiscount = new OrderDiscount();
            $orderDiscount->order_id = $orderId;
            $orderDiscount->sum = $discountModelByFeast->percent * $totalProducts/100    ;
            $totalOrder = $totalOrder - $orderDiscount->sum;
            $orderDiscount->discount_id = $discountModelByFeast->id;
            $orderDiscount->save();
            $this->modelOrderDiscounts[] = $orderDiscount;
        }


        $modelOrder->sum = $totalOrder;
        $modelOrder->update();


        // clean shop cart - delete all related records
        foreach ($cartProductModels as $cartProductModel) {
            $cartProductModel->delete();
        }

        $this->redirect(['/orderproduct/view', 'id' => $modelOrder->id]);

        //var_dump($discountModel_1->percent);die;
        //var_dump($orderProducts);die;
        /*return $this->render('create', ['modelOrderProducts' => $this->modelOrderProducts ,'producerModel' => $producerModel,
            'modelOrderDiscounts' => $this->modelOrderDiscounts, 'totalOrder'=>$totalOrder, 'totalProducts'=> $totalProducts]);
        */
    }
    // TODO Сумму нельзя передавать, нужно передавать лишь номер заказа.
    // А из этого заказа считать сумму, которая получилась исходя из набранных товаров.
    public function actionPayform($id,$sum)
    {

        $modelPayForm = new PayForm();
        // см выше коммент
        $modelPayForm->sum = $sum;

        if ($modelPayForm->load(Yii::$app->request->post()) && $modelPayForm->validate()) {
            $modelPayForm->sendRequestToGateway($id);
            return $this->redirect(['pay',['modelPayForm' => $modelPayForm, 'id' => $id]]);
        }
        return $this->render('payform', [
            'id' => $id,'modelPayForm' => $modelPayForm
        ]);

    }

    /**
     * http://sohen.loc/index.php?r=orderproduct/update-status&status=paid&token=X1X2343339djfk
     * Этот метод дергает платежный шлюз в случае если статус оплаты изменился на шлюзе.
     * @param $status Статус, который отправляем нам платежный шлюз
     * @param $token Некий уникальный придуманный ключ, который знает только платежный шлюз
     * @param  $orderId
     */
    public function actionUpdateStatus($status, $token, $orderId) {
        if ($token != 'X1X2343339djfk') {
            // Завершаем скрипт, потому что токен некорректный
            Yii::$app->end();
        }
        // В замисимости от стутуса выполняем нужное действие
        // success - обновляем заказу как оплачено и т.д.
    }

    public function actionView($id){
        $model  = Order::findOne($id);
        $modelOrderProducts = OrderProduct::find()->where(['order_id' => $id])->all();
        //var_dump(count($modelOrderProducts));die;

        $totalProducts = 0;
        foreach ($modelOrderProducts as $OrderProduct){
            $totalProducts = $totalProducts + $OrderProduct->sum;
        }
        $modelOrderDiscounts = OrderDiscount::find()->where(['order_id' => $id])->all();

        return $this->render('view', ['modelOrderProducts' => $modelOrderProducts, 'model' => $model, 'totalProducts' => $totalProducts, 'modelOrderDiscounts' => $modelOrderDiscounts,]);
    }



    public function actionDelete($id){

        $model  = Order::findOne($id);
        //$producerId = $model->producer_id;
        $modelOrderProducts = OrderProduct::find()->where(['order_id' => $id])->all();
        foreach ($modelOrderProducts as $orderProductModel) {
            $orderProductModel->delete();
        }
        $modelOrderDiscounts = OrderDiscount::find()->where(['order_id' => $id])->all();
        foreach ($modelOrderDiscounts as $orderDiscountModel) {
            $orderDiscountModel->delete();
        }

        $model->delete();
        return $this->redirect(['/default/index']);
    }

    public function actionSend($id,$sum,$producer_id){
        $str_text_message = "Получен новый заказ под номером" . $id . "на сумму" . $sum;
        $modelOrder  = Order::findOne($id);
        $producerModel = Producer::findOne($producer_id);
        $producer_tel = $producerModel->tel;

        $subject = t('Получен новый заказ', [], Language::getSystemLanguage($producerModel->user->language));

        Yii::$app->mailer->compose('notification/order', ['id' => $id, 'producerModel' => $producerModel])
            ->setFrom(Yii::$app->params['noreplyEmail'])
            ->setTo($producerModel->user->email)
            ->setSubject($subject)
            ->send();

        $postfields = array(
            'Username' => 'bramnik@hotmail.com',
            'Password' => '8c46ade4-4e4a-4421-bf36-c19ad37845c9',
            'MsgName'=> 'Hello',
            'GlobalID'=> '-1',
            'Msg'=> $str_text_message,
            'FromMobile'=> '0542609501',
            'Mobiles'=> $producer_tel
        );

        // open a curl connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,
            'http://www.slng6.com/WebService/SendSms.asmx/CreateAndSendSmsToMultiContactsByMobileNo?'.http_build_query($postfields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        //$modelOrder->orderstatus = 'send';
        $CurDate = new \DateTime('now', new \DateTimeZone('Asia/Jerusalem'));           // например тут мы вместо отправки сообщения сохраняем в файл для демонстрации
        $testFile = \Yii::getAlias('@runtime') . '/sms.log';
        file_put_contents($testFile, $CurDate->format('Y-m-d H:i:s') . " " . $producer_tel . " " . $str_text_message . PHP_EOL, FILE_APPEND);
        file_put_contents($testFile, $result . PHP_EOL, FILE_APPEND);

        if($result === false){
            // echo 'Curl error: ' . curl_error($ch);
        }
        else{
            $modelOrder->orderstatus = 'send';
            $modelOrder->save();
            //var_dump($modelOrder); var_dump($id);die;
            return $this->redirect(['/private-office/view']);
        }
    }

}

// save array to session
//$orderProducts = Yii::$app->session['orderProducts'];
//$orderProducts[] = $this->modelOrderProducts;
//Yii::$app->session['orderProducts'] = $orderProducts;
// get array from session
//$orderProducts = Yii::$app->session['orderProducts'][0];

