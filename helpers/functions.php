<?php

/**
 * @return \app\models\User|null|\yii\web\IdentityInterface
 */
function getUserModel()
{
    return getUser()->identity;
}

/**
 * @return mixed|User|\yii\web\User
 */
function getUser()
{
    return Yii::$app->user;
}

function t($message, $params = [], $language = null)
{
    return Yii::t('app', $message, $params, $language);
}

function te(\yii\db\ActiveRecord $row, $attribute, $code = null) {
    $lang = $code ? $code : Yii::$app->language;
    // Если принудительно указываем язык, то еще раз выводить на исходном языке не нужно,
    // Поэтому указываем пустую строчку.
    $defaultValue = $code ? '' : $row->{$attribute};
    return $row->hasTranslation($lang) ? $row->translate($lang)->{$attribute} : $defaultValue;
}