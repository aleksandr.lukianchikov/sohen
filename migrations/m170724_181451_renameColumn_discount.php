<?php

use yii\db\Migration;

class m170724_181451_renameColumn_discount extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->renameColumn('discount', 'image', 'image_1');
    }

    public function down()
    {
        $this->renameColumn('discount', 'image_1', 'image');
        return false;
    }

}
