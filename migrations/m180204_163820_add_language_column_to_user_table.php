<?php

use yii\db\Migration;

/**
 * Handles adding language to table `user`.
 */
class m180204_163820_add_language_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'language', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'language');
    }
}
