<?php

use yii\db\Migration;

/**
 * Handles adding completed to table `product_return`.
 */
class m170908_165049_add_completed_column_to_product_return_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product_return', 'completed', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product_return', 'completed');
    }
}
