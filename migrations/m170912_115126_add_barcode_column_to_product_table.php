<?php

use yii\db\Migration;

/**
 * Handles adding barcode to table `product`.
 */
class m170912_115126_add_barcode_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'barcode', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'barcode');
    }
}
