<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notification_text_translation`.
 */
class m171126_145111_change_user_password extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
       $this->alterColumn('user', 'password', 'VARCHAR(128)');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->alterColumn('user', 'password', 'VARCHAR(50)');
    }
}
