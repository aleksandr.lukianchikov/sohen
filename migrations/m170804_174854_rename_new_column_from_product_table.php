<?php

use yii\db\Migration;

class m170804_174854_rename_new_column_from_product_table extends Migration
{
    public function up()
    {
        $this->renameColumn('product', 'new', 'status_new');
    }

    public function down()
    {
        $this->renameColumn('product', 'status_new', 'new');
        return false;
    }
}
