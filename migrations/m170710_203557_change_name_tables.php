<?php

use yii\db\Migration;

/**
 * Handles dropping user_status from table `user`.
 */
class m170710_203557_change_name_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->renameTable('orderProduct', 'order_product');
        $this->renameTable('packType', 'pack_type');
        $this->renameTable('productType', 'producttype');
        $this->renameTable('shopCart', 'shop_cart');
        $this->renameTable('storageConditions', 'storage_conditions');

        $this->renameColumn('producttype', 'id_parent', 'parent_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->renameTable('order_product', 'orderProduct');
        $this->renameTable('pack_type', 'packType');
        $this->renameTable('producttype', 'productType');
        $this->renameTable('shop_cart', 'shopCart');
        $this->renameTable('storage_conditions', 'storageConditions');
    }
}
