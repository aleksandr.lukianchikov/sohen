<?php

use yii\db\Migration;

/**
 * Handles adding statement_date to table `compensation`.
 */
class m170912_101853_add_statement_date_column_to_compensation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('compensation', 'statement_date', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('compensation', 'statement_date');
    }
}
