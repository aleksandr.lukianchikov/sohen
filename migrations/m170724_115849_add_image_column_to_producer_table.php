<?php

use yii\db\Migration;

/**
 * Handles adding image to table `producer`.
 */
class m170724_115849_add_image_column_to_producer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('producer', 'image', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('producer', 'image');
    }
}
