<?php

use yii\db\Migration;

/**
 * Handles adding producer_id to table `discount`.
 */
class m170710_075556_add_producer_id_column_to_discount_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('discount', 'producer_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('discount', 'producer_id');
    }
}
