<?php

use yii\db\Migration;

class m170908_162405_add_statement_date_column_to_product_return_table extends Migration
{
    public function up()
    {
        $this->addColumn('product_return', 'statement_date', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product_return', 'statement_date');
    }
}
