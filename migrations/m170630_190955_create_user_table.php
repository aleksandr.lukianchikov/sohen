<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 * Has foreign keys to the tables:
 *
 * - `userStatus`
 */
class m170630_190955_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'shop_name' => $this->string(100),
            'email' => $this->string(50),
            'password' => $this->string(50),
            'tel' => $this->string(200),
            'delivery_address' => $this->string(),
            // 'userStatus_id' => $this->integer(),
            'user_status' => "ENUM('user', 'prod_admin', 'admin') NOT NULL DEFAULT 'user'",
            'money_box' => $this->float(),
            'sms_notification' => $this->boolean(),
            'email_notification' => $this->boolean(),

        ]);

    }

    public function down(){
        $this->dropTable('user');
    }
}
