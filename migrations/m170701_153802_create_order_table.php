<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m170701_153802_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'order_date' => $this->date(),
            'user_id' => $this->integer(),
            'orderstatus' => "ENUM('paid', 'delivered', 'in_the_way')",
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx_order_user_id',
            'order',
            'user_id'
        );
/*
        // add foreign key for table `user`
        $this->addForeignKey(
            'fk_order_user_id',
            'order',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
*/
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk_order_user_id',
            'order'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx_order_user_id',
            'order'
        );

        $this->dropTable('order');
    }
}
