

<?php

use yii\db\Migration;

class m170929_133447_rename_text_table_to_notification_text_table extends Migration
{
    public function safeUp()
    {
        $this->renameTable('text','notification_text');
    }

    public function safeDown()
    {
        $this->renameTable('notification_text','text');

        return false;
    }


}

