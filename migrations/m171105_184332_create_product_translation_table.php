<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_translation`.
 */
class m171105_184332_create_product_translation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('product_translation', [
            'id' => 'pk',
            'product_id' => $this->integer(),
            'language' => 'VARCHAR(6) NOT NULL',
            'name' => 'VARCHAR(255) NOT NULL',
            'discription' => $this->string(1000),
            'composition' => $this->string(1000),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('product_translation');
        return true;
    }
}
