<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_return`.
 */
class m170711_102436_create_product_return_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product_return', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'product_id' => $this->integer(),
            'reason_id' => $this->integer(),
            'reason_foto' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_return');
    }
}
