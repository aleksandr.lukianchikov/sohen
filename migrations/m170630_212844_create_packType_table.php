<?php

use yii\db\Migration;

/**
 * Handles the creation of table `packType`.
 */
class m170630_212844_create_packType_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('packType', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('packType');
    }
}
