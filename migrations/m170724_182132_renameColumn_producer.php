<?php

use yii\db\Migration;

class m170724_182132_renameColumn_producer extends Migration
{
    public function up()
    {
        $this->renameColumn('producer', 'image', 'image_1');
    }

    public function down()
    {
        $this->renameColumn('producer', 'image_1', 'image');
        return false;
    }


}
