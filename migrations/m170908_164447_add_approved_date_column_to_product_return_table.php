<?php

use yii\db\Migration;

/**
 * Handles adding approved_date to table `product_return`.
 */
class m170908_164447_add_approved_date_column_to_product_return_table extends Migration
{
    public function up()
    {
        $this->addColumn('product_return', 'approved_date', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product_return', 'approved_date');
    }
}
