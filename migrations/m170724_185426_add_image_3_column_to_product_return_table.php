<?php

use yii\db\Migration;

/**
 * Handles adding image_3 to table `product_return`.
 */
class m170724_185426_add_image_3_column_to_product_return_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product_return', 'image_3', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product_return', 'image_3');
    }
}
