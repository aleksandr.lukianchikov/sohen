<?php

use yii\db\Migration;

/**
 * Handles adding image to table `discount`.
 */
class m170719_132446_add_image_column_to_discount_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('discount', 'image', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('discount', 'image');
    }
}
