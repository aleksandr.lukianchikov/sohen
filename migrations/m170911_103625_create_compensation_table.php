<?php

use yii\db\Migration;

/**
 * Handles the creation of table `compensation`.
 */
class m170911_103625_create_compensation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('compensation', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'producer_id' => $this->integer(),
            'product_id' => $this->integer(),
            'amount' => $this->integer(),
            'percentReturn' => $this->integer(),
            'sum' => $this->integer(),
            'is_approved' => $this->boolean(),
            'approved_date' => $this->date(),
            'completed' => $this->boolean(),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            'idx_compensation_user_id',
            'compensation',
            'user_id'
        );
        /*
                // add foreign key for table `user`
                $this->addForeignKey(
                    'fk_compensation_user_id',
                    'compensation',
                    'user_id',
                    'user',
                    'id',
                    'CASCADE'
                );
        */

        // creates index for column `producer_id`
        $this->createIndex(
            'idx_compensation_producer_id',
            'compensation',
            'producer_id'
        );

        /*       // add foreign key for table `producer`
               $this->addForeignKey(
                   'fk_compensation_producer_id',
                   'compensation',
                   'producer_id',
                   'producer',
                   'id',
                   'CASCADE'
               );
        */


        // creates index for column `product_id`
        $this->createIndex(
            'idx_compensation_product_id',
            'compensation',
            'user_id'
        );
    }
        /*
                // add foreign key for table `product`
                $this->addForeignKey(
                    'fk_compensation_product_id',
                    'compensation',
                    'product_id',
                    'product',
                    'id',
                    'CASCADE'
                );
        */

        /**
         * @inheritdoc
         */
        public
        function down()
        {
            // drops foreign key for table `user`
            $this->dropForeignKey(
                'fk_compensation_user_id',
                'compensation'
            );

            // drops index for column `user_id`
            $this->dropIndex(
                'idx_compensation_user_id',
                'compensation'
            );

            // drops foreign key for table `product`
            $this->dropForeignKey(
                'fk_compensation_product_id',
                'compensation'
            );

            // drops index for column `product_id`
            $this->dropIndex(
                'idx_compensation_product_id',
                'compensation'
            );

            // drops foreign key for table `producer`
            $this->dropForeignKey(
                'fk_compensation_producer_id',
                'compensation'
            );

            // drops index for column `producer_id`
            $this->dropIndex(
                'idx_compensation_producer_id',
                'compensation'
            );

            $this->dropTable('orderProduct');
        }
    }

