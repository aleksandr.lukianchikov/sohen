<?php

use yii\db\Migration;

/**
 * Handles adding status to table `user`.
 */
class m171005_110648_add_status_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        //$this->addEnumValue('order','orderstatus',['created','paid', 'delivered', 'in_the_way']);
        $this->addColumn('user', 'status', "ENUM('active', 'inactive', 'moderate')");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'status');
    }
}
