<?php

use yii\db\Migration;

/**
 * Handles adding orderstatus to table `order`.
 */
class m170921_170654_add_orderstatus_column_to_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        //$this->addEnumValue('order','orderstatus',['created','paid', 'delivered', 'in_the_way']);
        $this->addColumn('order', 'orderstatus', "ENUM('created', 'paid', 'delivered', 'in_the_way')");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('order', 'orderstatus');
    }


}
