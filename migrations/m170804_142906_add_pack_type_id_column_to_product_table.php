<?php

use yii\db\Migration;

/**
 * Handles adding pack_type_id to table `product`.
 */
class m170804_142906_add_pack_type_id_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'pack_type_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'pack_type_id');
    }
}
