<?php

use yii\db\Migration;

/**
 * Handles the creation of table `discount_translation`.
 */
class m171108_163353_create_discount_translation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('discount_translation', [
            'id' => $this->primaryKey(),
            'discount_id' => $this->integer(),
            'language' => 'VARCHAR(6) NOT NULL',
            'name' => 'VARCHAR(255) NOT NULL',
            'discription' => $this->string(1000),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('discount_translation');
    }
}
