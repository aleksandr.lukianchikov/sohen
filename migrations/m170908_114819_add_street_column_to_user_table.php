<?php

use yii\db\Migration;

/**
 * Handles adding street to table `user`.
 */
class m170908_114819_add_street_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'street', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'street');
    }
}
