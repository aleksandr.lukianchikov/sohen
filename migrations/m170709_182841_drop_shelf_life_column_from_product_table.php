<?php

use yii\db\Migration;

/**
 * Handles dropping shelf_life from table `product`.
 */
class m170709_182841_drop_shelf_life_column_from_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('product', 'shelf_life');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('product', 'shelf_life', $this->date());
    }
}
