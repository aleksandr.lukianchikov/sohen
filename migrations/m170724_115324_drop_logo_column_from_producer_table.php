<?php

use yii\db\Migration;

/**
 * Handles dropping logo from table `producer`.
 */
class m170724_115324_drop_logo_column_from_producer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('producer', 'logo');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('producer', 'logo', $this->string());
    }
}
