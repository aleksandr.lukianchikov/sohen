<?php

use yii\db\Migration;

/**
 * Handles the creation of table `producer`.
 */
class m170701_141847_create_producer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('producer', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'tel' => $this->string(),
            'address' => $this->string(),
            'site' => $this->string(),
            'account' => $this->string(),
            'email' => $this->string(),
            'slogan' => $this->string(),
            'logo' => $this->binary(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('producer');
    }
}
