<?php

use yii\db\Migration;

/**
 * Handles adding is_approved to table `product_return`.
 */
class m170908_164208_add_is_approved_column_to_product_return_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product_return', 'is_approved', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product_return', 'is_approved');
    }
}
