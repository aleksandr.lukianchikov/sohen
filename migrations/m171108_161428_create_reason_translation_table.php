<?php

use yii\db\Migration;

/**
 * Handles the creation of table `reason_translation`.
 */
class m171108_161428_create_reason_translation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('reason_translation', [
            'id' => $this->primaryKey(),
            'reason_id' => $this->integer(),
            'language' => 'VARCHAR(6) NOT NULL',
            'name' => 'VARCHAR(100) NOT NULL',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('reason_translation');
    }
}
