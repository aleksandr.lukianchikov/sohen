<?php

use yii\db\Migration;

/**
 * Handles adding city to table `user`.
 */
class m170708_095303_add_city_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'city', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'city');
    }
}
