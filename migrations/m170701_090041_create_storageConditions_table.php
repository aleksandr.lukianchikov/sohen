<?php

use yii\db\Migration;

/**
 * Handles the creation of table `storageConditions`.
 */
class m170701_090041_create_storageConditions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('storageConditions', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('storageConditions');
    }
}
