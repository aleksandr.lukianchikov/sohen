<?php

use yii\db\Migration;

/**
 * Handles adding producer_id to table `order`.
 */
class m170909_185316_add_producer_id_column_to_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('order', 'producer_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('order', 'producer_id');
    }
}
