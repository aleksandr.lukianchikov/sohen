<?php

use yii\db\Migration;

/**
 * Handles dropping producttype_id from table `product`.
 */
class m170711_111818_drop_producttype_id_column_from_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('product', 'producttype_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('product', 'producttype_id', $this->integer());
    }
}
