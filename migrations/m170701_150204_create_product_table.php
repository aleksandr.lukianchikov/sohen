
<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 * Has foreign keys to the tables:
 *
 * - `productType`
 * - `storageConditions`
 * - `producer`
 */
class m170701_150204_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'producttype_id' => $this->integer(),
            'discription' => $this->string(1000),
            'composition' => $this->string(1000),
            'price' => $this->float(),
            'pack_gram' => $this->float(),
            'calories' => $this->float(),
            'shelf_life' => $this->date(),
            'storage_conditions_id' => $this->integer(),
            'kosher' => $this->boolean(),
            'available' => $this->boolean(),
            'new' => $this->boolean(),
            'producer_id' => $this->integer(),
            'image_1' => $this->binary(),
            'image_2' => $this->binary(),
            'image_3' => $this->binary(),
        ]);

        // creates index for column `productType_id`
        $this->createIndex(
            'idx_product_producttype_id',
            'product',
            'producttype_id'
        );
/*
        // add foreign key for table `productType`
        $this->addForeignKey(
            'fk_product_producttype_id',
            'product',
            'producttype_id',
            'productType',
            'id',
            'CASCADE'
        );
*/
        // creates index for column `storageConditions_id`
        $this->createIndex(
            'idx_product_storage_conditions_id',
            'product',
            'storage_conditions_id'
        );

/*        // add foreign key for table `storageConditions`
        $this->addForeignKey(
            'fk_product_storageConditions_id',
            'product',
            'storage_conditions_id',
            'storageConditions',
            'id',
            'CASCADE'
        );
*/
        // creates index for column `producer_id`
        $this->createIndex(
            'idx_product_producer_id',
            'product',
            'producer_id'
        );
/*
        // add foreign key for table `producer`
        $this->addForeignKey(
            'fk_product_producer_id',
            'product',
            'producer_id',
            'producer',
            'id',
            'CASCADE'
        );
*/
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `productType`
        $this->dropForeignKey(
            'fk_product_producttype_id',
            'product'
        );

        // drops index for column `productType_id`
        $this->dropIndex(
            'idx_product_producttype_id',
            'product'
        );

        // drops foreign key for table `storageConditions`
        $this->dropForeignKey(
            'fk_product_storage_conditions_id',
            'product'
        );

        // drops index for column `storageConditions_id`
        $this->dropIndex(
            'idx_product_storage_conditions_id',
            'product'
        );

        // drops foreign key for table `producer`
        $this->dropForeignKey(
            'fk_product_producer_id',
            'product'
        );

        // drops index for column `producer_id`
        $this->dropIndex(
            'idx_product_producer_id',
            'product'
        );

        $this->dropTable('product');
    }
}
