<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shopCart`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `product`
 */
class m170630_194203_create_shopCart_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('shopCart', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'product_id' => $this->integer(),
            'amount' => $this->integer(),
            'status' => "ENUM('pending', 'deffered') NOT NULL DEFAULT 'pending'"
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx_shopCart_user_id',
            'shopCart',
            'user_id'
        );

 /*       // add foreign key for table `user`
        $this->addForeignKey(
            'fk_shopCart_user_id',
            'shopCart',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
*/

        // creates index for column `product_id`
        $this->createIndex(
            'idx_shopCart_product_id',
            'shopCart',
            'product_id'
        );
/*
        // add foreign key for table `product`
        $this->addForeignKey(
            'fk_shopCart_product_id',
            'shopCart',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );
*/
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk_shopCart_user_id',
            'shopCart'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx_shopCart_user_id',
            'shopCart'
        );

        // drops foreign key for table `product`
        $this->dropForeignKey(
            'fk_shopCart_product_id',
            'shopCart'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            'idx_shopCart_product_id',
            'shopCart'
        );

        $this->dropTable('shopCart');
    }
}
