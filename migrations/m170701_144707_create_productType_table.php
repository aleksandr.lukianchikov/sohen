<?php

use yii\db\Migration;

/**
 * Handles the creation of table `productType`.
 * Has foreign keys to the tables:
 *
 * - `producer`
 */
class m170701_144707_create_productType_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('productType', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'id_parent' => $this->integer(),
            'producer_id' => $this->integer(),
        ]);

        // creates index for column `producer_id`
        $this->createIndex(
            'idx_productType_producer_id',
            'productType',
            'producer_id'
        );
/*
        // add foreign key for table `producer`
        $this->addForeignKey(
            'fk_productType_producer_id',
            'productType',
            'producer_id',
            'producer',
            'id',
            'CASCADE'
        );
*/
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `producer`
        $this->dropForeignKey(
            'fk_productType_producer_id',
            'productType'
        );

        // drops index for column `producer_id`
        $this->dropIndex(
            'idx_productType_producer_id',
            'productType'
        );

        $this->dropTable('productType');
    }
}
