<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notification_text_translation`.
 */
class m171126_145121_load_passwords extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $users = (new \yii\db\Query())
            ->from('user')
            ->all();

        foreach ($users as $user) {
            $newPassword = Yii::$app->security->generatePasswordHash($user['password']);
            Yii::$app->db->createCommand()
                ->update('user', ['password' => $newPassword], 'id = :id', [':id' => $user['id']])
                ->execute();
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        return true;
    }
}
