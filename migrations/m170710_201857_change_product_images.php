<?php

use yii\db\Migration;

/**
 * Handles dropping user_status from table `user`.
 */
class m170710_201857_change_product_images extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->alterColumn('product', 'image_1', 'VARCHAR(128) DEFAULT NULL');
        $this->alterColumn('product', 'image_2', 'VARCHAR(128) DEFAULT NULL');
        $this->alterColumn('product', 'image_3', 'VARCHAR(128) DEFAULT NULL');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->alterColumn('product', 'image_1', 'BLOB');
        $this->alterColumn('product', 'image_2', 'BLOB');
        $this->alterColumn('product', 'image_3', 'BLOB');
    }
}
