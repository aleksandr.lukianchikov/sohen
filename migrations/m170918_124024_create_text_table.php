<?php

use yii\db\Migration;

/**
 * Handles the creation of table `text`.
 */
class m170918_124024_create_text_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('text', [
            'id' => $this->primaryKey(),
            'producer_id' => $this->integer(),
            'text_1' => $this->text(),
            'text_2' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('text');
    }
}
