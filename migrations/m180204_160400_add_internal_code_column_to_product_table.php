<?php

use yii\db\Migration;

/**
 * Handles adding internal_code to table `product`.
 */
class m180204_160400_add_internal_code_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'internal_code', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'internal_code');
    }
}
