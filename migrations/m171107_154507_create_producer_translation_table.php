<?php

use yii\db\Migration;

/**
 * Handles the creation of table `producer_translation`.
 */
class m171107_154507_create_producer_translation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('producer_translation', [
            'id' => $this->primaryKey(),
            'producer_id' => $this->integer(),
            'language' => 'VARCHAR(6) NOT NULL',
            'name' => 'VARCHAR(255) NOT NULL',
            'address' => 'VARCHAR(250)',
            'slogan' => 'VARCHAR(250)',
            'about' =>  $this->text(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('producer_translation');
    }
}
