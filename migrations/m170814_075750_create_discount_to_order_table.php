<?php

use yii\db\Migration;

/**
 * Handles the creation of table `discount_to_order`.
 */
class m170814_075750_create_discount_to_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('discount_to_order', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'discount_id' => $this->integer(),
            'sum' => $this->float(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('discount_to_order');
    }
}
