<?php

use yii\db\Migration;

/**
 * Handles the creation of table `discount`.
 * Has foreign keys to the tables:
 *
 * - `product`
 * - `product`
 * - `user`
 */
class m170701_151755_create_discount_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('discount', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'type' => "ENUM('user_assoc', 'prod_assoc', 'sum_assoc', 'feast_assoc')",
            'product_id' => $this->integer(),
            'min_qnt_product' => $this->integer(),
            'percent' => $this->integer(),
            'qnt_product_gift' => $this->integer(),
            'product_gift_id' => $this->integer(),
            'user_id' => $this->integer(),
            'sum' => $this->integer(),
            'date_start' => $this->date(),
            'date_end' => $this->date(),
            'image' => $this->binary(),
        ]);

        // creates index for column `product_id`
        $this->createIndex(
            'idx_discount_product_id',
            'discount',
            'product_id'
        );
/*
        // add foreign key for table `product`
        $this->addForeignKey(
            'fk_discount_product_id',
            'discount',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );
*/
        // creates index for column `productGift_id`
        $this->createIndex(
            'idx_discount_product_gift_id',
            'discount',
            'product_gift_id'
        );

 /*       // add foreign key for table `product`
        $this->addForeignKey(
            'fk_discount_product_gift_id',
            'discount',
            'product_gift_id',
            'product',
            'id',
            'CASCADE'
        );
*/
        // creates index for column `user_id`
        $this->createIndex(
            'idx_discount_user_id',
            'discount',
            'user_id'
        );
/*
        // add foreign key for table `user`
        $this->addForeignKey(
            'fk_discount_user_id',
            'discount',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
*/
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `product`
        $this->dropForeignKey(
            'fk_discount_product_id',
            'discount'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            'idx_discount_product_id',
            'discount'
        );

        // drops foreign key for table `product`
        $this->dropForeignKey(
            'fk_discount_product_gift_id',
            'discount'
        );

        // drops index for column `productGift_id`
        $this->dropIndex(
            'idx_discount-product_gift_id',
            'discount'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk_discount_user_id',
            'discount'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx_discount_user_id',
            'discount'
        );

        $this->dropTable('discount');
    }
}
