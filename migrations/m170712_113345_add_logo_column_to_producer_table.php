<?php

use yii\db\Migration;

/**
 * Handles adding logo to table `producer`.
 */
class m170712_113345_add_logo_column_to_producer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('producer', 'logo', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('producer', 'logo');
    }
}
