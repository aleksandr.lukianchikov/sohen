<?php

use yii\db\Migration;

/**
 * Handles adding producer_id to table `product_return`.
 */
class m170711_212035_add_producer_id_column_to_product_return_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product_return', 'producer_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product_return', 'producer_id');
    }
}
