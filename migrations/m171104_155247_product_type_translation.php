<?php

use yii\db\Migration;

class m171104_155247_product_type_translation extends Migration
{
    public function safeUp()
    {
        $this->createTable('product_type_translation', [
            'id' => 'pk',
            'product_type_id' => 'INT NOT NULL',
            'language' => 'VARCHAR(6) NOT NULL',
            'name' => 'VARCHAR(255) NOT NULL',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('product_type_translation');
        return true;
    }
}
