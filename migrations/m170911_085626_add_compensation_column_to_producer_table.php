<?php

use yii\db\Migration;

/**
 * Handles adding compensation to table `producer`.
 */
class m170911_085626_add_compensation_column_to_producer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('producer', 'compensation', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('producer', 'compensation');
    }
}
