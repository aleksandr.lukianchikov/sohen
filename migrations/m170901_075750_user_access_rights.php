<?php

use yii\db\Migration;

/**
 * Handles the creation of table `discount_to_order`.
 */
class m170901_075750_user_access_rights extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
      $this->addColumn('user', 'access_rights', 'TEXT');
      $accessRights = json_encode(['user']);
      $this->update('user', ['access_rights' => $accessRights]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'access_rights');
    }
}
