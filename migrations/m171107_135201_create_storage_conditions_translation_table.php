<?php

use yii\db\Migration;

/**
 * Handles the creation of table `storage_conditions_translation`.
 */
class m171107_135201_create_storage_conditions_translation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('storage_conditions_translation', [
            'id' => $this->primaryKey(),
            'storage_conditions_id' => $this->integer(),
            'language' => 'VARCHAR(6) NOT NULL',
            'name' => 'VARCHAR(255) NOT NULL',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('storage_conditions_translation');
    }
}
