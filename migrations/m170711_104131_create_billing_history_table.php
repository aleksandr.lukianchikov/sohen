<?php

use yii\db\Migration;

/**
 * Handles the creation of table `billing_history`.
 */
class m170711_104131_create_billing_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('billing_history', [
            'id' => $this->primaryKey(),
            'date' => $this->date(),
            'billing_type_id' => $this->integer(),
            'user_id' => $this->integer(),
            'producer_id' => $this->integer(),
            'amount' => $this->float(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('billing_history');
    }
}
