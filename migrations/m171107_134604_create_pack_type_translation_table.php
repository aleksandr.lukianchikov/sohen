<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pack_type_translation`.
 */
class m171107_134604_create_pack_type_translation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pack_type_translation', [
            'id' => $this->primaryKey(),
            'pack_type_id' => $this->integer(),
            'language' => 'VARCHAR(6) NOT NULL',
            'name' => 'VARCHAR(255) NOT NULL',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pack_type_translation');
    }
}
