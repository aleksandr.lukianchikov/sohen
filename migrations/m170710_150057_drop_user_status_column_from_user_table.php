<?php

use yii\db\Migration;

/**
 * Handles dropping user_status from table `user`.
 */
class m170710_150057_drop_user_status_column_from_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('user', 'user_status');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('user', 'user_status', ENUM('user', 'prod_admin', 'admin'));
    }
}
