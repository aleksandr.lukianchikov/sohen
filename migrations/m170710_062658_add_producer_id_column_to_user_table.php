<?php

use yii\db\Migration;

/**
 * Handles adding producer_id to table `user`.
 */
class m170710_062658_add_producer_id_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'producer_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'producer_id');
    }
}
