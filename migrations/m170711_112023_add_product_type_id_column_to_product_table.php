<?php

use yii\db\Migration;

/**
 * Handles adding product_type_id to table `product`.
 */
class m170711_112023_add_product_type_id_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'product_type_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'product_type_id');
    }
}
