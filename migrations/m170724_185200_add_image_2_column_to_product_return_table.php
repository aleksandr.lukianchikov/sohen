<?php

use yii\db\Migration;

/**
 * Handles adding image_2 to table `product_return`.
 */
class m170724_185200_add_image_2_column_to_product_return_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product_return', 'image_2', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product_return', 'image_2');
    }
}
