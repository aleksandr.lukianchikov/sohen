<?php

use yii\db\Migration;

/**
 * Handles adding number_hause to table `user`.
 */
class m170908_125250_add_number_hause_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'number_house', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'number_house');
    }
}
