<?php

use yii\db\Migration;

/**
 * Handles the creation of table `billing_return`.
 */
class m170711_095543_create_billing_return_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('billing_return', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'producer_id' => $this->integer(),
            'amount' => $this->float(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('billing_return');
    }
}
