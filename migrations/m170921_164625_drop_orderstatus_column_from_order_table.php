<?php

use yii\db\Migration;

/**
 * Handles dropping orderstatus from table `order`.
 */
class m170921_164625_drop_orderstatus_column_from_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('order', 'orderstatus');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('order', 'orderstatus', "ENUM('created', 'paid', 'delivered', 'in_the_way')");
    }
}
