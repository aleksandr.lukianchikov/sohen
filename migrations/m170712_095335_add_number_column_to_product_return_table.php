<?php

use yii\db\Migration;

/**
 * Handles adding number to table `product_return`.
 */
class m170712_095335_add_number_column_to_product_return_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product_return', 'number', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product_return', 'number');
    }
}
