<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orderProduct`.
 * Has foreign keys to the tables:
 *
 * - `order`
 * - `product`
 */
class m170701_155001_create_orderProduct_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('orderProduct', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'product_id' => $this->integer(),
            'amount' => $this->integer(),
            'sum' => $this->integer(),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            'idx_orderProduct_order_id',
            'orderProduct',
            'order_id'
        );
/*
        // add foreign key for table `order`
        $this->addForeignKey(
            'fk_orderProduct_order_id',
            'orderProduct',
            'order_id',
            'order',
            'id',
            'CASCADE'
        );
*/

        // creates index for column `product_id`
        $this->createIndex(
            'idx_orderProduct_product_id',
            'orderProduct',
            'product_id'
        );

 /*       // add foreign key for table `product`
        $this->addForeignKey(
            'fk_orderProduct_product_id',
            'orderProduct',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );
 */
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `order`
        $this->dropForeignKey(
            'fk_orderProduct_order_id',
            'orderProduct'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            'idx_orderProduct_order_id',
            'orderProduct'
        );

        // drops foreign key for table `product`
        $this->dropForeignKey(
            'fk_orderProduct_product_id',
            'orderProduct'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            'idx_orderProduct_product_id',
            'orderProduct'
        );

        $this->dropTable('orderProduct');
    }
}
