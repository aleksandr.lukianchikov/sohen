<?php

use yii\db\Migration;

class m170929_155247_rename_discount_to_order_table_to_order_discount_table extends Migration
{
    public function safeUp()
    {
        $this->renameTable('discount_to_order','order_discount');
    }

    public function safeDown()
    {
        $this->renameTable('order_discount','discount_to_order');

        return false;
    }
}
