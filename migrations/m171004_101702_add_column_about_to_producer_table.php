<?php

use yii\db\Migration;

class m171004_101702_add_column_about_to_producer_table extends Migration
{
    public function up()
    {
        $this->addColumn('producer', 'about', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('producer', 'about');
    }
}
