
<?php

use yii\db\Migration;

class m170722_224707_change_productType_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->alterColumn('product_type', 'parent_id', 'INT(11) NOT NULL DEFAULT 0');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        return true;
    }
}
