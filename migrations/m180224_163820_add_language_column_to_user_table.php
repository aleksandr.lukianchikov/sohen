<?php

use yii\db\Migration;

/**
 * Handles adding language to table `user`.
 */
class m180224_163820_add_language_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->update('user', ['language' => 0], 'language IS NULL');
        $this->alterColumn('user', 'language', $this->integer(2)->defaultValue(0)->notNull());
        $this->alterColumn('notification_text_translation', 'language', $this->integer(2));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->alterColumn('user', 'language', $this->boolean());
    }
}
