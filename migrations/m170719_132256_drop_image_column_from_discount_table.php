<?php

use yii\db\Migration;

/**
 * Handles dropping image from table `discount`.
 */
class m170719_132256_drop_image_column_from_discount_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('discount', 'image');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('discount', 'image', $this->binary());
    }
}
