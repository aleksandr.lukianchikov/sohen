<?php

use yii\db\Migration;

class m170724_184817_renameColumn_product_return extends Migration
{
    public function up()
    {
        $this->renameColumn('product_return', 'reason_foto', 'image_1');
    }

    public function down()
    {
        $this->renameColumn('product_return', 'image_1', 'reason_foto');
        return false;
    }
}
