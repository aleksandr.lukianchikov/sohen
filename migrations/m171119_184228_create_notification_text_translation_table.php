<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notification_text_translation`.
 */
class m171119_184228_create_notification_text_translation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('notification_text_translation', [
            'id' => $this->primaryKey(),
            'notification_text_id' => $this->integer(),
            'language' => 'VARCHAR(6) NOT NULL',
            'text_1' => $this->text(),
            'text_2' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('notification_text_translation');
    }
}
