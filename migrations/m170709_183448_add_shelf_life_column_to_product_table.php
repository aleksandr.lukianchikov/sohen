<?php

use yii\db\Migration;

/**
 * Handles adding shelf_life to table `product`.
 */
class m170709_183448_add_shelf_life_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('product', 'shelf_life', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('product', 'shelf_life');
    }
}
