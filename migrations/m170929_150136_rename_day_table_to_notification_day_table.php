<?php

use yii\db\Migration;

class m170929_150136_rename_day_table_to_notification_day_table extends Migration
{
    public function safeUp()
    {
        $this->renameTable('day','notification_day');
    }

    public function safeDown()
    {
        $this->renameTable('notification_day','notification-day');

        return false;
    }
}
