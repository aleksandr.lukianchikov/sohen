<?php

use yii\db\Migration;

/**
 * Handles the creation of table `day`.
 */
class m170913_083228_create_day_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('day', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'producer_id' => $this->integer(),
            'reminder_day' => $this->string(),
            'order_day' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('day');
    }
}
