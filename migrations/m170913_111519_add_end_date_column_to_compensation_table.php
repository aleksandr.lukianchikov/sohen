<?php

use yii\db\Migration;

/**
 * Handles adding end_date to table `compensation`.
 */
class m170913_111519_add_end_date_column_to_compensation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('compensation', 'end_date', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('compensation', 'end_date');
    }
}
