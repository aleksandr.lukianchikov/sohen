<?php

use yii\db\Migration;

/**
 * Handles adding discription to table `discount`.
 */
class m170804_201344_add_discription_column_to_discount_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('discount', 'discription', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('discount', 'discription');
    }
}
