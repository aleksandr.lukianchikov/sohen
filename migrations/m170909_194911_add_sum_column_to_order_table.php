<?php

use yii\db\Migration;

/**
 * Handles adding sum to table `order`.
 */
class m170909_194911_add_sum_column_to_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('order', 'sum', $this->float());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('order', 'sum');
    }
}
