<?php

use yii\db\Migration;

/**
 * Handles the creation of table `reason`.
 */
class m170711_103156_create_reason_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('reason', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('reason');
    }
}
