<?php

return [
    'Английский' => 'אנגלית',
    'Адрес доставки' => 'כתובת למשלוח',
    'Акции' => 'הנחות ומבצעים',
    'АКЦИЯ' => 'מבצע',

    'БАРКОД' => 'ברקוד:',
    'Баркод' => 'ברקוד',

    'В НАЛИЧИИ:' => 'במלאי:',
    'В наличии' => 'במלאי',
    'В ПОДАРОК' => 'במתנה',
    'Вернуться к выбору производителя' => 'חזרה לבחירת היצרן',
    'В корзину' => 'הוסף',
    'Вернуться в корзину' => 'חזרה לסל',
    'Вернуться в магазин' => 'חזרה לחנות',
    'Внести изменения: ' => 'לשנות   ',
    'Внести изменения' => 'לשנות',
    'Внесение изменений' => 'לשנות   ',
    'Внесение изменений: ' => 'לשנות   ',
    'Внесение изменений в дни получения напоминаний для юзера: ' =>  ':ביצוע שינויים בימים של קבלת תזכורות עבור לקוח',
    'Внести изменения в акцию: ' => 'לשנות המבצע  ',
    'Внести изменения в данные: ' => 'לשנות   ',
    'Внутренний код' => 'קוד פנימי',
    'Войти в магазин' => 'להכנס לחנות',
    'Возвраты продуктов' => 'החזרות מוצרים',
    'Возврат продукта' => 'החזרת המוצר',
    'Возвращено' => 'חזר',
    'Возврат состоялся' => 'חזר',
    'Все акции' => 'כל המבצעם',
    'Все заказы' => 'כל ההזמנות',
    'Все мои заявки на компенсации' => 'כל הבקשות שלי לפיצוי',
    'Все мои заявки на компенсацию' => 'כל הבקשות שלי לפיצוי',
    'Все заказы' => 'כל ההזמנות',
    'Все заявки на компенсации' => 'כל הבקשות לפיצוי',
    'Все тексты' => 'כל הטקסטים',
    'Вход' => 'כניסה',
    'Выход' => 'יציאה',
    'Выберите...' => 'בחר...',
    'Выберите производителя' => 'בחר יצרן',
    'Выберите продукт' => 'בחר מוצר',
    'Выберите действие' => 'בחר פעולה',
    'Выбери производителя' => 'בחר יצרן',
    'Виртуальный кошелек' => 'כיס הוירטואלי',
    'Выберите нужное' => 'לבחור את מה שצריך',


    'Главная' => ' דף הבית',
    'Город' => 'העיר',
    ' грамм' => ' גרם',
    'Грамм в упаковке' => 'גרם לכל אריזה',

    'Добавление в слайдер просто картинок с текстом' => 'הוספת בסלידר רק תמונות עם טקסט',
    'Дата заказа' => 'תאריך הזמנה',
    'ДАТА НАЧАЛА АКЦИИ:' => 'תאריך התחלה המבצע',
    'ДАТА ОКОНЧАНИЯ АКЦИИ:' => 'תאריך סיום המבצע',
    'Дата начала' => 'תאריך התחלה',
    'Дата окончания' => 'תאריך סיום',
    'Дата подачи заявки' => 'תאריך היישום',
    'Дата подтверждения' => 'תאריך האישור',
    'День накануне' => 'יום קודם',
    'День заказа' => 'יום ההזמנה',
    'Дни' => 'ימים',
    'Дни получения напоминаний' => 'ימי תזכורת',

    'Заказы' => 'הזמנות',
    'Заказ' => 'הזמנה',
    'Заказ №' => 'הזמנה מספר ',
    'Зарегистрироваться' => 'הרשם',
    'Заявки на возврат продукта' => 'בקשות להחזרה',
    'Заявка на возврат № ' => 'בקשה להחזרה מספר',
    'Заявки на возврат' => 'בקשות להחזרה',
    'Заявки на компенсацию' => 'כל בקשות לפיצוי',
    'Заявки на компенсацию за непроданный товар с истекающим сроком хранения' => 'בקשות לפיצוי על מוצרים שלא נמכרו עם תקופת אחסון שפג תוקפה',

    'Иврит' => 'עברית',
    'и' => ' ',
    'Изменить исходные данные продукта: ' => 'ערוך נתוני מוצר:',
    'Изменить' => 'לשנות',
    'Изменения заказа: ' => 'לערוך ההזמנה',
    'Изменить текст: ' => 'שנות טקסט',

    'История заказов' => 'היסטוריה הרכישה',
    'История заявок на компенсацию' =>' היסטוריה של פיצוים',
    'История возвратов' => 'היסטוריה של חזרות המוצרים',
    'итого до акций: ' => 'סך הכל לפני מבצעים  ',
    'итого: ' => 'סך הכל ',
    'ИТОГО: ' => 'סך הכל ',

    'Калорийность' => 'קלוריות',
    'Картинка' => 'תמונה',
    'Картинка 1' => 'תמונה 1',
    'Картинка 2' => 'תמונה 2',
    'Картинка 3' => 'תמונה 3',
    'Количество упаковок' => 'מספר חבילות',
    'КОЛИЧЕСТВО ТОВАРА ПО СКИДКЕ:' => 'כמות המוצר שאפשר לקבל במבצע',
    'КОЛИЧЕСТВО ТОВАРОВ В ПОДАРОК:' => 'מספר חבילות במתנה',
    'Количество продукта которое можно получить по акции' => 'כמות המוצר שאפשר לקבל במבצע',
    'Количество штук необходимое для участия' => 'מספר חבילות שדרוש בשביל להשתתף',
    'Компенсации' => 'פיצויים',
    'КОМУ' => 'למי',
    'Контакты' => 'צור קשר',
    'КОНТАКТНЫЕ ДАННЫЕ' => 'צור קשר',
    'КОШЕРНОСТЬ:' => 'כשרות:',
    'Кошерность' => 'כשרות',
    'Кабинет администратора' => 'שולחן עבודה של אדמין',
    'Кабинет администратора фирмы' => '   שולחן עבודה של אדמין של חברה',
    'корневой тип' => 'סוג שורשי',
    'Корневой тип' => 'סוג שורשי',

    'Личный кабинет' => 'שלחן עבודה',
    'Личный кабинет представителя магазина' => ' שולחן עבודה של נציג החנות',

    'МИНИМАЛЬНОЕ КОЛИЧЕСТВО УПАКОВОК:' => 'מספר חבילות שדרוש בשביל להשתתף',
    'Мой личный кабинет' => ' שולחן עבודה',
    'Моя корзина' => 'סל הקניות שלי',
    'Мои заказы' => 'ההזמנות שלי',
    'Мои клиенты' => 'הלקוחות שלי',

    'Название акции на русском' => 'שם המבצע ברוסית',
    'Название акции на иврите' => 'שם המבצע בעברית',
    'Название типа продукта на русском' => 'סוג המוצר ברוסית',
    'Название типа продукта на иврите' => 'סוג המוצר בעברית',
    'Название продукта на русском' => 'שם המוצר ברוסית',
    'Название  продукта на иврите' => 'שם המוצר בעברית',
    'Неутвержденные покупатели' => 'קונים שלא אושרו',
    'Название магазина' => 'שם החנות',
    'Номер дома' => 'מספר הבניין',
    'Не выбрано' => 'לא נבחר',
    'Не выбрано...' => 'לא נבחר',
    'не задано' => 'לא הוגדר',
    'Номер телефона' => 'מספר טלפון',
    'Новинка' => 'חידוש',

    'Обновить корзину' => 'עדכן את הסל',
    'О нас' => 'אודות',
    'О НАС' => 'אודות',
    'ОПИСАНИЕ:' => 'תיאור',
    'ОПЛАТИТЬ' => 'לשלם',
    'ОТПРАВИТЬ' => 'לשלוח',
    'Описание акции на русском' => 'תיאור המבצע ברוסית',
    'Описание акции на иврите' => 'תיאור המבצע בעברית',
    'Описание на русском' => 'תיאור ברוסית',
    'Описание продукта на иврите' => 'תיאור בעברית',
    'ОТПРАВИТЬ E_MAIL' => 'לשלוח e-mail',
    'ОТ КОГО' => 'ממי',
    'Оформить компенсацию за непроданный товар у которого заканчивается срок хранения' => 'רישום בקשה פיצוי על מוצרים שלא נמכרו עם תקופת אחסון שפג תוקפה',
    'Оформить возврат товара' => 'רישום בקשה להחזרת המוצר',
    'ОФОРМИТЬ ЗАКАЗ' => 'להזמין',

    'Пароль' => 'סיסמה',
    'подробнее' => 'מידע נוסף',
    'Подтверждение пароля' => 'אימות סיסמה',
    'Подтверждено' => 'אושר',
    'Поиск' => 'חיפוש',
    'Покупатель' => 'קונה',
    'Покупатели' => 'לקוחות',
    'Покупатель- обладатель персональной скидки' => 'לקוח בעל הנחה אישית',
    'Причина' => 'סיבה לחזור',
    'ПРОДУКТ ПО АКЦИИ:' => 'מוצר תחת הנחה',
    'ПРОИЗВОДИТЕЛЬ:' => 'יצרן',
    'Производитель' => 'יצרן',
    'Процент' => 'אחוז',
    'Процент возврата' => 'אחוז ההחזרה',
    'Права доступа' => 'זכויות גישה',
    'Продукт' => 'המוצר',
    'Продукт который нужно купить чтобы участвовать в акции' => 'מוצר שדרוש לקנות בשביל להשתתף במבצע',
    'Продукт на который скидка' => 'מוצר תחת מבצע',
    'Продукты' => 'מוצרים',
    'Покупатели и дни недели получения напоминаний' => 'קונים וימים של קבלת תזכורות',

    'Русский' => 'רוסית',
    'Регистрационные данные' => 'נתוני הרישום שלי',
    'Работа с продуктами' => 'עבודה עם מוצרים',
    'Работа с акциями' => 'עבודה עם הנחות ומבצעים ',
    'Работа с типом продукта' => 'עבודה עם סוג המוצר',
    'Работа с текстами сообщений' => 'עבודה עם טקסטים של הודעות',
    'Работа с заявками на возврат продукта' => 'עבודה עם בקשות להחזרת המוצר',
    'Работа с заявками на компенсацию за непроданный товар с истекающим сроком хранения' => 'עבודה עם בקשות פיצוים על מוצרים שלא נמכרו עם תקופת אחסון שפג תוקפה',
    'Разрешение на получение смс' => 'אישור לקבלת sms',
    'Разрешение на получение email' => 'אישור לקבלת email',
    'Редактировать: ' => 'ערוך: ',
    'Редактировать' => 'ערוך',
    'Редактировать заявку на компенсацию №: ' => 'ערוך את הבקשה לפיצוי מספר:',
    'Редактировать заявки на компенсацию №: ' => 'ערוך את הבקשה לפיצוי מספר:',
    'Редактирование типа продукта: ' => 'עריכת סוג מוצר',

    'Редактирование типа продукта:' => 'עריכת סוג מוצר',
    'Родительский тип' => 'סוג האב',

    'Сбросить фильтр' => 'נקה',
    'Сброс' => 'נקה',
    'СКИДКА В ПРОЦЕНТАХ:' => 'הנחה באחוזים',
    'Создать' => 'ליצור',
    'Создание нового продукта' => 'צור מוצר חדש',
    'Создать новую акцию' => 'צור מבצע חדשה',
    'Создать акцию' => 'צור מבצע',
    'Создать новую картинку' => 'צור תמונה חדשה',
    'Создать новый тип' => 'ליצור סוג חדש',
    'Создать заявку на возврат продукта' => 'צור תביעה להחזרת המוצר',
    'Создать заявку на компенсацию' => 'צור בקשות לפיצוי',
    'Создать заявку на компенсацию за непроданный товар с истекающим сроком хранения' => 'ליצור בקשה פיצוי על מוצרים שלא נמכרו עם תקופת אחסון שפג תוקפה',
    'Создать покупателю дни получения напоминаний' => 'צור יום תזכורת עבור הלקוח',
    'Создать текст' => 'צור טקסט',
    'Создать новый текст' => 'צור טקסט חדש',
    'СРОК ХРАНЕНИЯ:' => 'חיי מדף:',
    'Срок хранения' => 'חיי מדף',
    'Срок хранения заканчивается' => 'חיי המדף מסתיימים',
    ' суток. ' => ' ימים ',
    'СОСТАВ:' => 'רכיבים:',
    'Состав на русском' => 'רכיבים ברוסית',
    'Состав продукта на иврите' => 'רכיבים בעיברית',
    'Сохранить' => 'שמור',
    'Статус заказа' => 'סטטוס הזמנה',
    'Сумма' => 'סכום',
    'Сумма при покупке на которую есть скидка' => 'סכום הקניה הדרוש כדי להשתתף במבצע',
    'Статус юзера' => 'סטטוס לקוח',
    'Статус' => 'סטטוס',

    'Тексты' => 'טקסטים',
    'Текст уведомления 1' => 'טקסט התראה 1',
    'Текст уведомления 2' => 'טקסט התראה 2',
    'Текст уведомления 1 на русском' => ' טקסט התראה 1 ברוסית',
    'Текст уведомления 2 на русском' => 'טקסט התראה 2 ברוסית',
    'Текст уведомления 1 на иврите' => 'טקסט התראה 1 בעברית',
    'Текст уведомления 2 на иврите' => 'טקסט התראה 2 בעברית',
    'Tексты' => 'טקסטים',
    'Тип акции' => 'סוג המבצע',
    'ТИП АКЦИИ:' => 'סוג המבצע',
    'ТИП УПАКОВКИ:' => 'סוג האריזה:',
    'Тип упаковки' => 'סוג האריזה',
    'ТИП ПРОДУКТА:' => 'סוג המוצר:',
    'Тип продукта' => 'סוג המוצר',
    'Текущая корзина' => 'סל שוטף',
    'Телефон' => 'מספר טלפון',
    'Типы продуктов' => 'סוגים המוצרים',
    'Только актуальные' => 'רק אקטואלי',

    'Укажите количество товара' => 'ציינו את כמות הפריטים',
    'УСЛОВИЯ ХРАНЕНИЯ' => 'תנאי אחסון:',
    'Условия хранения' => 'תנאי אחסון',
    'УСЛОВИЕ УЧАСТИЯ В АКЦИИ - ПОКУПКА ПРОДУКТА:' => 'מוצר שדרוש לקנות בשביל להשתתף במבצע',
    'Улица' => 'הרחוב',
    'Удалить' => 'למחוק',

    'Фильтры' => 'סינון',
    ' Фирма-производитель' => 'חברה יצרן',

    'ЦЕНА:' => 'מחיר',
    'Цена' => 'מחיר',
    'шек.' => ' &#8362 ', //₪


    'ЭКОНОМЬ С НАМИ!' => 'ותחסוך איתנו',

    'Язык' => 'שפה',
    'Язык коммуникации' => 'שפת תקשורת',

    '№ заказа' => 'מספר הזמנה',
    '№ заявки' => 'מספר הבקשה',
    '№ заявки ' => 'מספר הבקשה',
    'ОТМЕНИТЬ' => 'לבטל',

    'числа. ' => 'התאריך',
    'СОЗДАТЬ ТАКОЙ ЖЕ ЗАКАЗ В НОВОЙ КОРЗИНЕ' => 'חזור אותה הזמנה בסל חדש',
    'от' => 'מ',




    ' шек.' => ' &#8362',
    'Все скидки, акции' => 'כל המבצעים וההנחות',
    'Если у вас есть вопросы, пожалуйста, заполните эту форму чтобы связаться с нами' => 'אם יש לך שאלות עסקיות או שאלות אחרות, מלא את הטופס הבא כדי ליצור איתנו קשר. תודה.',
    'Контакты' => 'צור קשר',
    'Отправить' => 'שלח',

    'Редактирование заявки на возврат №' => 'ערוך את בקשה למחזירה מספר',
    'Редактирование' => 'ערוך',
    '№ заказа ' => 'מספר הזמנה',
    'Еще не утвержденные покупатели' => 'עדיין לא אושרה קונים',

    'Напоминание о заказе' => 'עדיין לא אושרה קונים',
    'Получен новый заказ' => 'עדיין לא אושרה קונים',

];
