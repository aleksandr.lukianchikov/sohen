<?php

return [
    'Главная' => 'Home',
    'О нас' => 'About us',
    'Контакты' => 'Contacts',
    'Вход' => 'Login',
    'Мой личный кабинет' => 'Cabinet',
    'Выход' => 'Logout',
    'Язык' => 'Language',
    'Русский' => 'Russian',
    'Английский' => 'English',
    'Иврит' => 'Hebrew',
];
