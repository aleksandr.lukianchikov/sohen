<?php

namespace app\commands;

use app\models\enums\Language;
use yii\console\Controller;
use yii\db\Query;
use \Yii;


class NotificationController extends Controller
{
    public function actionIndex()
    {

 /*       $postfields = array(
            'Username' => 'bramnik@hotmail.com',
            'Password' => '8c46ade4-4e4a-4421-bf36-c19ad37845c9',
            'MsgName'=> 'Hello',
            'GlobalID'=> '-1',
            'Msg'=> 'Глентвейн остывает!!!',
            'FromMobile'=> '0544843322',
            'Mobiles'=> '0545934322'
        );

        // open a curl connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,
            'http://www.slng6.com/WebService/SendSms.asmx/CreateAndSendSmsToMultiContactsByMobileNo?'.http_build_query($postfields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        // например тут мы вместо отправки сообщения сохраняем в файл для демонстрации
        $testFile = \Yii::getAlias('@runtime') . '/sms.log';
        file_put_contents($testFile, $result . PHP_EOL, FILE_APPEND);
*/
        $notifications = (new Query())
            ->select('notification_day.reminder_day, notification_day.user_id, notification_text.text_1 AS reminder_message, user.tel')
            ->from('notification_day')
            ->innerJoin('notification_text', 'notification_text.producer_id = notification_day.producer_id')
            ->innerJoin('user', 'user.id = notification_day.user_id')
            ->all();
        $currentDay = date('l');

        foreach ($notifications as $notification) {
            $day = $notification['reminder_day'];
            if ($day != $currentDay) {
                continue;
            }

            $phone = $notification['tel'];
            $message = $notification['reminder_message'];
            $postfields = array(
                'Username' => 'bramnik@hotmail.com',
                'Password' => '8c46ade4-4e4a-4421-bf36-c19ad37845c9',
                'MsgName'=> 'Hello',
                'GlobalID'=> '-1',
                'Msg'=> $message,
                'FromMobile'=> '0544843322',
                'Mobiles'=> $phone
            );

            // open a curl connection
           $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,
                'http://www.slng6.com/WebService/SendSms.asmx/CreateAndSendSmsToMultiContactsByMobileNo?'.http_build_query($postfields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);

            // Например для сервиса http://smsc.ru
            // Чтобы отправить сообщение нужно дернуть ссылку
            // http://smsc.ru/sys/send.php?login=<login>&psw=<password>&phones=<phones>&mes=<message>
            // Дергаем ссылку либо встроенной функцией curl (см. документацию)
            // А также можно пользоваться различными библиотеками такими как HttpClient

            $CurDate = new \DateTime('now', new \DateTimeZone('Asia/Jerusalem'));           // например тут мы вместо отправки сообщения сохраняем в файл для демонстрации
            $testFile = \Yii::getAlias('@runtime') . '/sms.log';
            file_put_contents($testFile, $CurDate->format('Y-m-d H:i:s') . " " . $phone . " " . $message . PHP_EOL, FILE_APPEND);
            file_put_contents($testFile, $result . PHP_EOL, FILE_APPEND);
        }
    }

    public function actionEmail()
    {
        $testFile = \Yii::getAlias('@runtime') . '/email.log';
        file_put_contents($testFile, "Email start" . PHP_EOL, FILE_APPEND);

        $notifications = (new Query())
            ->select('notification_day.reminder_day, notification_day.user_id, user.language,  notification_text.text_1 AS reminder_message, notification_text_translation.text_1 AS reminder_message_hw, user.tel, user.email')
            ->from('notification_day')
            ->innerJoin('user', 'user.id = notification_day.user_id')
            ->innerJoin('notification_text', 'notification_text.producer_id = notification_day.producer_id')
            ->innerJoin('notification_text_translation', 'notification_text_translation.notification_text_id = notification_text.id')
            ->all();
        $currentDay = date('l');

        foreach ($notifications as $notification) {
            $day = $notification['reminder_day'];
            if ($day != $currentDay) {
                continue;
            }

            $subject = Yii::t('app', 'Напоминание о заказе', [], Language::getSystemLanguage($notification['language']));
            if ($notification['language'] == Language::Hebrew) {
                $message = $notification['reminder_message_hw'];
            } else {
                $message = $notification['reminder_message'];
            }

            Yii::$app->mailer->compose('notification/reminder', ['message' => $message])
                ->setFrom(Yii::$app->params['noreplyEmail'])
                ->setTo($notification['email'])
                ->setSubject($subject)
                ->send();
            Yii::info($notification['email'], 'email');
        }
        // Аналогично, только для отправки по почте.
    }
}
