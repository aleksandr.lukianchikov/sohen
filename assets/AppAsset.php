<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use app\models\enums\LocaleEnum;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/fonts.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public static function register($view) {
        if(LocaleEnum::isRTL()) {
            $view->registerCssFile('@web/css/bootstrap-rtl.min.css', ['depends' => 'app\assets\AppAsset']);
            $view->registerCssFile('@web/css/site-rtl.css', ['depends' => 'app\assets\AppAsset']);
        }
        return parent::register($view);
    }
}
