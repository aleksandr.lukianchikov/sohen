<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\Compensation;

/* @var $this yii\web\View */
/* @var $model app\models\ProducerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="producer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'tel') ?>

    <?= $form->field($model, 'compensation')-> dropDownList(Compensation::getClientValues(), ['prompt' => 'Выберите...'])?>

    <?php // echo $form->field($model, 'address') ?>

    <?php //echo $form->field($model, 'site') ?>

    <?php // echo $form->field($model, 'account') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'slogan') ?>

    <?php // echo $form->field($model, 'image_1') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
