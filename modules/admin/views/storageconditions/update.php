<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StorageConditions */

$this->title = 'Update Storage Conditions: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Storage Conditions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="storage-conditions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
