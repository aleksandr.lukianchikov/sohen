<?php
use app\models\enums\LocaleEnum;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Изменить данные покупателя: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Покупатели', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $template = '<tr><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';
    if (LocaleEnum::isRTL()) {
        $template = '<tr><td{contentOptions}>{value}</td><th{captionOptions}>{label}</th></tr>';
    }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'template' => $template,
        'attributes' => [
           // 'id',
            'shop_name',
            'email',
            'tel',
            'delivery_address',
            'city',
        ],
    ]) ?>
    <div class="user-form" <?php if(LocaleEnum::isRTL()){ ?>  style="direction: rtl;" <?php  } ?> >
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'money_box')->textInput() ?>
    <?= $form->field($model, 'status')->dropDownList(\app\models\enums\Status::getClientValues(),['prompt'=>'Выберете нужное']) ?>
    <?= $form->field($model, 'language')->dropDownList(\app\models\enums\Language::getClientValues(),['prompt'=>'Выберете нужное']) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('Создать') : t('Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
