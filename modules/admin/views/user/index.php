<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
if($flag == 0) {
    $this->title = t('Еще не утвержденные покупатели');
}
else{
    $this->title = t('Покупатели');
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php
    if($flag == 0) {
        $icons =
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['user/view', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']), $url);
                    },

                    'update' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['user/update', 'id' => $model->id]);

                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']), $url);

                    },


                ],

            ];
    }
    else {
        $icons =
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['user/view', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']), $url);
                    },



                ],

            ];
    }
    ?>




    <?php if (LocaleEnum::isRTL()) {
    //Массив с колонками выносим в переменную для удобства
    $columns = [
        $icons,
        [
            'attribute' => 'city',
            'value' => function ($model) {
                return \app\models\enums\City::getClientValue($model->city);
            }
        ],
        [
            'attribute' => 'language',
            'value' => function ($model) {
                return \app\models\enums\Language::getClientValue($model->language);
            }
        ],
        'tel',
        'shop_name',
        ['class' => 'yii\grid\SerialColumn'],
    ];

    }
    else {
        $columns = [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'shop_name',

            'tel',
            // 'delivery_address',
            // 'money_box',
            // 'sms_notification',
            // 'email_notification:email',
            [
                'attribute' => 'city',
                'value' => function ($model) {
                    return \app\models\enums\City::getClientValue($model->city);
                }
            ],
            [
                'attribute' => 'language',
                'value' => function ($model) {
                    return \app\models\enums\Language::getClientValue($model->language);
                }
            ],
            // 'producer_id',
            // 'access_rights:ntext',
            // 'street',
            // 'number_house',

            $icons,
        ];
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,


        'columns' => $columns,
    ]);?>
</div>
