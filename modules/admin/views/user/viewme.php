<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\RegisterForm */

$this->title = $model->shop_name;

$this->params['breadcrumbs'][] =['label' => $this->title, 'url' => ['/private-office/view', 'user_id' => Yii::$app->request->get('user_id')]];
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(t('Изменить'), ['updateme', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'shop_name',
            'email',
            'tel',





        ],
    ]) ?>

</div>
