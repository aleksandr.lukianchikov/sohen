<?php
use app\models\enums\LocaleEnum;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => t('Покупатели'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">


    <p>
        <?php
        $classChangeBtn = ['class' => 'btn btn-primary'];
        if (LocaleEnum::isRTL()) {
            Html::addCssClass($classChangeBtn, 'pull-right');
        }
        ?>

        <?= Html::a(t('Изменить'), ['update', 'id' => $model->id], $classChangeBtn) ?>
        <?= Html::a(t('Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    $template = '<tr><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';
    if (LocaleEnum::isRTL()) {
        $template = '<tr><td{contentOptions}>{value}</td><th{captionOptions}>{label}</th></tr>';
    }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'template' => $template,
        'attributes' => [
           // 'id',
            'shop_name',

            'tel',
            'delivery_address',
            'money_box',
            'sms_notification',
            'email_notification:email',
            [
                'attribute' => 'city',
                'value' => function ($model) {
                    return \app\models\enums\City::getClientValue($model->city);
                }
            ],
            [
                'attribute' => 'language',
                'value' => function ($model) {
                    return \app\models\enums\Language::getClientValue($model->language);
                }
            ],
           // 'producer_id',

           // 'street',
           // 'number_house',
        ],
    ]) ?>

</div>
