<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\ProductType;
use app\models\enums\LocaleEnum;

$producerId = Yii::$app->user->identity->producer_id;
$filterProductType = Html::activeDropDownList(
    $searchModel,
    'product_type_id',
    ArrayHelper::map(ProductType::find()->where('producer_id = :p_id', [':p_id' => $producerId])->asArray()->all(), 'id', 'name'),
    ['class'=>'form-control','prompt' => t('Выберите...')]
);

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('Продукты');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?php
    $optionsBtn = ['class' => 'btn btn-primary', 'role' => 'button', 'data-toggle' => 'collapse','aria-expanded'=>'false','aria-controls'=>'collapseFilters'];
    $CreateBtn = ['class' => 'btn btn-success'];
    if (LocaleEnum::isRTL()) {
        Html::addCssClass($optionsBtn, 'pull-right');
        Html::addCssClass( $CreateBtn, 'pull-right');
    }
    ?>
    <?= Html::a(t('Создать'), ['create'],$CreateBtn) ?>
    <?=Html::a(t('Фильтры'),"#collapseFilters", $optionsBtn);?>



    <? if($searchModel->isActiveFilter()): ?>
        <?= Html::a(t('Сбросить фильтр'), [\yii\helpers\Url::to("/" .$this->context->action->getUniqueId())], ['class' => 'btn btn-default']); ?>
    <? endif; ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if("he-IL" ==  Yii::$app->language) {
        //Массив с колонками выносим в переменную для удобства
        $columns = [
            ['class' => 'yii\grid\ActionColumn'],
            [
                'attribute' => 'product_type_id',
                'filter' => $filterProductType,
                'value' => function($model){
                    return $model->getProductType()->one()->name_he;
                }
            ],


            // 'shelf_life',
            // 'kosher',
            // 'available',
            // 'status_new',
            // 'storage_conditions_id',
            // 'calories',
            // 'pack_gram',
            [
                'attribute' => 'price',
                'format' => 'raw',
                'value' => function ($model){
                    return '<b>' . Html::encode($model->price) . '</b>';
                },
            ],
            // 'composition',
            // 'discription',
            'barcode',
            'internal_code',
            'name',
            'name_he',
            ['class' => 'yii\grid\SerialColumn'],


        ];
    }
    else {
        $columns = [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            'name_he',
            'barcode',
            'internal_code',
            // 'discription',
            // 'composition',
            [
                'attribute' => 'price',
                'format' => 'raw',
                'value' => function ($model){
                    return '<b>' . Html::encode($model->price) . '</b>';
                },
            ],
            // 'pack_gram',
            // 'calories',
            // 'storage_conditions_id',
            // 'pack_type_id',
            // 'kosher',
            // 'available',
            // 'status_new',

            /*[
                'attribute' => 'producer_id',
                'filter' => false,
            ],*/

            // 'image_1',
            // 'image_2',
            // 'image_3',
            // 'shelf_life',

            [
                'attribute' => 'product_type_id',
                'filter' => $filterProductType,
                'value' => function($model){
                    return $model->getProductType()->one()->name;
                }
            ],



            ['class' => 'yii\grid\ActionColumn'],
        ];

    }
   ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => t('не задано')],

        'columns' => $columns,
    ]);?>
