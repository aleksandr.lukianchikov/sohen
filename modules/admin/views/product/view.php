<?php

use app\models\enums\LocaleEnum;
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\Kosher;
use app\models\enums\Available;
use app\models\enums\StatusNew;


/* @var $this yii\web\View */
/* @var $model app\models\Product */
if (LocaleEnum::isRTL()){
    $modelName = $model->name_he;
}
else {
    $modelName = $model->name;
}
$this->title = $modelName;
$this->params['breadcrumbs'][] = ['label' => t('Продукты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
            $classChangeBtn = ['class' => 'btn btn-primary'];
            if (LocaleEnum::isRTL()) {
                Html::addCssClass($classChangeBtn, 'pull-right');
            }
        ?>
        <?= Html::a(t('Внести изменения'), ['update', 'id' => $model->id], $classChangeBtn) ?>
        <?= Html::a(t('Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
        $template = '<tr><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';
        if (LocaleEnum::isRTL()) {
            $template = '<tr><td{contentOptions}>{value}</td><th{captionOptions}>{label}</th></tr>';
        }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'template' => $template,
        'attributes' => [
           // 'id',
            'name',
            [
                'label' => $model->getAttributeLabel('name_he'),
                'value' => te($model, 'name', 'he-IL'),
            ],
            'barcode',
            'internal_code',
            'discription',
            [
                'label' => $model->getAttributeLabel('discription_he'),
                'value' => te($model, 'discription', 'he-IL'),
            ],
            'composition',
            [
                'label' => $model->getAttributeLabel('composition_he'),
                'value' => te($model, 'composition', 'he-IL'),
            ],
            'price',
            'pack_gram',
            'calories',
            'shelf_life',

            [
                'attribute' => 'storage_conditions_id',
                'value' => function($model){
                    return $model->getStorageConditions()->one()->name;
                }
            ],

            [
                'attribute' => 'pack_type_id',
                'value' => function($model){
                    return $model->getPackType()->one()->name;
                }
            ],

            [
            'attribute' => 'product_type_id',
               'value' => function($model){
               return $model->getProductType()->one()->name;
               }
            ],

            [
                'attribute' => 'kosher',
                'value' => function ($model) {
                    return Kosher::getClientValue($model->kosher);
                }
            ],
            [
                'attribute' => 'available',
                'value' => function ($model) {
                    return Available::getClientValue($model->available);
                }
            ],

            [
                'attribute' => 'status_new',
                'value' => function ($model) {
                    return StatusNew::getClientValue($model->status_new);
                }
            ],
            [
                'attribute' => 'producer_id',
                'value' => function (\app\models\Product $model) {
                    return $model->producer->name;
                }
            ],
            [
                'attribute' => 'image_1',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_1) {
                        return Html::img('/uploads/' . $model->image_1, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'image_2',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_2) {
                        return Html::img('/uploads/' . $model->image_2, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'image_3',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_3) {
                        return Html::img('/uploads/' . $model->image_3, ['width' => '200px']);
                    }
                    return '';
                }
            ],

        ],
    ]) ?>

</div>
