<?php

use yii\helpers\Html;
use app\models\enums\LocaleEnum;


/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = t('Создание нового продукта');
$this->params['breadcrumbs'][] = ['label' => t('Продукты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
