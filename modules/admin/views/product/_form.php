<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\Kosher;
use app\models\enums\Available;
use app\models\enums\StatusNew;
use dosamigos\fileupload\FileUpload;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="product-form" >


    <?php $form = ActiveForm::begin
    ([
        'fieldConfig' => [
            'template' => "{hint}\n{error}\n{input}\n{label}\n"
        ],


]);?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'name_he')->textInput() ?>
    <?= $form->field($model, 'barcode')->textInput() ?>
    <?= $form->field($model, 'internal_code')->textInput() ?>
    <?= $form->field($model, 'product_type_id')->dropDownList($model->getProductTypeList(),['prompt'=>t('Выберите нужное'), 'encodeSpaces' => true,]) ?>
    <?= $form->field($model, 'discription')->textarea() ?>
    <?= $form->field($model, 'discription_he')->textarea() ?>
    <?= $form->field($model, 'composition')->textInput() ?>
    <?= $form->field($model, 'composition_he')->textInput() ?>
    <?= $form->field($model, 'price')->textInput() ?>
    <?= $form->field($model, 'pack_gram')->textInput() ?>
    <?= $form->field($model, 'pack_type_id')->dropDownList($model->getPackTypesList(),['prompt'=>t('Выберите нужное')]) ?>

    <?= $form->field($model, 'calories')->textInput() ?>
    <?= $form->field($model, 'shelf_life')->textInput() ?>
    <?= $form->field($model, 'storage_conditions_id')->dropDownList($model->getStorageConditionsList(),['prompt'=>t('Выберите нужное')]) ?>
    <?= $form->field($model, 'kosher')->dropDownList(Kosher::getClientValues(), ['prompt'=>t('Выберите нужное')]) ?>
    <?= $form->field($model, 'available')->dropDownList(Available::getClientValues(),['prompt'=>t('Выберите нужное')])  ?>
    <?= $form->field($model, 'status_new')->dropDownList(StatusNew::getClientValues(),['prompt'=>t('Выберите нужное')])  ?>
    <?= $form->field($model, 'image_file')->label(false)->widget(
        '\trntv\filekit\widget\Upload',
        [
            'url' => ['upload-file'],
            'sortable' => true,
            'maxFileSize' => 10 * 1024 * 1024, // 10 MiB
            'maxNumberOfFiles' => 3,
            //'clientOptions' => [ ...other blueimp options... ]
    ]
); ?>

    <div class="clearfix"></div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('Создать') : t('Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>



</div>
