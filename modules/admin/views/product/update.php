<?php

use yii\helpers\Html;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
 if("he-IL" ==  Yii::$app->language) {
    $modelName = $model->name_he;
}
else {
    $modelName = $model->name;
}
$this->title = t('Изменить исходные данные продукта: ') . $modelName;
$this->params['breadcrumbs'][] = ['label' => t('Продукты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('Внести изменения');
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
