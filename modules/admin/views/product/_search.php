<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\Kosher;
use app\models\enums\Available;
use app\models\enums\StatusNew;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?
$containerOptions = ['class' => 'collapse', 'id' => 'collapseFilters'];
if ($model->isActiveFilter()) {
    Html::addCssClass($containerOptions, 'in');
}
?>
<?php
$RtLStyle = ['direction'=> 'rtl'];
if(LocaleEnum::isRTL()){  Html::addCssStyle($containerOptions,$RtLStyle, true) ; }
?>

<?= Html::beginTag('div', $containerOptions) ?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'fieldConfig' => [
    'template'  => "{input}\n{label}\n{hint}\n{error}",],
]); ?>

    <?= $form->field($model, 'kosher')->dropDownList(Kosher::getClientValues(),['prompt' => t('Не выбрано...')]) ?>
    <?= $form->field($model, 'status_new')->dropDownList(StatusNew::getClientValues(), ['prompt' => t('Не выбрано...')]) ?>
    <?= $form->field($model, 'available')->dropDownList(Available::getClientValues(), ['prompt' => t('Не выбрано...')]) ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'product_type_id') ->dropDownList($model->getProductTypeList(),['prompt'=>t('Выберите нужное'), 'encodeSpaces' => true,])?>
    <?= $form->field($model, 'price') ?>
    <?= $form->field($model, 'barcode') ?>
    <?= $form->field($model, 'internal_code') ?>

    <?php //echo $form->field($model, 'composition') ?>

    <?php // echo $form->field($model, 'pack_gram') ?>

    <?php // echo $form->field($model, 'calories') ?>

    <?php // echo $form->field($model, 'storage_conditions_id') ?>

    <?php // echo $form->field($model, 'pack_type_id') ?>

    <?php // echo $form->field($model, 'producer_id') ?>

    <?php // echo $form->field($model, 'image_1') ?>

    <?php // echo $form->field($model, 'image_2') ?>

    <?php // echo $form->field($model, 'image_3') ?>

    <?php // echo $form->field($model, 'shelf_life') ?>

    <?php // echo $form->field($model, 'product_type_id') ?>

<div class="form-group">
    <?php
    $SearchBtn = ['class' => 'btn btn-primary'];
    $ResetBtn = ['class' => 'btn btn-default'];
    if (LocaleEnum::isRTL()) {
        Html::addCssClass($SearchBtn, 'pull-right');
        Html::addCssClass($ResetBtn, 'pull-right');
    }

    ?>

    <?= Html::submitButton(t('Поиск'),$SearchBtn) ?>
    <?= Html::resetButton(t('Сброс'), $ResetBtn) ?>


</div>

    <?php ActiveForm::end(); ?>

</div>
