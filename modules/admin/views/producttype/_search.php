<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\ProductTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<?
$containerOptions = ['class' => 'collapse', 'id' => 'collapseFilters'];
if ($model->isActiveFilter()) {
    Html::addCssClass($containerOptions, 'in');
}
?>
<?php
$RtLStyle = ['direction'=> 'rtl'];
if(LocaleEnum::isRTL()){  Html::addCssStyle($containerOptions,$RtLStyle, true) ; }
?>
<?= Html::beginTag('div', $containerOptions) ?>
<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'fieldConfig' => [
        'template'  => "{input}\n{label}\n{hint}\n{error}",],
]); ?>

    <?php //echo  $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>


    <?php   echo $form->field($model, 'parent_id') ->dropDownList($model->getProductTypeListMenu(),['prompt'=>t('Выберите нужное'), 'encodeSpaces' => true,])?>
    <?php // echo $form->field($model, 'producer_id') ?>

<div class="form-group">
    <?php
    $SearchBtn = ['class' => 'btn btn-primary'];
    $ResetBtn = ['class' => 'btn btn-default'];
    if (LocaleEnum::isRTL()) {
        Html::addCssClass($SearchBtn, 'pull-right');
        Html::addCssClass($ResetBtn, 'pull-right');
    }

    ?>

    <?= Html::submitButton(t('Поиск'),$SearchBtn) ?>
    <?= Html::resetButton(t('Сброс'), $ResetBtn) ?>


</div>

    <?php ActiveForm::end(); ?>

</div>
