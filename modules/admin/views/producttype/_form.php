<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\ProductType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-type-form"<?php if(LocaleEnum::isRTL()){ ?>  style="direction: rtl;" <?php  } ?> >

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'name_he')->textInput() ?>
    <?php   echo $form->field($model, 'parent_id') ->dropDownList($model->getProductTypeListMenu(),['prompt'=>t('Корневой тип'), 'encodeSpaces' => true,])?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('Создать') : t('Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
