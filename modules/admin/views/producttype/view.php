<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\ProductType */

$this->title = te($model, 'name');
$this->params['breadcrumbs'][] = ['label' => t('Типы продуктов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
            $classChangeBtn = ['class' => 'btn btn-primary'];
            if (LocaleEnum::isRTL()) {
                Html::addCssClass($classChangeBtn, 'pull-right');
            }
        ?>

        <?= Html::a(t('Изменить'), ['update', 'id' => $model->id], $classChangeBtn) ?>
        <?= Html::a(t('Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    $template = '<tr><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';
    if (LocaleEnum::isRTL()) {
        $template = '<tr><td{contentOptions}>{value}</td><th{captionOptions}>{label}</th></tr>';
    }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => t('корневой тип')],
        'template' => $template,
        'attributes' => [
            'name',
            [
                'label' => $model->getAttributeLabel('name_he'),
                'value' => te($model, 'name', 'he-IL'),
            ],



            [
                'attribute'=>'parent_id',
                'value' => function($model){
                    $rc = NULL;
                    $ptoductTypeModel =  $model->getProductType()->one();
                    if($ptoductTypeModel != NULL){
                        $rc = $ptoductTypeModel->name;
                    }
                    return $rc;
                }

            ],
            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],
        ],
    ]) ?>

</div>
