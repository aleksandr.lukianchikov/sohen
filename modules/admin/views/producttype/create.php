<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductType */

$this->title = t('Создать новый тип');
$this->params['breadcrumbs'][] = ['label' => t('Типы продуктов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
