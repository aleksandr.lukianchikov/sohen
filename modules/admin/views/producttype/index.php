<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('Типы продуктов');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-type-index">

       <h1  <?php if (LocaleEnum::isRTL()) {?>style ="text-align:right";<?php }?>  ><?= Html::encode($this->title) ?></h1>

    <?php
    $optionsBtn = ['class' => 'btn btn-primary', 'role' => 'button', 'data-toggle' => 'collapse','aria-expanded'=>'false','aria-controls'=>'collapseFilters'];
    $CreateBtn = ['class' => 'btn btn-success'];
    if (LocaleEnum::isRTL()) {
        Html::addCssClass($optionsBtn, 'pull-right');
        Html::addCssClass( $CreateBtn, 'pull-right');
    }
    ?>
    <?= Html::a(t('Создать'), ['create'],$CreateBtn) ?>
    <?=Html::a(t('Фильтры'),"#collapseFilters", $optionsBtn);?>
    <? if($searchModel->isActiveFilter()): ?>
        <?= Html::a(t('Сбросить фильтр'), [\yii\helpers\Url::to("/" .$this->context->action->getUniqueId())], ['class' => 'btn btn-default']); ?>
    <? endif; ?>
    <?php echo $this->render('_search', ['model' => $searchModel]);?>
    <?php if (LocaleEnum::isRTL()) {
      //Массив с колонками выносим в переменную для удобства
        $columns = [


            ['class' => 'yii\grid\ActionColumn'],


            [
                'attribute' => 'producer_id',
                'value' => function ($model) {
                    return $model->getProducer()->one()->name;
                }

            ],

            [
                'attribute' => 'parent_id',
                'value' => function ($model) {
                    $rc = NULL;
                    $ptoductTypeModel = $model->getProductType()->one();
                    if ($ptoductTypeModel != NULL) {
                        $rc = $ptoductTypeModel->name;
                    }
                    return $rc;
                }

            ],



            [
                'attribute' => 'parent_id',
                'value' => function ($model) {
                    $rc = NULL;
                    $ptoductTypeModel = $model->getProductType()->one();
                    if ($ptoductTypeModel != NULL) {
                        $rc = $ptoductTypeModel->name_he;
                    }
                    return $rc;
                }

            ],
            'name',
            'name_he',

            ['class' => 'yii\grid\SerialColumn'],

        ];
    }
    else{
        $columns = [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'name_he',
            [
                'attribute' => 'parent_id',
                'value' => function ($model) {
                    $rc = NULL;
                    $ptoductTypeModel = $model->getProductType()->one();
                    if ($ptoductTypeModel != NULL) {
                        $rc = $ptoductTypeModel->name;
                    }
                    return $rc;
                }

            ],
            [
                'attribute' => 'parent_id',
                'value' => function ($model) {
                    $rc = NULL;
                    $ptoductTypeModel = $model->getProductType()->one();
                    if ($ptoductTypeModel != NULL) {
                        $rc = $ptoductTypeModel->name_he;
                    }
                    return $rc;
                }

            ],



            [
                'attribute' => 'producer_id',
                'value' => function ($model) {
                    return $model->getProducer()->one()->name;
                }

            ],

            ['class' => 'yii\grid\ActionColumn'],

            ];

    }
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => t('корневой тип')],

        'columns' => $columns,
    ]);?>

