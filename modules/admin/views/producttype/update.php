<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\ProductType */
if("he-IL" ==  Yii::$app->language) {
    $this->title = t('Редактирование типа продукта: ') . ': ' . te($model, 'name')  ;
}
else{
    $this->title = t('Редактирование типа продукта: ') . te($model, 'name');
}
$this->params['breadcrumbs'][] = ['label' => t('Типы продуктов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => te($model, 'name'), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('Внесение изменений');
?>
<div class="product-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $template = '<tr><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';
    if (LocaleEnum::isRTL()) {
        $template = '<tr><td{contentOptions}>{value}</td><th{captionOptions}>{label}</th></tr>';
    }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'template' => $template,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => 'корневой тип'],
        'attributes' => [

            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],
        ],
    ]) ?>
    <div class="product-type-form" <?php if(LocaleEnum::isRTL()){ ?>  style="direction: rtl;" <?php  } ?>>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput() ?>
        <?= $form->field($model, 'name_he')->textInput() ?>
        <?php   echo $form->field($model, 'parent_id') ->dropDownList($model->getProductTypeListMenu(),['prompt'=>t('Корневой тип'), 'encodeSpaces' => true,])?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? t('Создать') : t('Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


</div>
