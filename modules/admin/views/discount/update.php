<?php
use app\models\enums\LocaleEnum;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Discount */

if("he-IL" ==  Yii::$app->language) {
    $this->title = $model->name_he  . t('Внести изменения в акцию: ');
}
else {
    $this->title = t('Внести изменения в акцию: ') . $model->name;
}


$this->params['breadcrumbs'][] = ['label' => t('Акции'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('Внести изменения');
?>
<div class="discount-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
