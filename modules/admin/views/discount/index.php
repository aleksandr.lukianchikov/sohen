<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\enums\Type;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DiscountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//var_dump($actual);die;

$this->title = t('Акции');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $optionsAct = ['id' => 'act'];
    $optionsBtn = ['class' => 'btn btn-primary', 'role' => 'button', 'data-toggle' => 'collapse','aria-expanded'=>'false','aria-controls'=>'collapseFilters'];
    $CreateBtn = ['class' => 'btn btn-success'];
    $ActualBtn = ['class' => 'btn btn-success','id' => 'actual'];//,'onClick'=> "$(this).text('Все акции');"];
    if (LocaleEnum::isRTL()) {
        Html::addCssClass($optionsBtn, 'pull-right');
        Html::addCssClass( $CreateBtn, 'pull-right');
        Html::addCssClass( $ActualBtn, 'pull-right');
    }
    ?>
    <?= Html::a(t('Создать'), ['create'],$CreateBtn) ?>
    <?=Html::a(t('Фильтры'),"#collapseFilters", $optionsBtn);?>
    <? if ($searchModel->actual): ?>
        <?= Html::a(t('Все акции'), ['discount/index'],$ActualBtn) ?>
    <? else: ?>
        <?= Html::a(t('Только актуальные'), ['actuali'],$ActualBtn) ?>
    <? endif; ?>

    <? if($searchModel->isActiveFilter()): ?>
        <?= Html::a(t('Сбросить фильтр'), [\yii\helpers\Url::to("/" .$this->context->action->getUniqueId())], ['class' => 'btn btn-default']); ?>
    <? endif; ?>


    <?php echo $this->render('_search', ['model' => $searchModel]); ?>



<?php if(LocaleEnum::isRTL()){
        //Массив с колонками выносим в переменную для удобства
        $columns = [

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['discount/view', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']), $url);
                    },

                    'update' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['discount/update', 'id' => $model->id]);


                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']), $url);

                    }
                ],

            ],
            'date_end',
            // 'date_start',
            // 'sum',
            // 'user_id',
            [
                'attribute' => 'product_gift_id',
                'value' => function($model){
                    $rc = NULL;
                    $productModel = $model->getProductGift()->one();
                    if($productModel != NULL){
                        $rc = $productModel->name_he;
                    }
                    return $rc;
                    //return $model->getProductConditionName();
                }

            ],
            // 'qnt_product_gift',
            // 'percent',
            'min_qnt_product',
            [
                'attribute' => 'product_id',
                'value' => function($model){
                    $rc = NULL;
                    $productModel = $model->getProduct()->one();
                    if($productModel != NULL){
                        $rc = $productModel->name_he;
                    }
                    return $rc;
                    //return $model->getProductConditionName();
                }

            ],
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    return Type::getClientValue($model->type);
                }
            ],
            'name_he',
            //'id',
            ['class' => 'yii\grid\SerialColumn'],

        ];
    }
    else {
        $columns = [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    return Type::getClientValue($model->type);
                }
            ],
            [
                'attribute' => 'product_id',
                'value' => function($model){
                    $rc = NULL;
                    $productModel = $model->getProduct()->one();
                    if($productModel != NULL){
                        $rc = $productModel->name;
                    }
                    return $rc;
                    //return $model->getProductConditionName();
                }

            ],
            'min_qnt_product',
            // 'percent',
            // 'qnt_product_gift',
            [
                'attribute' => 'product_gift_id',
                'value' => function($model){
                    $rc = NULL;
                    $productModel = $model->getProductGift()->one();
                    if($productModel != NULL){
                        $rc = $productModel->name;
                    }
                    return $rc;
                    //return $model->getProductConditionName();
                }

            ],
            // 'user_id',
            // 'sum',
            // 'date_start',
            'date_end',
            // 'producer_id',
            // 'image_1',


            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['discount/view', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']), $url);
                    },

                    'update' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['discount/update', 'id' => $model->id]);


                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']), $url);

                    }
                ],

            ],


        ];
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => t('не задано')],

        'columns' => $columns,
    ]);?>
</div>
