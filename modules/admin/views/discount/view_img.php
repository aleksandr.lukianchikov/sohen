<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\Type;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\Discount */

$this->title =  te($model, 'name');
$this->params['breadcrumbs'][] = ['label' => t('Акции'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        $classChangeBtn = ['class' => 'btn btn-primary'];
        if (LocaleEnum::isRTL()) {
            Html::addCssClass($classChangeBtn, 'pull-right');
        }
        ?>

        <?= Html::a(t('Изменить'), ['update', 'id' => $model->id], $classChangeBtn) ?>
        <?= Html::a(t('Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

    </p>

    <?php
    $template = '<tr><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';
    if (LocaleEnum::isRTL()) {
        $template = '<tr><td{contentOptions}>{value}</td><th{captionOptions}>{label}</th></tr>';
    }
    ?>

    <? if($model->type != NULL): ?>

        <?= DetailView::widget([
            'model' => $model,
            'template' => $template,
            'attributes' => [
                //  'id',
                'name',
                [
                    'label' => $model->getAttributeLabel('name_he'),
                    'value' => te($model, 'name', 'he-IL'),
                ],
                [
                    'attribute' => 'type',
                    'value' => function ($model) {
                        return Type::getClientValue($model->type);
                    }
                ],
            'date_start',
                'date_end',
                [
                    'attribute' => 'producer_id',
                    'value' => function (\app\models\Discount $model) {
                        return $model->producer->name;
                    }
                ],
                'discription',
                [
                    'label' => $model->getAttributeLabel('discription_he'),
                    'value' => te($model, 'discription', 'he-IL'),
                ],

                [
                    'attribute' => 'image_1',
                    'format' => 'html',
                    'value' => function ($model) {
                        if ($model->image_1) {
                            return Html::img('/uploads/' . $model->image_1, ['width' => '200px']);
                        }
                        return '';
                    }
                ],
            ],
        ]) ?>


    <? endif; ?>
    <? if($model->type == NULL): ?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'image_1',
                    'format' => 'html',
                    'value' => function ($model) {
                        if ($model->image_1) {
                            return Html::img('/uploads/' . $model->image_1, ['width' => '200px']);
                        }
                        return '';
                    }
                ],
            ],
        ]) ?>
    <? endif; ?>
</div>