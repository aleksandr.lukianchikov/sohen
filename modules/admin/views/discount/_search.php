<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\Type;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\DiscountSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?
$containerOptions = ['class' => 'collapse', 'id' => 'collapseFilters'];
if ($model->isActiveFilter()) {
    Html::addCssClass($containerOptions, 'in');
}
?>
<?php
$RtLStyle = ['direction'=> 'rtl'];
if(LocaleEnum::isRTL()){  Html::addCssStyle($containerOptions,$RtLStyle, true) ; }
?>

<?= Html::beginTag('div', $containerOptions) ?>
<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'fieldConfig' => [
        'template'  => "{input}\n{label}\n{hint}\n{error}",],
]); ?>


    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'type')->dropDownList(Type::getClientValues(),['prompt'=>t('Не выбрано')])?>

    <?= $form->field($model, 'product_id') -> dropDownList($model -> getProductList(),['prompt'=>t('Не выбрано')]) ?>

    <?= $form->field($model, 'product_gift_id')-> dropDownList($model -> getProductList(),['prompt'=>t('Не выбрано')]) ?>

    <?= $form->field($model, 'user_id')->dropDownList($model -> getUserList(),['prompt'=>t('Не выбрано')]) ?>

    <?= $form->field($model, 'date_start')->widget(
        \dosamigos\datepicker\DatePicker::className(), [
        // inline too, not bad
        'inline' => true,
        // modify template for custom rendering
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'

        ]
    ]);?>

    <?= $form->field($model, 'date_end')->widget(
        \dosamigos\datepicker\DatePicker::className(), [
        // inline too, not bad
        'inline' => true,
        // modify template for custom rendering
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'

        ]
    ]);?>

    <?php // echo $form->field($model, 'percent') ?>

    <?php // echo $form->field($model, 'qnt_product_gift') ?>




    <?php // echo $form->field($model, 'sum') ?>



    <?php // echo $form->field($model, 'image_1') ?>

    <?php // echo $form->field($model, 'producer_id') ?>

<div class="form-group">
    <?php
    $SearchBtn = ['class' => 'btn btn-primary'];
    $ResetBtn = ['class' => 'btn btn-default'];
    if (LocaleEnum::isRTL()) {
        Html::addCssClass($SearchBtn, 'pull-right');
        Html::addCssClass($ResetBtn, 'pull-right');
    }

    ?>

    <?= Html::submitButton(t('Поиск'),$SearchBtn) ?>
    <?= Html::resetButton(t('Сброс'), $ResetBtn) ?>


</div>

    <?php ActiveForm::end(); ?>

</div>
