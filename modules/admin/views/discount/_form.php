<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\Type;
use dosamigos\fileupload\FileUpload;
use app\models\enums\LocaleEnum;


/* @var $this yii\web\View */
/* @var $model app\models\Discount */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="discount-form"<?php if(LocaleEnum::isRTL()){ ?>  style="direction: rtl;" <?php  } ?> >

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'name_he')->textInput() ?>
    <?= $form->field($model, 'type')->dropDownList(Type::getClientValues(),['prompt'=>t('Выберите нужное')])?>
    <?= $form->field($model, 'product_id')->dropDownList($model -> getProductList(),['prompt'=>t('Выберите нужное')]) ?>
    <?= $form->field($model, 'min_qnt_product')->textInput() ?>
    <?= $form->field($model, 'percent')->textInput() ?>
    <?= $form->field($model, 'qnt_product_gift')->textInput() ?>
    <?= $form->field($model, 'product_gift_id')->dropDownList($model -> getProductList(),['prompt'=>t('Выберите нужное')]) ?>

    <?= $form->field($model, 'user_id')->dropDownList($model -> getUserList(),['prompt'=>t('Выберите нужное')]) ?>
    <?= $form->field($model, 'sum')->textInput() ?>
    <?= $form->field($model, 'discription')->textarea() ?>
    <?= $form->field($model, 'discription_he')->textarea() ?>


    <!-- должен показывать календарь -->
    <?= $form->field($model, 'date_start')->widget(
        \dosamigos\datepicker\DatePicker::className(), [
        // inline too, not bad
        'inline' => true,
        // modify template for custom rendering
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'

        ]
    ]);?>

    <?= $form->field($model, 'date_end')->widget(
        \dosamigos\datepicker\DatePicker::className(), [
        // inline too, not bad
        'inline' => true,
        // modify template for custom rendering
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
    ]);?>

    <?= $form->field($model, 'image_file')->widget(
        '\trntv\filekit\widget\Upload',
        [
            'url' => ['upload-file'],
            'sortable' => true,
            'maxFileSize' => 10 * 1024 * 1024, // 10 MiB
            'maxNumberOfFiles' => 1,
            //'clientOptions' => [ ...other blueimp options... ]
        ]
    ); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('Создать новую акцию') : t('Внести изменения'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
