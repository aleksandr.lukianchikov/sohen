<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Compensation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="compensation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'producer_id')->textInput() ?>

    <?= $form->field($model, 'product_id')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'percentReturn')->textInput() ?>

    <?= $form->field($model, 'sum')->textInput() ?>

    <?= $form->field($model, 'approved_date')->textInput() ?>

    <?= $form->field($model, 'statement_date')->textInput() ?>

    <?= $form->field($model, 'end_date')->textInput() ?>

    <?= $form->field($model, 'is_approved')->dropDownList(\app\models\enums\IsApproved::getClientValues(),['prompt'=>t('Выберите нужное')])?>



    <?= $form->field($model, 'completed')->dropDownList(\app\models\enums\Completed::getClientValues(),['prompt'=>t('Выберите нужное')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('Создать') : t('Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
