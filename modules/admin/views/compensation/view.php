<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\IsApproved;
use app\models\enums\Completed;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\Compensation */

$this->title = t('№ заявки ') . $model->id;
$this->params['breadcrumbs'][] = ['label' => t('Заявки на компенсацию'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compensation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        $classChangeBtn = ['class' => 'btn btn-primary'];
        if (LocaleEnum::isRTL()) {
            Html::addCssClass($classChangeBtn, 'pull-right');
        }
        ?>

        <?= Html::a(t('Внести изменения'), ['update', 'id' => $model->id], $classChangeBtn) ?>

    </p>

    <?php
    $template = '<tr><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';
    if (LocaleEnum::isRTL()) {
        $template = '<tr><td{contentOptions}>{value}</td><th{captionOptions}>{label}</th></tr>';
    }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'template' => $template,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }
            ],
            [
                'attribute' => 'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }
            ],
            [
                'attribute' => 'product_id',
                'value' => function($model){
                    if (LocaleEnum::isRTL()) {
                        return $model->getProduct()->one()->name_he;
                    }
                    else{
                        return $model->getProduct()->one()->name;
                    }
                }
            ],

            'amount',
            'percentReturn',
            'sum',
            'statement_date',
            'end_date',
            [
                'attribute' => 'is_approved',
                'value' => function($model){
                    return IsApproved::getClientValue($model->is_approved);
                }
            ],
            'approved_date',
            [
                'attribute' => 'completed',
                'value' => function($model){
                    return Completed::getClientValue($model->completed);
                }
            ],
        ],
    ]) ?>

</div>
