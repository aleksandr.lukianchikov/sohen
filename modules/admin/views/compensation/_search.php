<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\CompensationSearch */
/* @var $form yii\widgets\ActiveForm */
?>


<?
$containerOptions = ['class' => 'collapse', 'id' => 'collapseFilters'];
if ($model->isActiveFilter()) {
    Html::addCssClass($containerOptions, 'in');
}
?>
<?php
$RtLStyle = ['direction'=> 'rtl'];
if(LocaleEnum::isRTL()){  Html::addCssStyle($containerOptions,$RtLStyle, true) ; }
?>
<?= Html::beginTag('div', $containerOptions) ?>
<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'fieldConfig' => [
        'template'  => "{input}\n{label}\n{hint}\n{error}",],
]); ?>
    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id')->dropDownList($model->getUserList(),['prompt'=>t('Не выбрано')])  ?>

    <?php // echo $form->field($model, 'producer_id') ?>

    <?= $form->field($model, 'product_id')->dropDownList($model->getProductList(),['prompt'=>t('Не выбрано')]) ?>




    <?= $form->field($model, 'is_approved')->dropDownList(\app\models\enums\IsApproved::getClientValues(),['prompt'=>t('Не выбрано')]) ?>

    <?= $form->field($model, 'completed')->dropDownList(\app\models\enums\Completed::getClientValues(),['prompt'=>t('Не выбрано')]) ?>

    <?php // echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'percentReturn') ?>

    <?php // echo $form->field($model, 'sum') ?>

    <?=  $form->field($model, 'statement_date') ?>

    <?php // echo  $form->field($model, 'end_date') ?>



    <?php // echo $form->field($model, 'approved_date') ?>

<div class="form-group">
    <?php
    $SearchBtn = ['class' => 'btn btn-primary'];
    $ResetBtn = ['class' => 'btn btn-default'];
    if (LocaleEnum::isRTL()) {
        Html::addCssClass($SearchBtn, 'pull-right');
        Html::addCssClass($ResetBtn, 'pull-right');
    }

    ?>

    <?= Html::submitButton(t('Поиск'),$SearchBtn) ?>
    <?= Html::resetButton(t('Сброс'), $ResetBtn) ?>


</div>

    <?php ActiveForm::end(); ?>

</div>
