<?php
use app\models\enums\LocaleEnum;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Compensation */
if("he-IL" ==  Yii::$app->language) {
    $this->title = $model->id . t('Редактировать заявки на компенсацию №: ') ;
}
else{
    $this->title = t('Редактировать заявки на компенсацию №: ') . $model->id;
}
$this->params['breadcrumbs'][] = ['label' => t('Заявки на компенсацию'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('Редактировать');
?>
<div class="compensation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $template = '<tr><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';
    if (LocaleEnum::isRTL()) {
        $template = '<tr><td{contentOptions}>{value}</td><th{captionOptions}>{label}</th></tr>';
    }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'template' => $template,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }
            ],
            [
                'attribute' => 'product_id',
                'value' => function($model){
                    if (LocaleEnum::isRTL()) {
                        return $model->getProduct()->one()->name_he;
                    }
                    else{
                        return $model->getProduct()->one()->name;
                    }
                }
            ],


            [
                'attribute' => 'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }
            ],
            'amount',
            'end_date',
            'statement_date',

        ],
    ]) ?>
<div class="compensation-form"<?php if(LocaleEnum::isRTL()){ ?>  style="direction: rtl;" <?php  } ?> >
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'percentReturn')->textInput() ?>
    <?= $form->field($model, 'sum')->textInput() ?>
    <?= $form->field($model, 'is_approved')->dropDownList(\app\models\enums\IsApproved::getClientValues(),['prompt'=>t('Выберите нужное')])   ?>
    <?php // echo  $form->field($model, 'approved_date')->textInput() ?>
    <?= $form->field($model, 'completed')->dropDownList(\app\models\enums\Completed::getClientValues(),['prompt'=>t('Выберите нужное')])  ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('Создать') : t('Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
