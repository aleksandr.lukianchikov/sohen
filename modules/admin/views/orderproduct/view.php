<?php

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var \app\models\OrderProduct[] $modelOrderProducts*/

//$this->title = $model->id;

$this->params['breadcrumbs'][] = $this->title;
$srt_ord_num = t('Заказ №');
$str_from = t('от');
$text = t(' Фирма-производитель');
$text_2 = t('числа. ');
$str_cansel = t('ОТМЕНИТЬ');
$str_pay = t('ОПЛАТИТЬ');
$str_repeat = t('СОЗДАТЬ ТАКОЙ ЖЕ ЗАКАЗ В НОВОЙ КОРЗИНЕ');
$str_shek = t('шек.');
?>
<div class="order-product-view">

    <h1><?= Html::encode($this->title) ?></h1>



    <?php if("he-IL" !=  Yii::$app->language) { ?>

        <P>Заказ № <?=$model->id; ?> от <?=$model->order_date ?> числа. Фирма-производитель "<?=$model->producer->name;?>"</P>
        <?php  $form = ActiveForm::begin(); $total = 0; ?>

        <? foreach ($modelOrderProducts as $i => $orderProduct): ?>
            <div id="cart-<?= $orderProduct->id ?>" class="row cart-item-row">

                <div class="col-sm-4">
                    <h4  class="product-name"><strong>
                            <?= te($orderProduct->product,'name') ?></strong></h4><h4></h4>
                </div>
                <div class="col-sm-4">
                    <h6 class="barcode-name"><strong> <?= $orderProduct->product->internal_code ?></strong></h6>
                </div>
                <div class="col-sm-4">
                    <div class="col-sm-4 text-right">
                        <h6><strong><?= $orderProduct->product->price ?> <span class="text-muted">x</span></strong></h6>
                    </div>
                    <div class="col-sm-4 text-right">
                        <h6><strong><?= $orderProduct->amount ?> <span class="text-muted"></span></strong></h6>
                    </div>
                    <div class="col-sm-4 text-right">
                        <?php  $s = $orderProduct->amount * $orderProduct->product->price ?>
                        <h6><strong><?php $total = $total + $s;?><?=$s?> <span class="text-muted"></span></strong></h6>
                    </div>
                    <div class="col-sm-2">

                    </div>
                </div>
            </div>
        <? endforeach; ?>
        <div class="row">
            <div class="text-center">
                <div class="col-xs-9">

                </div>
                <div class="col-xs-3">

                </div>
            </div>
        </div>
        <?php ActiveForm::end(); } else { ?>
        <P style="text-align: right"> <span><?='   ' .    $text_2 . $model->order_date . '. ' ;?> </span> <span><?=  $srt_ord_num .  $model->id  ?>
            </span><span> <?='. ' .  $model->producer->name  ;?></span><span><?='   '. $text . ' '; ?></span></P>
        <?php   $form = ActiveForm::begin(); $total = 0; ?>
        <? foreach ($modelOrderProducts as $i => $orderProduct): ?>
            <div id="cart-<?= $orderProduct->id ?>" class="row cart-item-row">
                <div class="col-xs-4">
                    <div class="col-xs-4 text-right">
                        <?php  $s = $orderProduct->amount * $orderProduct->product->price ?>
                        <h6><strong><?php $total = $total + $s;?><?=$s?> <span class="text-muted"></span></strong></h6>
                    </div>


                    <div class="col-xs-4 text-right">
                        <h6><strong><?= $orderProduct->amount ?> <span class="text-muted"></span></strong></h6>
                    </div>

                    <div class="col-xs-4 text-right">
                        <h6><strong><span class="text-muted">x </span><?= $orderProduct->product->price ?></strong></h6>
                    </div>

                    <div class="col-xs-2">

                    </div>
                </div>
                <div class="col-xs-4">
                    <h6 class="barcode-name"style="text-align: right" ><strong> <?= $orderProduct->product->barcode ?></strong></h6>
                </div>
                <div class="col-xs-4"style="text-align: right">
                    <h4 class="product-name"><strong>
                            <?= te($orderProduct->product,'name') ?></strong></h4><h4></h4>
                </div>


            </div>
        <? endforeach; ?>
        <div class="row">
            <div class="text-center">
                <div class="col-xs-9">

                </div>
                <div class="col-xs-3">

                </div>
            </div>
        </div>
        <?php ActiveForm::end(); }?>
</div>

<div class="row text-center">
    <div class="col-xs-12">
        <h4 class="text-right"><?=t('итого до акций: ')?><strong><span class="amount-total"><?php if($totalProducts == 0){$totalProducts = $model->sum;};?><?=$totalProducts?><?=t( 'шек.')?></span></strong></h4>
    </div>
</div>

<div class="row">
    <?php $totalPlusDiscount = $total; ?>
    <?php if("he-IL" !=  Yii::$app->language) { ?>
        <? foreach ($modelOrderDiscounts as $i => $orderDiscount): ?>
            <div >
                <div class="col-xs-2"  id="discount"><?= t('АКЦИЯ')?></div>
                <div class="col-xs-6" id="discount-name"><?=te($orderDiscount->discount,'name')?></div>
                <div class="col-xs-1" id="discount-percent"><?=$orderDiscount->discount->percent.'%'?></div>
                <div class="col-xs-2" id="discount-sum">-<?=$orderDiscount->sum ?> <?= $str_shek ?><?php $totalPlusDiscount = $totalPlusDiscount - $orderDiscount->sum ?></div>

            </div>
        <? endforeach; ?>
    <?php } else {?>
        <? foreach ($modelOrderDiscounts as $i => $orderDiscount): ?>
            <div >
                <div class="col-xs-2" id="discount-sum" style="text-align: right">-<?=$orderDiscount->sum ?> <?= $str_shek ?><?php $totalPlusDiscount = $totalPlusDiscount - $orderDiscount->sum ?></div>
                <div class="col-xs-1" id="discount-percent" style="text-align: right"><?=$orderDiscount->discount->percent.'%'?></div>
                <div class="col-xs-6" id="discount-name" style="text-align: right" ><?=te($orderDiscount->discount,'name')?></div>
                <div class="col-xs-2"  id="discount" style="text-align: right"><?= t('АКЦИЯ')?></div>

            </div>
        <? endforeach; ?>
    <?php } ?>


    <div class="col-xs-12">
        <h4 class="text-right"><?=t('итого: ')?><strong><span class="amount-total"><?=$model->sum;?><?=t(' шек.')?></span></strong></h4>
    </div>
</div>
<? if($model->orderstatus == 'created'): ?>
    <div class = "row" id ="orderproduct-buttons">
        <div class =" col-md-5">

        </div>

        <div class ="col-md-3">
            <?= Html::a('<span ></span>' .  $str_cansel, ['orderproduct/delete', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
        </div>

        <div class =" col-md-1">

        </div>

        <div class ="col-md-3">
            <?= Html::a('<span ></span>' .  $str_pay , ['orderproduct/payform','id' => $model->id, 'sum' => $model->sum], ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>

<? endif; ?>
<? if($model->orderstatus != 'created'): ?>
    <div class ="col-md-6">
        <?= Html::a('<span ></span>' .  $str_repeat, ['/cart/repeat', 'id' => $model->id], ['class' => 'btn btn-primary btn-block']) ?>
    </div>
<? endif; ?>
