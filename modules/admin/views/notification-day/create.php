<?php

use yii\helpers\Html;
use app\models\enums\DAY;


/* @var $this yii\web\View */
/* @var $model app\models\NotificationDay */

$this->title = t('Создать покупателю дни получения напоминаний');
$this->params['breadcrumbs'][] = ['label' => t('Дни'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="day-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
