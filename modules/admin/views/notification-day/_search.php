<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\Day;
use app\models\enums\City;
use app\models\enums\LocaleEnum;


/* @var $this yii\web\View */
/* @var $model app\models\NotificationDaySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?
    $containerOptions = ['class' => 'collapse', 'id' => 'collapseFilters'];
    if ($model->isActiveFilter()) {
        Html::addCssClass($containerOptions, 'in');
    }
?>
<?php
$RtLStyle = ['direction'=> 'rtl'];
if(LocaleEnum::isRTL()){  Html::addCssStyle($containerOptions,$RtLStyle, true) ; }
?>
<?= Html::beginTag('div', $containerOptions) ?>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'fieldConfig' => [
            'template'  => "{input}\n{label}\n{hint}\n{error}",],
    ]); ?>

    <?= $form->field($model, 'user_id')->dropDownList($model->getUserList(),['prompt'=>t('Выберите нужное')]) ?>
    <?= $form->field($model, 'city')->dropDownList(City::getClientValues(),['prompt'=>t('Выберите нужное')]) ?>

    <?= $form->field($model, 'reminder_day')->dropDownList(Day::getClientValues(),['prompt'=>t('Выберите нужное')])  ?>

    <?= $form->field($model, 'order_day')->dropDownList(Day::getClientValues(),['prompt'=>t('Выберите нужное')])  ?>



    <div class="form-group">
        <?= Html::submitButton(t('Поиск'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(t('Сброс'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
