<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\enums\Day;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NotificationDaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('Дни получения напоминаний');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="day-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    $optionsBtn = ['class' => 'btn btn-primary', 'role' => 'button', 'data-toggle' => 'collapse','aria-expanded'=>'false','aria-controls'=>'collapseFilters'];
    $CreateBtn = ['class' => 'btn btn-success'];
    if (LocaleEnum::isRTL()) {
        Html::addCssClass($optionsBtn, 'pull-right');
        Html::addCssClass( $CreateBtn, 'pull-right');
    }
    ?>
    <?= Html::a(t('Создать'), ['create'],$CreateBtn) ?>
    <?=Html::a(t('Фильтры'),"#collapseFilters", $optionsBtn);?>

    <? if($searchModel->isActiveFilter()): ?>
        <?= Html::a(t('Сбросить фильтр'), [\yii\helpers\Url::to("/" .$this->context->action->getUniqueId())], ['class' => 'btn btn-default']); ?>
    <? endif; ?>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

     <?php if("he-IL" ==  Yii::$app->language) {
        $columns = [

            ['class' => 'yii\grid\ActionColumn'],
            [
                'attribute' => 'order_day',
                'value' => function ($model) {
                    return Day::getClientValue($model->order_day);
                }
            ],
            [
                'attribute' => 'reminder_day',
                'value' => function ($model) {
                    return Day::getClientValue($model->reminder_day);
                }
            ],
            // 'producer_id',
            [
                'attribute'=>'city',
                'value' => function($model){
                    //return $model->getUser()->one()->city;
                    return $model->getCity();
                }
            ],
            [
                'attribute'=>'user_id',
                'value' => function($model){
                    return $model->user->shop_name;
                }

            ],
            //'id'
            ['class' => 'yii\grid\SerialColumn'],
                ];
    }
    else{
        $columns = [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute'=>'user_id',
                'value' => function($model){
                    return $model->user->shop_name;
                }

            ],

            [
                'attribute'=>'city',
                'value' => function($model){
                    //return $model->getUser()->one()->city;
                    return $model->getCity();
                }
            ],

            // 'producer_id',
            [
                'attribute' => 'reminder_day',
                'value' => function ($model) {
                    return Day::getClientValue($model->reminder_day);
                }
            ],
            [
                'attribute' => 'order_day',
                'value' => function ($model) {
                    return Day::getClientValue($model->order_day);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ];
    }
?>

     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => t('не задано')],

        'columns' => $columns,
    ]);?>
</div>
