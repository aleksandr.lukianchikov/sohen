<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\Day;
use app\models\enums\City;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\NotificationDay */

$this->title =  $model->getProducer()->one()->name;
$this->params['breadcrumbs'][] = ['label' => t('Дни'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="day-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        $classChangeBtn = ['class' => 'btn btn-primary'];
        if (LocaleEnum::isRTL()) {
            Html::addCssClass($classChangeBtn, 'pull-right');
        }
        ?>
        <?= Html::a(t('Внести изменения'), ['update', 'id' => $model->id], $classChangeBtn) ?>
        <?= Html::a(t('Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
    $template = '<tr><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';
    if (LocaleEnum::isRTL()) {
        $template = '<tr><td{contentOptions}>{value}</td><th{captionOptions}>{label}</th></tr>';
    }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'template' => $template,
        'attributes' => [
           // 'id',
            [
                'attribute'=>'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }

            ],

            [
                'attribute'=>'city',
                'value' => function($model){
                    //return $model->getUser()->one()->city;
                    return $model->getCity();
                }
           ],


           // 'producer_id',
            [
                'attribute' => 'reminder_day',
                'value' => function ($model) {
                    return Day::getClientValue($model->reminder_day);
                }
            ],
            [
                'attribute' => 'order_day',
                'value' => function ($model) {
                    return Day::getClientValue($model->order_day);
                }
            ],
        ],
    ]) ?>

</div>
