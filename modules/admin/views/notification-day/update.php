<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\NotificationDay */
if(LocaleEnum::isRTL()){
    $this->title = '"' . $model->getUser()->one()->shop_name . '     '  . '"' . t('Внесение изменений в дни получения напоминаний для юзера: ') ;
}
else{
    $this->title = t('Внесение изменений в дни получения напоминаний для юзера: ') . $model->getUser()->one()->shop_name;
}
$this->params['breadcrumbs'][] = ['label' => t('Дни получения напоминаний'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('Изменить');
?>
<div class="day-update">

    <h1><?= Html::encode($this->title) ?></h1>





    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
