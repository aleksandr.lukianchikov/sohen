<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\Day;
use app\models\ArrayHelper;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\NotificationDay */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="day-form" <?php if(LocaleEnum::isRTL()){ ?>  style="direction: rtl;" <?php  } ?> >

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->dropDownList($model->getUserList(),['prompt'=>t('Выберите нужное')]) ?>

    <?php // echo $form->field($model, 'producer_id')->textInput() ?>

    <?= $form->field($model, 'reminder_day')->dropDownList(Day::getClientValues(),['prompt'=>t('Выберите нужное')])  ?>

    <?= $form->field($model, 'order_day')->dropDownList(Day::getClientValues(),['prompt'=>t('Выберите нужное')])   ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('Создать') : t('Внести изменения'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
