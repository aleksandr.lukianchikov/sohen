<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\IsApproved;
use app\models\enums\Completed;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\ProductReturn */
if (LocaleEnum::isRTL()){
    $this->title = $model->id . t('Заявка на возврат № ');
}
else {
    $this->title = t('Заявка на возврат № ') . $model->id;
}
$this->params['breadcrumbs'][] = ['label' => t('Заявки на возврат'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-return-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
            $classChangeBtn = ['class' => 'btn btn-primary'];
             if (LocaleEnum::isRTL()) {
                Html::addCssClass($classChangeBtn, 'pull-right');
        }
        ?>

        <?= Html::a(t('Изменить'), ['update', 'id' => $model->id], $classChangeBtn) ?>

    </p>

    <?php
    $template = '<tr><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';
    if (LocaleEnum::isRTL()) {
        $template = '<tr><td{contentOptions}>{value}</td><th{captionOptions}>{label}</th></tr>';
    }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'template' => $template,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }
            ],
            [
                'attribute' => 'product_id',
                'value' => function($model){
                if (LocaleEnum::isRTL()) {
                    return $model->getProduct()->one()->name_he;
                    }
                 else{
                     return $model->getProduct()->one()->name;
                    }
                }
            ],

            [
                'attribute' => 'reason_id',
                'value' => function($model){
                if (LocaleEnum::isRTL()) {
                    return $model->getReason()->one()->name_he;
                    }
                else{
                    return $model->getReason()->one()->name;
                     }
                }

            ],
            [
                'attribute' => 'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }
            ],
            'number',
            'statement_date',
            [
                'attribute' => 'is_approved',
                'value' => function($model){
                    return IsApproved::getClientValue($model->is_approved);
                }
            ],
            [
                'attribute' => 'image_1',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_1) {
                        return Html::img('/uploads/' . $model->image_1, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'image_2',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_2) {
                        return Html::img('/uploads/' . $model->image_2, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'image_3',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_3) {
                        return Html::img('/uploads/' . $model->image_3, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            'approved_date',
            [
                'attribute' => 'completed',
                'value' => function($model){
                    return IsApproved::getClientValue($model->completed);
                }
            ],


        ],
    ]) ?>

</div>
