<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\fileupload\FileUpload;

/* @var $this yii\web\View */
/* @var $model app\models\ProductReturn */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-return-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'producer_id')->dropDownList(\yii\helpers\ArrayHelper::map(app\models\Producer::find()->all()
        ,'id','name'),['prompt'=>t('Выберите нужное')]) ?>

    <?php if("he-IL" ==  Yii::$app->language) { ?>
    <?= $form->field($model, 'product_id')->dropDownList(\yii\helpers\ArrayHelper::map(app\models\Product::find()->all()
        ,'id','name_he'),['prompt'=>t('Выберите нужное')]) ?>
    <?php } else {?>
     <?= $form->field($model, 'product_id')->dropDownList(\yii\helpers\ArrayHelper::map(app\models\Product::find()->all()
    ,'id','name'),['prompt'=>t('Выберите нужное')]) ?>
    <?php } ?>

    <?= $form->field($model, 'number')->textInput() ?>
    <?= $form->field($model, 'reason_id')->dropDownList($model->getReasonList(),['prompt'=>t('Выберите нужное')])  ?>
    <?= $form->field($model, 'image_file')->widget(
        '\trntv\filekit\widget\Upload',
        [
            'url' => ['upload-file'],
            'sortable' => true,
            'maxFileSize' => 10 * 1024 * 1024, // 10 MiB
            'maxNumberOfFiles' => 3,
            //'clientOptions' => [ ...other blueimp options... ]
        ]
    ); ?>
    <?= $form->field($model, 'statement_date')->textInput() ?>
    <?= $form->field($model, 'is_approved')->dropDownList(\app\models\enums\IsApproved::getClientValues(),['prompt'=>t('Выберите нужное')])   ?>
    <?= $form->field($model, 'approved_date')->textInput() ?>
    <?= $form->field($model, 'completed')->dropDownList(\app\models\enums\Completed::getClientValues(),['prompt'=>t('Выберите нужное')])  ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('Создать') : t('Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
