<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\ProductReturnSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?
$containerOptions = ['class' => 'collapse', 'id' => 'collapseFilters' ];
if ($model->isActiveFilter()) {
    Html::addCssClass($containerOptions, 'in');
}
?>
<?php
$RtLStyle = ['direction'=> 'rtl'];
if(LocaleEnum::isRTL()){  Html::addCssStyle($containerOptions,$RtLStyle, true) ; }
?>
<?= Html::beginTag('div', $containerOptions) ?>
<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'fieldConfig' => [
        'template'  => "{input}\n{label}\n{hint}\n{error}",],
]); ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo  $form->field($model, 'user_id') ?>

<?php  if (LocaleEnum::isRTL()) { ?>
    <?= $form->field($model, 'product_id')->dropDownList(\yii\helpers\ArrayHelper::map(app\models\Product::find()->all()
        ,'id','name_he'),['prompt'=>t('Выберите нужное')]) ?>
<?php } else {?>
    <?= $form->field($model, 'product_id')->dropDownList(\yii\helpers\ArrayHelper::map(app\models\Product::find()->all()
        ,'id','name'),['prompt'=>t('Выберите нужное')]) ?>
<?php } ?>

    <?= $form->field($model, 'reason_id')->dropDownList($model->getReasonList(),['prompt'=>t('Не выбрано')]) ?>

    <?= $form->field($model, 'is_approved')->dropDownList(\app\models\enums\IsApproved::getClientValues()
        ,['prompt'=>t('Не выбрано')]) ?>

    <?= $form->field($model, 'completed')->dropDownList(\app\models\enums\Completed::getClientValues(),['prompt'=>t('Не выбрано')]) ?>

    <?= $form->field($model, 'statement_date') ?>

    <?= $form->field($model, 'user_id')->dropDownList($model->getUserList(),['prompt'=>t('Не выбрано')]) ?>

    <?php //echo $form->field($model, 'image_1') ?>

    <?php // echo $form->field($model, 'producer_id') ?>

    <?php // echo $form->field($model, 'number') ?>

    <?php // echo $form->field($model, 'image_2') ?>

    <?php // echo $form->field($model, 'image_3') ?>

<div class="form-group">
    <?php
    $SearchBtn = ['class' => 'btn btn-primary'];
    $ResetBtn = ['class' => 'btn btn-default'];
    if (LocaleEnum::isRTL()) {
        Html::addCssClass($SearchBtn, 'pull-right');
        Html::addCssClass($ResetBtn, 'pull-right');
    }

    ?>

    <?= Html::submitButton(t('Поиск'),$SearchBtn) ?>
    <?= Html::resetButton(t('Сброс'), $ResetBtn) ?>


</div>

    <?php ActiveForm::end(); ?>

</div>
