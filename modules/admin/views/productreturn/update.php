<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use app\models\enums\LocaleEnum;
/* @var $this yii\web\View */
/* @var $model app\models\ProductReturn */
if (LocaleEnum::isRTL()){
    $this->title = $model->id . t('Редактирование заявки на возврат №') ;
}
else{
    $this->title = t('Редактирование заявки на возврат №') . $model->id;
}
$this->params['breadcrumbs'][] = ['label' => t('Заявки на возврат'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('Редактирование');
?>
<div class="product-return-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    $template = '<tr><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';
    if (LocaleEnum::isRTL()) {
        $template = '<tr><td{contentOptions}>{value}</td><th{captionOptions}>{label}</th></tr>';
    }

            $classChangeBtn = ['class' => 'btn btn-primary'];
             if (LocaleEnum::isRTL()) {
                 Html::addCssClass($classChangeBtn, 'pull-right');
             }
        ?>

    ?>

    <?= DetailView::widget([
        'model' => $model,
        'template' => $template,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }
            ],
            [
                'attribute' => 'product_id',
                'value' => function($model){
                    if (LocaleEnum::isRTL()) {
                        return $model->getProduct()->one()->name_he;
                    }
                    else{
                        return $model->getProduct()->one()->name;
                    }
                }
            ],

            [
                'attribute' => 'reason_id',
                'value' => function($model){
                    if (LocaleEnum::isRTL()) {
                        return $model->getReason()->one()->name_he;
                    }
                    else{
                        return $model->getReason()->one()->name;
                    }
                }

            ],
            [
                'attribute' => 'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }
            ],
            'number',
            'statement_date',

        ],
    ]) ?>
    <div class="product-return-form"  <?php if(LocaleEnum::isRTL()){ ?>  style="direction: rtl;" <?php  } ?>  >
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'is_approved')->dropDownList(\app\models\enums\IsApproved::getClientValues(),['prompt'=>t('Выберите нужное')])   ?>
    <?php // echo $form->field($model, 'approved_date')->textInput() ?>
    <?= $form->field($model, 'completed')->dropDownList(\app\models\enums\Completed::getClientValues(),['prompt'=> t('Выберите нужное')])  ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('Создать') : t('Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : $classChangeBtn]) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>

</div>
