<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductReturn */

$this->title = 'Создать заявку на возврат';
$this->params['breadcrumbs'][] = ['label' => 'Product Returns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-return-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
