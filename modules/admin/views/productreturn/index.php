<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\enums\IsApproved;
use app\models\enums\Completed;
use app\models\enums\LocaleEnum;
use Yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductReturnSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('Заявки на возврат продукта');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="product-return-index">
    <h1><?= Html::encode($this->title) ?></h1>




    <?php
    $optionsBtn = ['class' => 'btn btn-primary', 'role' => 'button', 'data-toggle' => 'collapse','aria-expanded'=>'false','aria-controls'=>'collapseFilters'];
        if (LocaleEnum::isRTL()) {
            Html::addCssClass($optionsBtn, 'pull-right');
        }
    ?>
    <?=Html::a(t('Фильтры'),"#collapseFilters", $optionsBtn);
    ?>


    <? if($searchModel->isActiveFilter()): ?>
        <?= Html::a(t('Сбросить фильтр'), [\yii\helpers\Url::to("/" .$this->context->action->getUniqueId())], ['class' => 'btn btn-default']); ?>
    <? endif; ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]);?>

    <?php if (LocaleEnum::isRTL()){
        //Массив с колонками выносим в переменную для удобства


            $columns = [
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            $url = \yii\helpers\Url::to(['productreturn/view', 'id' => $model->id]);
                            return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']), $url);
                        },

                        'update' => function ($url, $model, $key) {
                            $url = \yii\helpers\Url::to(['productreturn/update', 'id' => $model->id]);
                            return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']), $url);
                        },

                    ],

                ],
                [
                    'attribute' => 'completed',
                    'value' => function($model){
                        return Completed::getClientValue($model->completed);
                    }
                ],
                'approved_date',
                [
                    'attribute' => 'is_approved',
                    'value' => function($model){
                        return IsApproved::getClientValue($model->is_approved);
                    }
                ],
                'statement_date',
                'number',
                  [
                     'attribute'=>'reason_id',
                      'value' => function($model){
                           return $model->getReason()->one()->name_he;
                       }
                   ],

               // 'reason_id',
                [
                    'attribute'=>'product_id',
                    'value' => function($model){
                        return $model->getProduct()->one()->name_he;
                    }

                ],

                [
                    'attribute'=>'user_id',
                    'value' => function($model){
                        return $model->getUser()->one()->shop_name;
                    }

                ],
                ['class' => 'yii\grid\SerialColumn'],

            ];


    }else{
        $columns = [
        ['class' => 'yii\grid\SerialColumn'],


            [
                'attribute'=>'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }

            ],
            [
                'attribute'=>'product_id',
                'value' => function($model){
                    return $model->getProduct()->one()->name;
                }

            ],
           // 'reason_id',
            [
               'attribute'=>'reason_id',
                'value' => function($model){
                   return $model->getReason()->one()->name;
                }
            ],

            'number',
            'statement_date',
            [
                'attribute' => 'is_approved',
                'value' => function($model){
                    return IsApproved::getClientValue($model->is_approved);
                }
            ],



            //'image_1',
            // 'producer_id',
            // 'number',
            // 'image_2',
            // 'image_3',
            'approved_date',
            [
                'attribute' => 'completed',
                'value' => function($model){
                    return Completed::getClientValue($model->completed);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['productreturn/view', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']), $url);
                    },

                    'update' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['productreturn/update', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']), $url);
                    },

                ],

            ],

          ];


    }
?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => t('не задано')],

        'columns' => $columns,
    ]);?>
</div>
