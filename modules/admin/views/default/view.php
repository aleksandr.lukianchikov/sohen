<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container">
    <div class="jumbotron text-center">
        <?php if("he-IL" !=  Yii::$app->language){?>
            <h2 id="cabinet-admin"><?php echo t( 'Кабинет администратора фирмы')?> "<?=$producerModel->name ?>"</h2>
        <?php }?>
        <?php if("he-IL" ==  Yii::$app->language){?>
            <h2 id="cabinet-admin">"<?=$producerModel->name ?>"<?php echo t( 'Кабинет администратора фирмы')?> </h2>
        <?php }?>
    </div>
    <h2 id="admin-choose-action"><?php echo t( 'Выберите действие')?></h2>
    <ul class="list-group">

        <li class="list-group-item list-group-item-warning"><a href="index.php?r=admin/order/index"><?php echo t( 'Мои заказы')?></a></li>
        <li class="list-group-item list-group-item-success"> <a href="index.php?r=admin/product/index" ><?php echo t( 'Работа с продуктами')?></a></li>
        <li class="list-group-item list-group-item-info"><a href="index.php?r=admin/discount/index" ><?php echo t( 'Работа с акциями')?></a></li>
        <li class="list-group-item list-group-item-danger"> <a href="index.php?r=admin/discount/image" ><?php echo t( 'Добавление в слайдер просто картинок с текстом')?></a></li>
        <li class="list-group-item list-group-item-warning"> <a href="index.php?r=admin/producttype/index" > <?php echo t( 'Работа с типом продукта')?></a></li>
        <li class="list-group-item list-group-item-danger"><a href="index.php?r=admin/notification-day/index" ><?php echo t( 'Покупатели и дни недели получения напоминаний')?></a></li>
        <li class="list-group-item list-group-item-warning "><a href="index.php?r=admin/user/current" > <?php echo t( 'Мои клиенты')?></a></li>
        <li class="list-group-item list-group-item-success"><a href="index.php?r=admin/notification-text/index" ><?php echo t( 'Работа с текстами сообщений')?></a></li>
        <li class="list-group-item list-group-item-info"> <a href="index.php?r=admin/productreturn/index"><?php echo t( 'Работа с заявками на возврат продукта')?></a></li>
        <li class="list-group-item list-group-item-danger"> <a href="index.php?r=admin/compensation/index" >
              <?php echo t( 'Работа с заявками на компенсацию за непроданный товар с истекающим сроком хранения')?></a></li>
                
        <li class="list-group-item list-group-item-warning"> <a href="index.php?r=admin/user/index"><?php echo t( 'Неутвержденные покупатели')?></a></li>
        <li class="list-group-item list-group-item-success"><a href="index.php?r=admin/user/viewme"><?php echo t( 'Регистрационные данные')?></a></li>
    </ul>

</div>