<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\OrderStatus;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
if (LocaleEnum::isRTL()) {
    $this->title =  $model->id .' ' .  t('№ заказа ');
}
else{
    $this->title = t('№ заказа ') . $model->id;
}
$this->params['breadcrumbs'][] = ['label' => t('Заказ'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $template = '<tr><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';
    if (LocaleEnum::isRTL()) {
        $template = '<tr><td{contentOptions}>{value}</td><th{captionOptions}>{label}</th></tr>';
    }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'template' => $template,
        'attributes' => [
            'id',
            'order_date',
            [
                'attribute' => 'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }
            ],

            [
                'attribute' => 'orderstatus',
                'value' => function ($model) {
                    return OrderStatus::getClientValue($model->orderstatus);
                }
            ],

            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],
            'sum',
        ],
    ]) ?>

</div>
