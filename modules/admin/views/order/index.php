<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\enums\OrderStatus;
use app\models\enums\LocaleEnum;
/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('Все заказы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1 <?php if(LocaleEnum::isRTL()) {?>style ="text-align:right";<?php }?>  ><?= Html::encode($this->title) ?></h1>


    <?php
    $optionsBtn = ['class' => 'btn btn-primary', 'role' => 'button', 'data-toggle' => 'collapse','aria-expanded'=>'false','aria-controls'=>'collapseFilters'];
    if (LocaleEnum::isRTL()) {
        Html::addCssClass($optionsBtn, 'pull-right');
    }
    ?>
    <?=Html::a(t('Фильтры'),"#collapseFilters", $optionsBtn);
    ?>

    <? if($searchModel->isActiveFilter()): ?>
        <?= Html::a(t('Сбросить фильтр'), [\yii\helpers\Url::to("/" .$this->context->action->getUniqueId())], ['class' => 'btn btn-default']); ?>
    <? endif; ?>

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?php if(LocaleEnum::isRTL()) {
        //Массив с колонками выносим в переменную для удобства
        $columns = [


            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {print} {update}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['order/view', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']), $url);
                    },

                    'update' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['order/update', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']), $url);
                    },

                    'print' => function ($url, $model, $key) {
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-print']), $url);
                    },
                ],

            ],
            'sum',

            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],

            [
                'attribute' => 'orderstatus',
                'value' => function ($model) {
                    return OrderStatus::getClientValue($model->orderstatus);
                }
            ],

            [
                'attribute'=>'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }

            ],

            'order_date',

            'id',

            ['class' => 'yii\grid\SerialColumn'],

        ];
    }
    else{
        $columns = [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'order_date',

            [
                'attribute'=>'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }

            ],

            [
                'attribute' => 'orderstatus',
                'value' => function ($model) {
                    return \app\models\enums\OrderStatus::getClientValue($model->orderstatus);
                }
            ],

            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],
            'sum',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {print} {update}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['orderproduct/view', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']), $url);
                    },

                    'update' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['order/update', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']), $url);
                    },

                    'print' => function ($url, $model, $key) {
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-print']), $url);
                    },
                ],

            ],


        ];

    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => t('не задано')],

        'columns' => $columns,
    ]);?>
</div>
