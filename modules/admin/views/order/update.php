<?php
use app\models\enums\LocaleEnum;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\OrderProduct */
if(LocaleEnum::isRTL()) {
    $this->title = $model->id . t('Изменения заказа: ') ;
}
else{
    $this->title = t('Изменения заказа: ') . $model->id;
}
$this->params['breadcrumbs'][] = ['label' => t('Заказ'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('Внесение изменений');
?>

<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_date',
            [
                'attribute' => 'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }
            ],


            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],
            'sum',
        ],
    ]) ?>

</div>





<div class="order-product-update" <?php if(LocaleEnum::isRTL()){ ?>  style="direction: rtl;" <?php  } ?> >

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'orderstatus')->dropDownList(\app\models\enums\OrderStatus::getClientValues(), ['prompt'=>t('Выберете нужное')]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('Создать') : t('Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>