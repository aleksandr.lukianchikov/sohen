<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\NotificationText */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="text-form" <?php if(LocaleEnum::isRTL()){ ?>  style="direction: rtl;" <?php  } ?>  >

    <?php $form = ActiveForm::begin(); ?>

    <?php // echo $form->field($model, 'producer_id')->textInput() ?>

    <?= $form->field($model, 'text_1')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'text_1_he')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'text_2')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'text_2_he')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('Создать') : t('Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
