<?php

use yii\helpers\Html;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\NotificationText */
if (LocaleEnum::isRTL()) {
    $this->title = $model->getProducer()->one()->name . ' ' . t('Изменить текст: ');
}
else{
    $this->title = t('Изменить текст: ') . ' ' . $model->getProducer()->one()->name;
}
$this->params['breadcrumbs'][] = ['label' => t('Tексты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('Изменить');
?>
<div class="text-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
