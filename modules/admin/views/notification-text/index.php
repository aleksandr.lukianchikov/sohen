<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\enums\LocaleEnum;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NotificationTextSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('Все тексты');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    $CreateBtn = ['class' => 'btn btn-success'];
    if (LocaleEnum::isRTL()) {
        Html::addCssClass( $CreateBtn, 'pull-right');
    }
    ?>
    <?= Html::a(t('Создать'), ['create'],$CreateBtn) ?>



    <?php  if (LocaleEnum::isRTL()) {
        $columns = [
            ['class' => 'yii\grid\ActionColumn'],
            'text_2_he:ntext',
            'text_1_he:ntext',
            ['class' => 'yii\grid\SerialColumn'],
        ];
    }
    else{
        $columns = [
            ['class' => 'yii\grid\SerialColumn'],

            //  'id',
            //'producer_id',
            'text_1:ntext',
            'text_2:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ];
    }

    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => t('не задано')],

        'columns' => $columns,
    ]);?>
</div>