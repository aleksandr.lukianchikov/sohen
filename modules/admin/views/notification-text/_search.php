<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\NotificationTextSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="text-search" <?php if(LocaleEnum::isRTL()){ ?>  style="direction: rtl;" <?php  } ?> >

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'producer_id') ?>

    <?= $form->field($model, 'text_1') ?>

    <?= $form->field($model, 'text_2') ?>

    <div class="form-group">
        <?php
        $SearchBtn = ['class' => 'btn btn-primary'];
        $ResetBtn = ['class' => 'btn btn-default'];
        if (LocaleEnum::isRTL()) {
            Html::addCssClass($SearchBtn, 'pull-right');
            Html::addCssClass($ResetBtn, 'pull-right');
        }

        ?>

        <?= Html::submitButton(t('Поиск'),$SearchBtn) ?>
        <?= Html::resetButton(t('Сброс'), $ResetBtn) ?>


    </div>
