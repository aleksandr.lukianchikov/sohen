<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\LocaleEnum;
/* @var $this yii\web\View */
/* @var $model app\models\NotificationText */

$this->title =  $model->getProducer()->one()->name;

$this->params['breadcrumbs'][] = ['label' => t('Тексты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-view">

    <h1><?='Тексты сообщений компании' . ' ' .  Html::encode($this->title) ?></h1>

    <p>
        <?php
        $classChangeBtn = ['class' => 'btn btn-primary'];
        if (LocaleEnum::isRTL()) {
            Html::addCssClass($classChangeBtn, 'pull-right');
        }
        ?>
        <?= Html::a(t('Внести изменения'), ['update', 'id' => $model->id], $classChangeBtn) ?>
        <?= Html::a(t('Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
    $template = '<tr><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';
    if (LocaleEnum::isRTL()) {
        $template = '<tr><td{contentOptions}>{value}</td><th{captionOptions}>{label}</th></tr>';
    }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'template' => $template,
        'attributes' => [
          //  'id',
            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],
            'text_1:ntext',

            'text_2:ntext',

            [
               'label' => $model->getAttributeLabel('text_1_he'),
                'value' => te($model, 'text_1', \app\models\enums\Language::Hebrew),
            ],
            [
                'label' => $model->getAttributeLabel('text_2_he'),
                'value' => te($model, 'text_2', \app\models\enums\Language::Hebrew),
            ],
        ],
    ]) ?>

</div>
