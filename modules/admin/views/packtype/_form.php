<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\LocaleEnum;

/* @var $this yii\web\View */
/* @var $model app\models\PackType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pack-type-form" <?php if(LocaleEnum::isRTL()){ ?>  style="direction: rtl;" <?php  } ?> >

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_he')->textInput() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
