<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PackType */

$this->title = 'Create Pack Type';
$this->params['breadcrumbs'][] = ['label' => 'Pack Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pack-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
