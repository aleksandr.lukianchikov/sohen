<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PackType */

$this->title = 'Update Pack Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Pack Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pack-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
