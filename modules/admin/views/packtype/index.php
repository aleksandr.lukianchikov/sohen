<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PackTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pack Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pack-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pack Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'name',
            'name_he',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
