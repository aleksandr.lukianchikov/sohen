<?php
namespace app\modules\admin\components;

use app\components\Controller;

class AdminController extends Controller {
    public $layout = 'main';
}