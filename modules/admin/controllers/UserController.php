<?php

namespace app\modules\admin\controllers;
use app\modules\admin\components\AdminController;
use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\PasswordsForm;
use app\models\RegisterForm;
use app\models\ResetForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionRegister()
    {
        $modelRegisterForm = new RegisterForm();
        if ($modelRegisterForm->load(Yii::$app->request->post()) && $modelRegisterForm->validate()) {
            $userModel = new User();
            $userModel->setAttributes($modelRegisterForm->getAttributes());
            $userModel->setPass($userModel->password);
            $userModel->delivery_address = $modelRegisterForm->street . ' ' . $modelRegisterForm->number_house;
            $userModel->access_rights = json_encode(['producer_admin']);
            $userModel->status = 'active';
            if($userModel->save()) {
                return $this->redirect(['site/login']);
            }
        }
        return $this->render('register', [
            'modelRegisterForm' => $modelRegisterForm,
        ]);
    }




    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $flag = 0;
        $searchModel = new UserSearch();
        $searchModel->access_rights = '["customer"]';
        $searchModel->status = 'moderate';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'flag' => $flag,
        ]);
    }
    public function actionCurrent(){
        $flag = 1;
        $searchModel = new UserSearch();
        $searchModel->access_rights = '["customer"]';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'flag' => $flag,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionViewme()
    {
        $id = Yii::$app->user->identity->producer_id;
        return $this->render('viewme', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    public function actionUpdateme()
    {
        $modelRegisterForm = new RegisterForm();
        $model = $this->findModel(Yii::$app->user->identity->id);
        $modelRegisterForm->shop_name = $model->shop_name;
        $modelRegisterForm->email = $model->email;
        //       $modelRegisterForm->password = '123456';
        //       $modelRegisterForm->password_confirm = '123456';
        $modelRegisterForm->tel = $model->tel;
        $modelRegisterForm->producer_id = $model->producer_id;
        $modelRegisterForm->city = "admin";
        $modelRegisterForm->street = "admin";
        $modelRegisterForm->number_house = "admin";
        $modelRegisterForm->language = $model->language;

        if ($modelRegisterForm->load(Yii::$app->request->post()) && $modelRegisterForm->validate()) {
           // if($modelRegisterForm->validate())
            //var_dump($modelRegisterForm->getAttributes()); die;
            $model->setAttributes($modelRegisterForm->getAttributes());
            $model->setPass($model->password);
            if ($model->save()) {
                return $this->redirect(['viewme', 'id' => $model->id]);
            }
        }
        else {
            return $this->render('updateme', [
                'modelRegisterForm' => $modelRegisterForm, 'model' => $model
            ]);
        }
    }



    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionSetPassword($token){
        // SQL injection
        //var_dump("SELECT * FROM user WHERE token = '$token'"); die;
        // x2' OR '1
        // SELECT * FROM user WHERE token = 'x2' OR '1'
        // $result = Yii::$app->db->createCommand("SELECT * FROM user WHERE token = :token")->bindParam(':token', $token)->query()->read();

        $model = User::findOne(['token' => $token]);
        if (!$model) {
            throw new HttpException(403);
        }
        $modelPasswordsForm = new PasswordsForm();
        if ($modelPasswordsForm->load(Yii::$app->request->post()) && $modelPasswordsForm->validate()){
            $model->password = Yii::$app->security->generatePasswordHash($modelPasswordsForm->password);
            if ($model->save(false)) {
                $model->token = '';
                $model->save(false);
            }
            return $this->redirect(['site/login']);
        }

        return $this->render('passwords', [
            'modelPasswordsForm' => $modelPasswordsForm,
        ]);
    }
}
