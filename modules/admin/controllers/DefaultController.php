<?php
/**
 * Created by PhpStorm.
 * User: Bramnik
 * Date: 9/6/2017
 * Time: 11:02 AM
 */

namespace app\modules\admin\controllers;
use app\modules\admin\components\AdminController;
use Yii;
use app\models\Producer;

class DefaultController extends AdminController
{
    public function actionView()
    {
        $producerModel = Producer::findOne(Yii::$app->user->identity->producer_id);
        return $this->render('view',['producerModel' => $producerModel]);

    }

}