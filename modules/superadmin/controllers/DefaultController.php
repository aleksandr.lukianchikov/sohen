<?php
/**
 * Created by PhpStorm.
 * User: Bramnik
 * Date: 10/3/2017
 * Time: 1:29 PM
 */

namespace app\modules\superadmin\controllers;
use app\modules\superadmin\components\SuperadminController;

class DefaultController extends SuperadminController{
    public function actionView()
    {
        return $this->render('view');
    }

}