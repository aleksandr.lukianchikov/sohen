<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StorageConditions */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Все условия хранения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="storage-conditions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
         //   'id',
            'name',
            [
                'label' => $model->getAttributeLabel('name_he'),
                'value' => te($model, 'name', 'he-IL'),
            ],
        ],
    ]) ?>

</div>
