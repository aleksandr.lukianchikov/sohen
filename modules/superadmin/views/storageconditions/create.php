<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StorageConditions */

$this->title = 'Create Storage Conditions';
$this->params['breadcrumbs'][] = ['label' => 'Storage Conditions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="storage-conditions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
