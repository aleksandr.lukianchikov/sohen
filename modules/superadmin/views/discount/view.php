<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\Type;

/* @var $this yii\web\View */
/* @var $model app\models\Discount */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Акции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <? if($model->type != NULL): ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          //  'id',
            'name',
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    return Type::getClientValue($model->type);
                }
            ],

            [
                'attribute' => 'product_id',
                'value' => function($model){

                    return $model->getProductConditionName();
                }

            ],
            'min_qnt_product',
            'percent',
            'qnt_product_gift',
            [
                'attribute' => 'product_gift_id',
                'value' => function($model){

                    return $model->getProductName();
                }

            ],
            'user_id',
            'sum',
            'date_start',
            'date_end',
            [
                'attribute' => 'producer_id',
                'value' => function (\app\models\Discount $model) {
                    return $model->producer->name;
                }
            ],
            'discription',
            [
                'attribute' => 'image_1',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_1) {
                        return Html::img('/uploads/' . $model->image_1, ['width' => '200px']);
                    }
                    return '';
                }
            ],
        ],
    ]) ?>


<? endif; ?>
<? if($model->type == NULL): ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'image_1',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_1) {
                        return Html::img('/uploads/' . $model->image_1, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            ],
    ]) ?>
<? endif; ?>
</div>