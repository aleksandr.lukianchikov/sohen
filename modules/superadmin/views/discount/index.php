<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\enums\Type;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DiscountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Акции';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
        Фильтры
    </a>
    <? if($searchModel->isActiveFilter()): ?>
        <?= Html::a('Сбросить фильтр', [\yii\helpers\Url::to("/" .$this->context->action->getUniqueId())], ['class' => 'btn btn-default']); ?>
    <? endif; ?>
    <?= Html::a('Только актуальные', ['create'], ['class' => 'btn btn-success']) ?>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           'id',
            'name',
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    return Type::getClientValue($model->type);
                }
            ],
            [
                'attribute' => 'product_id',
                'value' => function($model){
                    $rc = NULL;
                    $productModel = $model->getProduct()->one();
                    if($productModel != NULL){
                        $rc = $productModel->name;
                    }
                    return $rc;
                    //return $model->getProductConditionName();
                }

            ],
            'min_qnt_product',
            // 'percent',
            // 'qnt_product_gift',
            [
                'attribute' => 'product_gift_id',
                'value' => function($model){
                    $rc = NULL;
                    $productModel = $model->getProductGift()->one();
                    if($productModel != NULL){
                        $rc = $productModel->name;
                    }
                    return $rc;
                    //return $model->getProductConditionName();
                }

            ],
            // 'user_id',
            // 'sum',
            // 'date_start',
             'date_end',
            // 'producer_id',
            // 'image_1',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
