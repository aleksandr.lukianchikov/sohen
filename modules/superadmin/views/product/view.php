<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\Kosher;
use app\models\enums\Available;
use app\models\enums\StatusNew;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Внести изменения', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            'name',
            'barcode',
            'discription',
            'composition',
            'price',
            'pack_gram',
            'calories',
            'shelf_life',

            [
                'attribute' => 'storage_conditions_id',
                'value' => function($model){
                    return $model->getStorageConditions()->one()->name;
                }
            ],

            [
                'attribute' => 'pack_type_id',
                'value' => function($model){
                    return $model->getPackType()->one()->name;
                }
            ],

            [
            'attribute' => 'product_type_id',
               'value' => function($model){
               return $model->getProductType()->one()->name;
               }
            ],

            [
                'attribute' => 'kosher',
                'value' => function ($model) {
                    return Kosher::getClientValue($model->kosher);
                }
            ],
            [
                'attribute' => 'available',
                'value' => function ($model) {
                    return Available::getClientValue($model->available);
                }
            ],

            [
                'attribute' => 'status_new',
                'value' => function ($model) {
                    return StatusNew::getClientValue($model->status_new);
                }
            ],
            [
                'attribute' => 'producer_id',
                'value' => function (\app\models\Product $model) {
                    return $model->producer->name;
                }
            ],
            [
                'attribute' => 'image_1',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_1) {
                        return Html::img('/uploads/' . $model->image_1, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'image_2',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_2) {
                        return Html::img('/uploads/' . $model->image_2, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'image_3',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_3) {
                        return Html::img('/uploads/' . $model->image_3, ['width' => '200px']);
                    }
                    return '';
                }
            ],

        ],
    ]) ?>

</div>
