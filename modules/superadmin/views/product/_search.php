<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\Kosher;
use app\models\enums\Available;
use app\models\enums\StatusNew;

/* @var $this yii\web\View */
/* @var $model app\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?
$containerOptions = ['class' => 'collapse', 'id' => 'collapseFilters'];
if ($model->isActiveFilter()) {
    Html::addCssClass($containerOptions, 'in');
}
?>
<?= Html::beginTag('div', $containerOptions) ?>
<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>

    <?= $form->field($model, 'kosher')->dropDownList(Kosher::getClientValues(),['prompt' => 'Не выбрано...']) ?>
    <?= $form->field($model, 'status_new')->dropDownList(StatusNew::getClientValues(), ['prompt' => 'Не выбрано...']) ?>
    <?= $form->field($model, 'available')->dropDownList(Available::getClientValues(), ['prompt' => 'Не выбрано...']) ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'product_type_id') ->dropDownList($model->getProductTypeList(),['prompt'=>'Выберите нужное', 'encodeSpaces' => true,])?>
    <?= $form->field($model, 'price') ?>
    <?= $form->field($model, 'barcode') ?>

    <?php //echo $form->field($model, 'composition') ?>

    <?php // echo $form->field($model, 'pack_gram') ?>

    <?php // echo $form->field($model, 'calories') ?>

    <?php // echo $form->field($model, 'storage_conditions_id') ?>

    <?php // echo $form->field($model, 'pack_type_id') ?>

    <?php // echo $form->field($model, 'producer_id') ?>

    <?php // echo $form->field($model, 'image_1') ?>

    <?php // echo $form->field($model, 'image_2') ?>

    <?php // echo $form->field($model, 'image_3') ?>

    <?php // echo $form->field($model, 'shelf_life') ?>

    <?php // echo $form->field($model, 'product_type_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сброс', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
