<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\ProductType;

$producerId = Yii::$app->user->identity->producer_id;
$filterProductType = Html::activeDropDownList(
    $searchModel,
    'product_type_id',
    ArrayHelper::map(ProductType::find()->where('producer_id = :p_id', [':p_id' => $producerId])->asArray()->all(), 'id', 'name'),
    ['class'=>'form-control','prompt' => 'Выберите...']
);

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
        Фильтры
    </a>

    <? if($searchModel->isActiveFilter()): ?>
        <?= Html::a('Сбросить фильтр', [\yii\helpers\Url::to("/" .$this->context->action->getUniqueId())], ['class' => 'btn btn-default']); ?>
    <? endif; ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'name',
            'barcode',
           // 'discription',
           // 'composition',
            [
                'attribute' => 'price',
                'format' => 'raw',
                'value' => function ($model){
                    return '<b>' . Html::encode($model->price) . '</b>';
                },
            ],
            // 'pack_gram',
            // 'calories',
            // 'storage_conditions_id',
            // 'pack_type_id',
            // 'kosher',
            // 'available',
            // 'status_new',

            /*[
                'attribute' => 'producer_id',
                'filter' => false,
            ],*/

            // 'image_1',
            // 'image_2',
            // 'image_3',
            // 'shelf_life',

            [
                'attribute' => 'product_type_id',
                'filter' => $filterProductType,
                'value' => function($model){
                    return $model->getProductType()->one()->name;
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
