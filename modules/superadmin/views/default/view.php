<?php

use yii\helpers\Html;


/* @var $this yii\web\View */

/* @var $form yii\widgets\ActiveForm */
?>
<div class="container">
    <div class="jumbotron text-center">
        <h2 id="cabinet-admin">Кабинет Супер-админа фирмы </h2>

    </div>
    <h2 id="admin-choose-action">Выберите действие</h2>
    <ul class="list-group">

        <li class="list-group-item list-group-item-danger"> <a href="index.php?r=admin/productreturn/index">Неутвержденные покупатели</a></li>
        <li class="list-group-item list-group-item-warning"><a href="index.php?r=admin/default/view">Кабинет админа veles</a></li>
        <li class="list-group-item list-group-item-success"> <a href="index.php?r=superadmin/discount/index" >Неутвержденные акции</a></li>
        <li class="list-group-item list-group-item-info"><a href="index.php?r=superadmin/product/index" > Неутвержденные продукты</a></li>
        <li class="list-group-item list-group-item-danger"> <a href="index.php?r=superadmin/discount/image" >Неутвержденные картинки для слайдера</a></li>
        <li class="list-group-item list-group-item-warning"> <a href="index.php?r=superadmin/reason/index" >Работа с причинами возврата</a></li>
        <li class="list-group-item list-group-item-success"><a href="index.php?r=superadmin/packtype/index" >Работа с типами упаковки</a></li>
        <li class="list-group-item list-group-item-info "><a href="index.php?r=superadmin/storageconditions/index" >Работа с условиями хранения</a></li>
        <li class="list-group-item list-group-item-danger"> <a href="index.php?r=superadmin/user/index">Все покупатели</a></li>
        <li class="list-group-item list-group-item-warning"> <a href="index.php?r=superadmin/producer/index">Все производители</a></li>

    </ul>

</div>
