<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Producer */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Все производители', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'label' => $model->getAttributeLabel('name_he'),
                'value' => te($model, 'name', 'he-IL'),
            ],
            'tel',
            'address',
            [
                'label' => $model->getAttributeLabel('address_he'),
                'value' => te($model, 'address', 'he-IL'),
            ],
            'site',
            'account',
            'email:email',
            'slogan',
            [
                'label' => $model->getAttributeLabel('slogan_he'),
                'value' => te($model, 'slogan', 'he-IL'),
            ],
            [
                'attribute' => 'compensation',
                'value' => function ($model) {
                    return \app\models\enums\Compensation::getClientValue($model->compensation);
                }
            ],
            [
                'attribute' => 'image_1',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_1) {
                        return Html::img('/uploads/' . $model->image_1, ['width' => '200px']);
                    }
                    return '';
                }
            ],
        ],
    ]) ?>

</div>
