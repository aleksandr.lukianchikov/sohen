<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProducerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фирмы-производители';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::a('Создать нового производителя', ['create'], ['class' => 'btn btn-success']) ?>
    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
        Фильтры
    </a>

    <? if($searchModel->isActiveFilter()): ?>
        <?= Html::a('Сбросить фильтр', [\yii\helpers\Url::to("/" .$this->context->action->getUniqueId())], ['class' => 'btn btn-default']); ?>
    <? endif; ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'tel',
            'address',
            'site',
            // 'account',
            // 'email:email',
            // 'slogan',
            // 'image_1',
            // 'compensation',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
