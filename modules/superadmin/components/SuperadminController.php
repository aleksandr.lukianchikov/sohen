<?php
/**
 * Created by PhpStorm.
 * User: Bramnik
 * Date: 10/3/2017
 * Time: 1:30 PM
 */

namespace app\modules\superadmin\components;
use app\components\Controller;

class SuperadminController extends Controller
{
    public $layout = 'main';
}