<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\IsApproved;
use app\models\enums\Completed;

/* @var $this yii\web\View */
/* @var $model app\models\Compensation */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => t('Все мои заявки на компенсации'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compensation-view">





    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute'=>'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }

            ],
            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],
            [
                'attribute'=>'product_id',
                'value' => function($model){
                    return $model->getProduct()->one()->name_he;
                }

            ],
            'amount',
            'percentReturn',
            'sum',
            'statement_date',
            'end_date',
            [
                'attribute' => 'is_approved',
                'value' => function($model){
                    return IsApproved::getClientValue($model->is_approved);
                }
            ],
            'approved_date',
            [
                'attribute' => 'completed',
                'value' => function($model){
                    return Completed::getClientValue($model->completed);
                }
            ],
        ],
    ]) ?>

</div>
