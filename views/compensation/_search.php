<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CompensationSearch */
/* @var $form yii\widgets\ActiveForm */
?>


<?
$containerOptions = ['class' => 'collapse', 'id' => 'collapseFilters'];
if ($model->isActiveFilter()) {
    Html::addCssClass($containerOptions, 'in');
}
?>
<?= Html::beginTag('div', $containerOptions) ?>
<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>

    <?= $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'producer_id')->dropDownList($model->getProducerList(),['prompt'=>'Не выбрано']) ?>

    <?php // echo $form->field($model, 'product_id') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'percentReturn') ?>

    <?php // echo $form->field($model, 'sum') ?>

    <?= $form->field($model, 'is_approved')->dropDownList(\app\models\enums\IsApproved::getClientValues(),['prompt'=>'Не выбрано']) ?>

    <?php // echo $form->field($model, 'approved_date') ?>

    <?=  $form->field($model, 'statement_date') ?>

    <?= $form->field($model, 'completed')->dropDownList(\app\models\enums\Completed::getClientValues(),['prompt'=>'Не выбрано']) ?>

    <div class="form-group">
        <?= Html::submitButton(t('Поиск'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(t('Сброс'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
