<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Compensation */
/* @var $form yii\widgets\ActiveForm */
?>



    <?= DetailView::widget([
        'model' => $model,

        'attributes' => [
            [
                'attribute' => 'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }
            ],
            [
                'attribute' => 'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }
            ],
            'statement_date',
        ],
    ]) ?>
<div class="compensation-form">
    <?php $form = ActiveForm::begin(); ?>

    <?php // echo $form->field($model, 'user_id')->textInput() ?>

    <?php // $form->field($model, 'producer_id')->textInput(array('readonly'=>true)) ?>

    <?= $form->field($model, 'product_id')->dropDownList($model->getProductList(),['prompt'=>t('Выберите продукт')])?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <!-- должен показывать календарь -->
    <?= $form->field($model, 'end_date')->widget(
        \dosamigos\datepicker\DatePicker::className(), [
        // inline too, not bad
        'inline' => true,
        // modify template for custom rendering
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'

        ]
    ]);?>


    <?php // echo $form->field($model, 'percentReturn')->textInput() ?>

    <?php // echo $form->field($model, 'sum')->textInput() ?>

    <?php // echo  $form->field($model, 'is_approved')->textInput() ?>

    <?php // echo  $form->field($model, 'approved_date')->textInput() ?>

    <?php  // echo  $form->field($model, 'completed')->textInput() ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('Создать') : t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
