<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Compensation */

$this->title = t('Редактировать: ') . $model->id;
$this->params['breadcrumbs'][] = ['label' => t('Компенсации'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('Изменить');
?>
<div class="compensation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
