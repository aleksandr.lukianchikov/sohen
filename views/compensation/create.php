<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Compensation */

$this->title = t('Создать заявку на компенсацию за непроданный товар с истекающим сроком хранения');
$this->params['breadcrumbs'][] = ['label' => t('Все мои заявки на компенсацию'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compensation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
