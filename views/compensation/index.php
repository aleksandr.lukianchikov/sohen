<?php

use app\models\enums\LocaleEnum;
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\enums\IsApproved;
use app\models\enums\Completed;
use yii\bootstrap\Modal;
use yii\bootstrap\Nav;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CompensationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('Все мои заявки на компенсацию');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compensation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>

    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseFilters" aria-expanded="false"
       aria-controls="collapseFilters" <?php if(LocaleEnum::isHebrew()){?>style ="text-align:right"; <?php }?>  >
        <?php echo t('Фильтры')?>
    </a>
    <? if($searchModel->isActiveFilter()): ?>
        <?= Html::a(t('Сбросить фильтр'), [\yii\helpers\Url::to($this->context->action->getUniqueId())], ['class' => 'btn btn-default']); ?>
    <? endif; ?>

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?php if("he-IL" ==  Yii::$app->language) {
        //Массив с колонками выносим в переменную для удобства
        $columns = [
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['productreturn/view', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']), $url);
                    },

                ],

            ],

            [
                'attribute' => 'completed',
                'value' => function($model){
                    return Completed::getClientValue($model->completed);
                }
            ],

            'approved_date',
            //  'completed',
            //'approved_date',
            //'is_approved',
            //'sum',
            //'percentReturn',
            [
                'attribute' => 'is_approved',
                'value' => function($model){
                    return IsApproved::getClientValue($model->is_approved);
                }
            ],
            'end_date',
            'statement_date',
            'amount',
            [
                'attribute'=>'product_id',
                'value' => function($model){
                    return $model->getProduct()->one()->name_he;
                }

            ],
            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],
            [
                'attribute'=>'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }

            ],
            'id',
            ['class' => 'yii\grid\SerialColumn'],

        ];
    }
    else {
        $columns = [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }

            ],
            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],
            [
                'attribute'=>'product_id',
                'value' => function($model){
                    return $model->getProduct()->one()->name;
                }

            ],
            'amount',
            'statement_date',
            'end_date',
            [
                'attribute' => 'is_approved',
                'value' => function($model){
                    return IsApproved::getClientValue($model->is_approved);
                }
            ],
            // 'percentReturn',
            // 'sum',
            // 'is_approved',
            // 'approved_date',
            // 'completed',
            'approved_date',
            [
                'attribute' => 'completed',
                'value' => function($model){
                    return Completed::getClientValue($model->completed);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['productreturn/view', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']), $url);
                    },

                ],

            ],

        ];
    }

    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => t('не задано')],

        'columns' => $columns,
    ]);?>
</div>
<?php


$this->registerJs("
    $(function(){
    
            $('#create_compen').on('click', function(e){
            var url = $(this).data('url');
            $('#check_producer_comp').modal('show').data('url', url);
            e.preventDefault();
        });
        
        $('.add-producer').on('click', function(e){
          
           
        });
    
    });
");


Modal::begin([
    'id' => 'check_producer_comp',
    'header' => t('Выберите производителя'),
    'size' => Modal::SIZE_SMALL,

]);

echo Nav::widget(['items' => $menuItemsComp]);


Modal::end();

?>
