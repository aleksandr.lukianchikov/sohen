<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\IsApproved;
use app\models\enums\Completed;

/* @var $this yii\web\View */
/* @var $model app\models\ProductReturn */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => t('Возвраты продуктов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-return-view">

    <?php if("he-IL" !=  Yii::$app->language) {  ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'id',
            [
                'attribute' => 'user_id',
               //'format' => 'raw',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;

                }
            ],
            [
                'attribute' => 'product_id',
                'value' => function($model){
                    return $model->getProduct()->one()->name;
                }
            ],
           // 'reason_id',
            [
                'attribute' => 'reason_id',
                'value' => function($model){
                    return $model->getReason()->one()->name;
                }
            ],
            [
                'attribute' => 'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }
            ],
            'number',
            'statement_date',
            [
                'attribute' => 'is_approved',
                'value' => function($model){
                    return IsApproved::getClientValue($model->is_approved);
                }
            ],

            [
                'attribute' => 'image_1',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_1) {
                        return Html::img('/uploads/' . $model->image_1, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'image_2',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_2) {
                        return Html::img('/uploads/' . $model->image_2, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'image_3',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_3) {
                        return Html::img('/uploads/' . $model->image_3, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            'approved_date',
            [
                'attribute' => 'completed',
                'value' => function($model){
                    return Completed::getClientValue($model->completed);
                }
            ],
        ],
    ]) ?>

    <?php }


    else{ ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'id',
            [
                'attribute' => 'user_id',
                //'format' => 'raw',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;

                }
            ],
            [
                'attribute' => 'product_id',
                'value' => function($model){
                    return $model->getProduct()->one()->name_he;
                }
            ],
            //'reason_id',
             [
                 'attribute' => 'reason_id',
                 'value' => function($model){
                    return $model->getReason()->one()->name_he;
                 }
               ],
            [
                'attribute' => 'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }
            ],
            'number',
            'statement_date',
            [
                'attribute' => 'is_approved',
                'value' => function($model){
                    return IsApproved::getClientValue($model->is_approved);
                }
            ],

            [
                'attribute' => 'image_1',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_1) {
                        return Html::img('/uploads/' . $model->image_1, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'image_2',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_2) {
                        return Html::img('/uploads/' . $model->image_2, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'image_3',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->image_3) {
                        return Html::img('/uploads/' . $model->image_3, ['width' => '200px']);
                    }
                    return '';
                }
            ],
            'approved_date',
            [
                'attribute' => 'completed',
                'value' => function($model){
                    return Completed::getClientValue($model->completed);
                }
            ],
        ],
    ]) ?>

<?php    } ?>

</div>
