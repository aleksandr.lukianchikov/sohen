<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductReturnSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<?
$containerOptions = ['class' => 'collapse', 'id' => 'collapseFilters'];
if ($model->isActiveFilter()) {
    Html::addCssClass($containerOptions, 'in');
}
?>
<?= Html::beginTag('div', $containerOptions) ?>
<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo  $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'product_id') ?>

    <?= $form->field($model, 'reason_id')->dropDownList($model->getReasonList(),['prompt'=>t('Не выбрано')]) ?>

    <?= $form->field($model, 'producer_id')->dropDownList($model->getProducerList(),['prompt'=>t('Не выбрано')]) ?>

    <?=$form->field($model, 'statement_date') ?>

    <?= $form->field($model, 'is_approved')->dropDownList(\app\models\enums\IsApproved::getClientValues(),['prompt'=>t('Не выбрано')]) ?>

    <?= $form->field($model, 'completed')->dropDownList(\app\models\enums\Completed::getClientValues(),['prompt'=>t('Не выбрано')]) ?>

    <?php //echo $form->field($model, 'image_1') ?>

    <?php // echo $form->field($model, 'producer_id') ?>

    <?php // echo $form->field($model, 'number') ?>

    <?php // echo $form->field($model, 'image_2') ?>

    <?php // echo $form->field($model, 'image_3') ?>

    <div class="form-group">
        <?= Html::submitButton(t('Поиск'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(t('Сброс'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
