<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductReturn */

$this->title = t('Создать заявку на возврат продукта');
$this->params['breadcrumbs'][] = ['label' => t('Возвраты продуктов'), 'url' => ['index']];

?>
<div class="product-return-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
