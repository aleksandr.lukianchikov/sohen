<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductReturn */

$this->title = t('Редактировать: ') . $model->id;
$this->params['breadcrumbs'][] = ['label' => t('Возврат продукта'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('Редактировать');
?>
<div class="product-return-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
