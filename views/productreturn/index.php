<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\enums\IsApproved;
use app\models\enums\Completed;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductReturnSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('Возвраты продуктов');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-return-index">


    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseFilters" aria-expanded="false"
       aria-controls="collapseFilters" <?php if("he-IL" ==  Yii::$app->language){?>style ="text-align:right"; <?php }?>  >
        <?php echo t('Фильтры')?>
    </a>

    <? if($searchModel->isActiveFilter()): ?>
        <?= Html::a(t('Сбросить фильтр'), [\yii\helpers\Url::to($this->context->action->getUniqueId())], ['class' => 'btn btn-default']); ?>
    <? endif; ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if("he-IL" ==  Yii::$app->language) {
        //Массив с колонками выносим в переменную для удобства
        $columns = [
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['productreturn/view', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']), $url);
                    },

                ],

            ],
            [
                'attribute' => 'completed',
                'value' => function($model){
                    return Completed::getClientValue($model->completed);
                }
            ],
            'approved_date',
            [
                'attribute' => 'is_approved',
                'value' => function($model){
                    return IsApproved::getClientValue($model->is_approved);
                }
            ],
            'statement_date',
            'number',
             [
                 'attribute' => 'reason_id',
                  'value' => function($model){
                     return $model->getReason()->one()->name_he;
                  }
               ],
           // 'reason_id',
            [
                'attribute'=>'product_id',
                'value' => function($model){
                    return $model->getProduct()->one()->name_he;
                }

            ],
            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],
            ['class' => 'yii\grid\SerialColumn'],

        ];
    }
    else{
        $columns = [
            ['class' => 'yii\grid\SerialColumn'],


            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],
            [
                'attribute'=>'product_id',
                'value' => function($model){
                    return $model->getProduct()->one()->name;
                }

            ],
           // 'reason_id',
             [
                 'attribute' => 'reason_id',
                  'value' => function($model){
                     return $model->getReason()->one()->name;
                  }
               ],
            'number',
            'statement_date',
            [
                'attribute' => 'is_approved',
                'value' => function($model){
                    return IsApproved::getClientValue($model->is_approved);
                }
            ],
            // 'user_id',
            // 'image_1',
            // 'image_2',
            // 'image_3',
            'approved_date',
            [
                'attribute' => 'completed',
                'value' => function($model){
                    return Completed::getClientValue($model->completed);
                }
            ],



            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['productreturn/view', 'id' => $model->id]);
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']), $url);
                    },

                ],

            ],

        ];
    }
?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => t('не задано')],

        'columns' => $columns,
    ]);?>
</div>
