<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\fileupload\FileUpload;

/* @var $this yii\web\View */
/* @var $model app\models\ProductReturn */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-return-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'producer_id')->dropDownList(\yii\helpers\ArrayHelper::map(app\models\Producer::find()->all()
        ,'id','name'),['prompt'=>t('Выберите нужное')]) ?>

    <?= $form->field($model, 'product_id')->dropDownList($model->getProductList(),['prompt'=>t('Выберите продукт')])?>
    <?= $form->field($model, 'number')->textInput() ?>
    <?= $form->field($model, 'reason_id')->dropDownList($model->getReasonList(),['prompt'=>t('Выберите нужное')])  ?>

    <?= $form->field($model, 'image_file')->widget(
        '\trntv\filekit\widget\Upload',
        [
            'url' => ['upload-file'],
            'sortable' => true,
            'maxFileSize' => 10 * 1024 * 1024, // 10 MiB
            'maxNumberOfFiles' => 3,
            //'clientOptions' => [ ...other blueimp options... ]
        ]
    ); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('Создать') : t('Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
