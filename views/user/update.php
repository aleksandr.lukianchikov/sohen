<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RegisterForm */

$this->title = t('Внести изменения: ') . $model->shop_name;
//$this->params['breadcrumbs'][] = ['label' => 'User', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->shop_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('Изменить');
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('register', [
        'model' => $model,
        'modelRegisterForm' => $modelRegisterForm
    ]) ?>

</div>