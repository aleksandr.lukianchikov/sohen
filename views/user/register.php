<?php
use app\models\enums\City;
use app\models\enums\Language;
use app\models\enums\LocaleEnum;
?>
<div <?php if(LocaleEnum::isRTL()){ ?>  style="direction: rtl;" <?php  } ?> >
<?php
 $form = \yii\bootstrap\ActiveForm::begin([
    'id' => 'login-form',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?= $form->field($modelRegisterForm, 'shop_name')->textInput() ?>
<?= $form->field($modelRegisterForm, 'email')->textInput() ?>
<?= $form->field($modelRegisterForm, 'password')->passwordInput() ?>
<?= $form->field($modelRegisterForm, 'password_confirm')->passwordInput() ?>
<?= $form->field($modelRegisterForm, 'tel')->textInput() ?>
<?= $form->field($modelRegisterForm, 'city')->dropDownList(City::getClientValues(),['prompt'=>t('Выберите нужное')])  ?>
<?= $form->field($modelRegisterForm, 'street')->textInput() ?>
<?= $form->field($modelRegisterForm, 'number_house')->textInput() ?>
<?= $form->field($modelRegisterForm, 'language')->dropDownList(Language::getClientValues(),['prompt'=>t('Выберите нужное')])  ?>



    <div class="form-group">
    <div class="col-lg-offset-1 col-lg-11">
        <button type="submit" class="btn btn-primary" ><?= !isset($model) ? t('Зарегистрироваться') : t('Сохранить') ?></button>
    </div>
</div>

<?php \yii\bootstrap\ActiveForm::end(); ?>
</div>

