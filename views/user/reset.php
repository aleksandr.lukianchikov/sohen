<? foreach (Yii::$app->session->getAllFlashes() as $key => $message): ?>
    <? \yii\bootstrap\Alert::begin([
            'options' => [
                'class' => 'alert-' . $key,
            ],
    ]); ?>
        <?= Yii::$app->session->getFlash($key) ?>
    <? \yii\bootstrap\Alert::end(); ?>
<? endforeach; ?>

<?php
$form = \yii\bootstrap\ActiveForm::begin([
    'id' => 'login-form',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ],
]); ?>

<?= $form->field($modelResetForm, 'email')->textInput() ?>
    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <button type="submit" class="btn btn-primary" >Отправить</button>
        </div>
    </div>

<?php \yii\bootstrap\ActiveForm::end(); ?>