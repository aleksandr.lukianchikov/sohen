<?php
$form = \yii\bootstrap\ActiveForm::begin([
    'id' => 'login-form',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ],
]); ?>

<?= $form->field($modelPasswordsForm, 'password')->passwordInput() ?>
<?= $form->field($modelPasswordsForm, 'password_confirm')->passwordInput() ?>
    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <button type="submit" class="btn btn-primary" >Отправить</button>
        </div>
    </div>
<?php \yii\bootstrap\ActiveForm::end(); ?>