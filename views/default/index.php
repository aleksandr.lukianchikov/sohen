<?php
//var_dump(phpversion());
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this yii\web\View */
/* @var $pages \yii\data\Pagination */
/* @var $searchModel app\models\ProducerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Producers';

NavBar::begin([

]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right','style' => 'margin-right: 0px;'],
    'items' => [
        ['label' => t('Мой личный кабинет'), 'url' => ['/private-office/view', 'user_id' => Yii::$app->request->get('user_id')]],

    ],

]);
NavBar::end();

?>

<p id="choose_producer"><?php echo t('Выбери производителя') . '&nbsp;&nbsp;' ?><span id="and"><?php echo t('и')?></span></p>
<p id="save-with-us"><?php echo t('ЭКОНОМЬ С НАМИ!')?></p>
<?php
$models = $dataProvider->getModels();
?>
<div class="products">
    <?php foreach ($models as $i => $producer): ?>
        <?php if ($i % 3 == 0): ?>
            <div class="row">
        <?php endif; ?>
        <div class="col-md-4" style="padding-right: 10px; ">
            <a href="<?= \yii\helpers\Url::toRoute(['product/index', 'producer_id' => $producer->id]) ?>">
                <?= Html::img('uploads/'.$producer->image_1); ?>
                <span class="choose_producer_name"> <?= $producer->name ?></span>
            </a>
        </div>
        <?php if ($i % 2 == 0 && $i || count($models) == $i + 1): ?>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
    <?php
    // display pagination
    echo LinkPager::widget([
        'pagination' => $dataProvider->getPagination(),
    ]);
    ?>
</div>
