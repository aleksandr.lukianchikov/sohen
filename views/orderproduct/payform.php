<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$sum = $modelPayForm->sum;

?>
    <h1><?= '№ Заказа' . ' ' . Html::encode($id) ?></h1>

<h2><?= Html::encode($modelPayForm->sum . ' ' . 'шекелей') ?></h2>


<?php
$form = \yii\bootstrap\ActiveForm::begin([
    'id' => 'login-form',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ],
]); ?>

<?// $form->field($modelPayForm, 'supplier')->textInput() ?>
<?// $form->field($modelPayForm, 'sum')->textInput() ?>
<?// $form->field($modelPayForm, 'currency')->textInput() ?>
<?= $form->field($modelPayForm, 'ccno')->textInput() ?>
<?= $form->field($modelPayForm, 'expdate')->textInput() ?>
<?= $form->field($modelPayForm, 'myid')->textInput() ?>
<?= $form->field($modelPayForm, 'mycvv')->textInput() ?>
<?// $form->field($modelPayForm, 'cred_type')->textInput() ?>
<?// $form->field($modelPayForm, 'TranzilaPW')->textInput() ?>



    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <button type="submit" class="btn btn-primary" ><?=t('Оплатить'); ?></button>
            <?= Html::a(' Оплатитьь', ['orderproduct/payform','modelPayForm'=>$modelPayForm, 'id' => $id , 'sum' => $sum], ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>

<?php \yii\bootstrap\ActiveForm::end(); ?>