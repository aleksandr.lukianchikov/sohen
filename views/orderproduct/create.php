<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \app\models\OrderProduct[] $modelOrderProducts
 * @var \app\models\Producer $producerModel
 */
$str_text_1 = t('Вернуться в корзину');
$str_text_2 = t('ОПЛАТИТЬ');
?>

<div class="row">
    <div class="col-xs-9">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">
                    <div class="row">
                        <div class="col-xs-6">
                            <h5><span class="glyphicon glyphicon-shopping-cart"></span> <?= $producerModel->name; ?></h5>
                        </div>
                        <div class="col-xs-6">
                            <?= Html::a('<span class="glyphicon glyphicon-share-alt"></span>' .  $str_text_1, ['cart/index', 'producer_id' => $producerModel->id], ['class' => 'btn btn-primary pull-right']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); $total = 0; ?>
                <? foreach ($modelOrderProducts as $i => $orderProduct): ?>
                    <div id="cart-<?= $orderProduct->id ?>" class="row cart-item-row">

                        <div class="col-xs-4">
                            <h4 class="product-name"><strong>
                                    <?= te($orderProduct->product,'name'); ?></strong></h4><h4></h4>
                        </div>
                        <div class="col-xs-4">
                            <h6 class="barcode-name"><strong> <?= $orderProduct->product->barcode ?></strong></h6>
                        </div>
                        <div class="col-xs-4">
                            <div class="col-xs-4 text-right">
                                <h6><strong><?= $orderProduct->product->price ?> <span class="text-muted">x</span></strong></h6>
                            </div>
                            <div class="col-xs-4 text-right">
                                <h6><strong><?= $orderProduct->amount ?> <span class="text-muted"></span></strong></h6>
                            </div>
                            <div class="col-xs-4 text-right">
                                <?php  $s = $orderProduct->amount * $orderProduct->product->price ?>
                                <h6><strong><?php $total = $total + $s;?><?=$s?> <span class="text-muted"></span></strong></h6>
                            </div>
                            <div class="col-xs-2">

                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
                <div class="row">
                    <div class="text-center">
                        <div class="col-xs-9">

                        </div>
                        <div class="col-xs-3">

                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="row text-center">
                <div class="col-xs-12">
                    <h4 class="text-right"><?php echo t('итого до акций: ')?><strong><span class="amount-total"><?=$totalProducts ?> шек.</span></strong></h4>
                </div>
            </div>
            <div class="row">
                <?php $totalPlusDiscount = $total; ?>
                <? foreach ($modelOrderDiscounts as $i => $orderDiscount): ?>
                <div>
                    <div class="col-xs-2"  id="discount"><?=t('АКЦИЯ')?></div> <div class="col-xs-6" id="discount-name"><?=te($orderDiscount->discount,'name')?></div>
                    <div class="col-xs-1" id="discount-percent"><?=$orderDiscount->discount->percent.'%'?></div>
                    <div class="col-xs-2" id="discount-sum">-<?=$orderDiscount->sum. t(' шек.')?><?php $totalPlusDiscount = $totalPlusDiscount - $orderDiscount->sum ?></div>

                </div>
                <? endforeach; ?>
            </div>
            <div class="panel-footer">
                <div class="row text-center">
                    <div class="col-xs-9">
                        <h4 class="text-right"><?=t('ИТОГО: ')?><strong><span class="amount-total"><?=$totalOrder?><?=t(' шек.')?></span></strong></h4>
                    </div>

                    <div class ="col-xs-3">
                        <?= Html::a('<span ></span>' .  $str_text_2, ['orderproduct/pay'], ['class' => 'btn btn-success btn-block']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
