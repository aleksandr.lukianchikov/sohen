<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
\app\assets\NotifyAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'SOHEN',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $classPosition = \app\models\enums\LocaleEnum::isRTL() ? 'navbar-left' : 'navbar-right';
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav ' . $classPosition],
        'items' => [
            ['label' => t('Главная'), 'url' => ['/default/index']],
            ['label' => t('О нас'), 'url' => ['/site/about']],

            ['label' => t('Контакты'), 'url' => ['/site/contact']],
            ['label' => t('Вход'), 'url' => ['/site/login'], 'visible' => Yii::$app->user->isGuest],
            ['label' => t('Язык'),
                'items' => array_map(function ($code) {
                    return [
                        'label' => Yii::$app->params['availableLocales'][$code],
                        'url' => ['/site/set-locale', 'locale' => $code],
                        'active' => Yii::$app->language === $code
                    ];
                }, array_keys(Yii::$app->params['availableLocales']))
            ],
            ['label' => Yii::$app->user->identity ? Yii::$app->user->identity->shop_name : '',
                'items' => [
                    ['label' => t('Личный кабинет'), 'url' => ['private-office/view']],
                    ['label' => t('Выход'), 'url' => ['/site/logout']],


                ],
                'visible' => !Yii::$app->user->isGuest,
            ],
        ],

    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
