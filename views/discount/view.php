<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\models\enums\Type;
use app\models\Product;

/* @var $this yii\web\View */
/* @var $model app\models\Discount */
$str = t('СКИДКА В ПРОЦЕНТАХ:');
$str_text = t('КОЛИЧЕСТВО ТОВАРА ПО СКИДКЕ:');
 if("he-IL" !=  Yii::$app->language) {
     $this->title = $model->name;
 }
 else{
     $this->title = $model->name_he;
 }
NavBar::begin([
    'brandLabel' => t('Вернуться к выбору производителя'),
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        //'class' => 'navbar-inverse navbar-fixed-top',
    ],


]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        ['label' => t('Вернуться в магазин'), 'url' => ['/product/index', 'producer_id' => $model->producer_id]],
        ['label' => t('Мой личный кабинет'), 'url' => ['/private-office/view']],
        ['label' => t('Моя корзина'), 'url' => ['product/index','producer_id'=> $model->producer_id]],


    ],

]);
NavBar::end();
?>
<div class="discount-view">

    <h1 <?php if("he-IL" ==  Yii::$app->language){ ?>style ="text-align: right "; <?php } ?> id="discount_title" ><?= Html::encode($this->title) ?></h1>

    <? if($model->type != 'only_picture'): ?>
    <?php if("he-IL" ==  Yii::$app->language){?>
    <div class="item  col-xs-6 col-lg-6">
        <div class="thumbnail">
            <?='<img class="img-responsive" src="uploads/' . $model->image_1 .' "/> ' ?>
        </div>
    </div>

    <div id="discount_view" class="caption" style="text-align: right">



        <h4  class="group inner list-group-item-text"><?= $model->producer->name . ' ' ; ?><span class="view_product_span"><?=t('ПРОИЗВОДИТЕЛЬ:')?></span> </h4>
        <h4  class="group inner list-group-item-text"><?= $model->date_start . ' '; ?><span class="view_product_span"><?=t('ДАТА НАЧАЛА АКЦИИ:')?></span> </h4>
        <h4  class="group inner list-group-item-text"><?= $model->date_end . ' ' ;?><span class="view_product_span"><?=t('ДАТА ОКОНЧАНИЯ АКЦИИ:')?></span> </h4>
        <h4  class="group inner list-group-item-text"> <span class="view_product_span"><?=t('ТИП АКЦИИ:'). ' ';?></span><?= Type::getClientValue($model->type). ' ';?></h4>


        <h4  <?php if($model->percent == 100){  $str=t('В ПОДАРОК');} ?>class="group inner list-group-item-text">
            <?= $model->percent . " %" ?> <span class="view_product_span"><?= $str ?></span></h4>

        <h4  <?php if($model->discription == NULL){ ?> hidden <?php } ?> class="group inner list-group-item-text"><span class="view_product_span"><?=t('ОПИСАНИЕ:')?></span> <p><?= te($model ,'discription') . ' '; ?></p>
            </h4>

        <h4  <?php if($model->product_id == NULL){ ?> hidden <?php } ?>class="group inner list-group-item-text" ><span class="view_product_span"><?=t('УСЛОВИЕ УЧАСТИЯ В АКЦИИ - ПОКУПКА ПРОДУКТА:')?></span>
            <?= $model->getProductConditionName();?>
             </h4>

        <h4  <?php if($model->min_qnt_product == NULL){ ?> hidden <?php } ?> class="group inner list-group-item-text"> <?= $model->min_qnt_product ?>
            <span class="view_product_span"><?=t('МИНИМАЛЬНОЕ КОЛИЧЕСТВО УПАКОВОК:')?></span></h4>

        <h4  <?php if($model->product_gift_id == NULL && $model->product_gift_id == $model->product_id){ ?> hidden <?php } ?>class="group inner list-group-item-text" >
            <span class="view_product_span"><?=t('ПРОДУКТ ПО АКЦИИ:')?></span>
            <?= $model->getProductName();?> </h4>

        <h4  <?php if($model->qnt_product_gift == NULL){ ?> hidden <?php } ?>
            <?php if($model->percent == 100){  $str_text =t('КОЛИЧЕСТВО ТОВАРОВ В ПОДАРОК:');} ?>class="group inner list-group-item-text">
            <?= $model->qnt_product_gift?> <span class="view_product_span"><?= $str_text ?></span></h4>
    </div>
 <?php } else {?>
            <div class="item  col-xs-6 col-lg-6">
                <div class="thumbnail">
                    <?='<img class="img-responsive" src="uploads/' . $model->image_1 .' "/> ' ?>
                </div>
            </div>

    <div id="discount_view" class="caption" >
        <h4  class="group inner list-group-item-text"><span class="view_product_span">ПРОИЗВОДИТЕЛЬ:</span> <?= $model->producer->name; ?></h4>
        <h4  class="group inner list-group-item-text"><span class="view_product_span">ДАТА НАЧАЛА АКЦИИ:</span> <?= $model->date_start?></h4>
        <h4  class="group inner list-group-item-text"><span class="view_product_span">ДАТА ОКОНЧАНИЯ АКЦИИ:</span> <?= $model->date_end?></h4>
        <h4  class="group inner list-group-item-text"><span class="view_product_span">ТИП АКЦИИ:</span> <?= Type::getClientValue($model->type)?></h4>

        <h4  <?php if($model->percent == 100){  $str='В ПОДАРОК';} ?>class="group inner list-group-item-text">
            <span class="view_product_span"><?= $str ?></span> <?= $model->percent . " %"?></h4>

        <h4  <?php if($model->discription == NULL){ ?> hidden <?php } ?> class="group inner list-group-item-text">
            <span class="view_product_span">ОПИСАНИЕ:</span> <?= $model->discription?></h4>

        <h4  <?php if($model->product_id == NULL){ ?> hidden <?php } ?>class="group inner list-group-item-text" >
            <span class="view_product_span">УСЛОВИЕ УЧАСТИЯ В АКЦИИ - ПОКУПКА ПРОДУКТА:</span> <?= $model->getProductConditionName();?></h4>

        <h4  <?php if($model->min_qnt_product == NULL){ ?> hidden <?php } ?> class="group inner list-group-item-text">
            <span class="view_product_span">МИНИМАЛЬНОЕ КОЛИЧЕСТВО УПАКОВОК:</span> <?= $model->min_qnt_product?></h4>

        <h4  <?php if($model->product_gift_id == NULL && $model->product_gift_id == $model->product_id){ ?> hidden <?php } ?>class="group inner list-group-item-text" >
            <span class="view_product_span">ПРОДУКТ ПО АКЦИИ:</span> <?= $model->getProductName();?></h4>

        <h4  <?php if($model->qnt_product_gift == NULL){ ?> hidden <?php } ?>
            <?php if($model->percent == 100){  $str_text ='КОЛИЧЕСТВО ТОВАРОВ В ПОДАРОК:';} ?>class="group inner list-group-item-text">
            <span class="view_product_span"><?= $str_text ?></span> <?= $model->qnt_product_gift?></h4>
    </div>

<?php }?>

    <? endif; ?>
    <? if($model->type == 'only_picture'): ?>
        <div class="item  col-xs-8 col-lg-6=8">
            <div class="thumbnail">
                <?='<img class="img-responsive" src="uploads/' . $model->image_1 .' "/> ' ?>
            </div>
        </div>
    <? endif; ?>

</div>
