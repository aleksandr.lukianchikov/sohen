<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\enums\Type;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DiscountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('Акции');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if("he-IL" ==  Yii::$app->language) {
        $columns = [
            ['class' => 'yii\grid\ActionColumn'],
            // 'percent',
            // 'qnt_product_gift',
            // 'product_gift_id',
            // 'user_id',
            // 'sum',
            // 'date_start',
            // 'date_end',
            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],
            // 'image_1',
            'min_qnt_product',
            [
                'attribute' => 'product_id',
                'value' => function($model){
                    $rc = NULL;
                    $productModel = $model->getProduct()->one();
                    if($productModel != NULL){
                        $rc = $productModel->name_he;
                    }
                    return $rc;
                    //return $model->getProductConditionName();
                }

            ],
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    return Type::getClientValue($model->type);
                }
            ],
            'name_he',
            ['class' => 'yii\grid\SerialColumn'],

            ];
    }
    else{
        $columns = [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    return Type::getClientValue($model->type);
                }
            ],
            [
                'attribute' => 'product_id',
                'value' => function($model){
                    $rc = NULL;
                    $productModel = $model->getProduct()->one();
                    if($productModel != NULL){
                        $rc = $productModel->name;
                    }
                    return $rc;
                    //return $model->getProductConditionName();
                }

            ],
            'min_qnt_product',
            // 'percent',
            // 'qnt_product_gift',
            // 'product_gift_id',
            // 'user_id',
            // 'sum',
            // 'date_start',
            // 'date_end',
            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],
            // 'image_1',

            ['class' => 'yii\grid\ActionColumn'],
        ];
    }
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => t('не задано')],

        'columns' => $columns,
    ]);?>
</div>
