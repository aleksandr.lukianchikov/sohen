<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\Kosher;
use app\models\enums\Available;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $producerModel \app\models\Producer */

$this->title = $model->name;
//$lang =  Yii::$app->language; "he-IL"
//var_dump($lang);die;
NavBar::begin([
    'brandLabel' => t('Вернуться к выбору производителя'),
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        //'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
$classPosition = \app\models\enums\LocaleEnum::isRTL() ? 'navbar-left' : 'navbar-right';
echo Nav::widget([
    'options' => ['class' => 'navbar-nav ' . $classPosition, 'style' => 'margin-right: 0px;'],
    'items' => [
        ['label' => t('Мой личный кабинет'), 'url' => ['/user/index', 'user_id' => Yii::$app->request->get('user_id')]],
        ['label' => t('Моя корзина'), 'url' => ['/cart/index', 'producer_id' => $model->producer_id]],
        ['label' => t('Вернуться в магазин'), 'url' => ['/product/index', 'producer_id' => $model->producer_id]],
    ],

]);
NavBar::end();


//$img = $model->image_1 ? ('/uploads/' . $model->image_1) : 'http://placehold.it/400x250/000/fff';
?>
<div class="row">
    <div class="col-md-6">
        <div class="item">
            <div class="thumbnail">
                <?php
                use yii\bootstrap\Carousel;

                $carousel = [];

                for ($i = 0; $i < count($model->getImageFields()); $i++) {
                    if (!empty($model->{'image_' . ($i + 1)})) {
                        $carousel[$i]['content'] = '<img src="uploads/' . $model->{'image_' . ($i + 1)} . '"/>';
                        $carousel[$i]['caption'] = '';
                        $carousel[$i]['options'] = [];

                    }
                }

                echo Carousel::widget([
                    'items' => $carousel,
                    'options' => ['class' => 'carousel slide'],
                    'controls' => [
                        '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
                        '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'
                    ]
                ]);

                ?>
                <? // Html::img($img, ['class' => 'group list-group-image']) ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">

            <?php if("he-IL" !=  Yii::$app->language){?>
        <div class="caption">
                <h2 id="view_product_name" class="group inner list-group-item-heading"><?= te($model, 'name');?></h2>

                <h4 id="view_product_pack_gram"
                    class="group inner list-group-item-text"><?= $model->pack_gram . t(' грамм') ?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span"><?php echo t('БАРКОД')?></span> <?= $model->barcode?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span"><?php echo t('ТИП УПАКОВКИ:')?></span> <?= te($model->packType, 'name'); ?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span"><?php echo t('ЦЕНА:')?></span> <?= $model->price . t(' шек.') ?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span"><?php echo t('ПРОИЗВОДИТЕЛЬ:')?></span> <?= te($model->producer, 'name'); ?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span"><?php echo t('СРОК ХРАНЕНИЯ:')?></span> <?= $model->shelf_life . t(' суток. ') ?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span"><?php echo t('УСЛОВИЯ ХРАНЕНИЯ')?></span> <?= te($model->storageConditions, 'name'); ?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span"><?php echo t('В НАЛИЧИИ:')?></span> <?= Available::getClientValue($model->available) ?>
                </h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span"><?php echo t('КОШЕРНОСТЬ:')?></span> <?= Kosher::getClientValue($model->kosher) ?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span"><?php echo t('ТИП ПРОДУКТА:')?></span> <?= te($model->productType, 'name'); ?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span"><?php echo t('ОПИСАНИЕ:')?></span> <?= te($model, 'discription');?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span"><?php echo t('СОСТАВ:')?></span> <?= te($model, 'composition');?></h4>

            <?php }?>
        </div>
            <?php if("he-IL" ==  Yii::$app->language){?>
        <div class="caption" style="text-align: right">
                <h2 id="view_product_name" class="group inner list-group-item-heading"><?= te($model, 'name');?></h2>

                <h4 id="view_product_pack_gram"
                    class="group inner list-group-item-text"><?= t(' грамм') ?> <?= $model->pack_gram  . ' '?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span_he"><?php echo t('БАРКОД')?></span> <?= $model->barcode?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span_he"><?php echo t('ТИП УПАКОВКИ:')?></span> <?= te($model->packType, 'name'); ?></h4>
                <h4 class="group inner list-group-item-text">  <span
                            class="view_product_span_he"><?=t(' шек.')?> <?php echo t('ЦЕНА:')?></span>  <?= $model->price . ' ' ?></h4>
                <h4 class="group inner list-group-item-text">  <?= te($model->producer, 'name'); ?> <span
                            class="view_product_span_he">:<?php echo t('ПРОИЗВОДИТЕЛЬ:')?></span></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span_he"><?php echo t('СРОК ХРАНЕНИЯ:')?></span> <?= $model->shelf_life . t(' суток. ') ?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span_he"><?php echo t('УСЛОВИЯ ХРАНЕНИЯ')?></span> <?= te($model->storageConditions, 'name'); ?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span_he"><?php echo t('В НАЛИЧИИ:')?></span> <?= Available::getClientValue($model->available) ?>
                </h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span_he"><?php echo t('КОШЕРНОСТЬ:')?></span> <?= Kosher::getClientValue($model->kosher) ?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span_he"><?php echo t('ТИП ПРОДУКТА:')?></span> <?= te($model->productType, 'name'); ?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span_he"><?php echo t('ОПИСАНИЕ:')?></span> <?= te($model, 'discription');?></h4>
                <h4 class="group inner list-group-item-text"><span
                            class="view_product_span_he"><?php echo t('СОСТАВ:')?></span> <?= te($model, 'composition');?></h4>
            <?php }?>


        </div>
        <div class="col-xs-12 col-md-6">
            <a class="btn btn-success to-cart"  data-url="<?= Url::to(['/cart/add', 'id' => $model->id ])?>" href="#"><?php echo t('В корзину')?></a>
        </div>

    </div>
</div>
<?php

/*$this->registerJs("
    $(function(){
        $('.to-cart').on('click', function(e){
            var url = $(this).data('url');
            $.get(url, function(){
                alert('Добавлено');
            });
            e.preventDefault();
        });
    });    
");

?>
*/
$this->registerJs("
    $(function(){
        $('.to-cart').on('click', function(e){
            var url = $(this).data('url');
            $('#product_count').modal('show').data('url', url);
            e.preventDefault();                
        });
        $('.add-to-cart').on('click', function(e){
            var modal = $('#product_count');
            var amount = modal.find('input[name=\"amount\"]').val();   
            if(amount != 0){
            var url = modal.data('url') + '&amount=' + amount;  
                   
            $.get(url, function(){
                modal.modal('hide');
                // http://bootstrap-growl.remabledesigns.com
                $.notify('Product has added to cart.');
            });  
             }   
            e.preventDefault();
           
            
        });
    });    
");
echo $this->render('_modal');
?>