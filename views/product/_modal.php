<?php
/**
 * @var $this \yii\web\View
 */
use yii\bootstrap\Modal;

Modal::begin([
    'id' => 'product_count',
    'header' => t('Укажите количество товара'),
    'size' => Modal::SIZE_SMALL,
]);

echo \yii\bootstrap\Html::textInput('amount', '', ['type' => 'number']);
echo \yii\bootstrap\Html::button('Add', ['class' => 'btn btn-success btn-sm add-to-cart', 'style' => 'float: right;']);
Modal::end();
$this->registerJs("
    $('#product_count').on('show.bs.modal', function(e){
        $(e.target).find('input').val('');                
    });        
    $('#product_count').on('shown.bs.modal', function (e) {
        $(e.target).find('input').focus();
    }) 
");
