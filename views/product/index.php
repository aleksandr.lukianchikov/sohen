<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\ProductType;
use app\models\Discount;
use execut\widget\TreeView;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\Carousel;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $product \app\models\Product */
/* @var $producerModel \app\models\Producer */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Products';
//$this->params['breadcrumbs'][] = $this->title;
//горизотальное меню
$shekel = '&#8362 ';
?>

<?php
NavBar::begin([
    'brandLabel' => t('Вернуться к выбору производителя'),
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        //'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
$classPosition = \app\models\enums\LocaleEnum::isRTL() ? 'navbar-left' : 'navbar-right';
echo Nav::widget([
    'options' => ['class' => 'navbar-nav ' . $classPosition, 'style' => 'margin-right: 0px;'],
    'items' => [
        ['label' => t('Все скидки, акции'), 'url' => ['/discount/index', 'producer_id' => Yii::$app->request->get('producer_id')]],
        ['label' => t('Мой личный кабинет'), 'url' => ['/private-office/view', 'user_id' => Yii::$app->request->get('user_id')]],
        ['label' => t('Моя корзина'), 'url' => ['/cart/index', 'producer_id' => Yii::$app->request->get('producer_id')]],

    ],

]);
NavBar::end();

?>

<div class="row">
    <div class="col-md-3">

        <div id = "dropdown" class="dropdown">
            <button id = "producer_data" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><?=$producerModel->name ?>
                <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li ><?= Html::a(t('КОНТАКТНЫЕ ДАННЫЕ'), ['/producer/contacts', 'id' => $producerModel->id], ['tabindex' => '-1']) ?></li>
                <li ><?= Html::a(t('О НАС'), ['/producer/view', 'id' => $producerModel->id], ['tabindex' => '-1']) ?></li>
                <li ><?= Html::a(t('ОТПРАВИТЬ E_MAIL'), ['/producer/send-email', 'id' => $producerModel->id], ['tabindex' => '-1']) ?></li>

            </ul>
        </div>
        <div class="menu-types">
            <?= \yii\widgets\Menu::widget([
                'items' => ProductType::getChildren($producerModel->id, 0),
                'activateParents' => true,
            ]); ?>
        </div>
    </div>
    <div class="col-md-9">
        <div class="hidden-xs ui-slider-horizontal">
            <?php
            $discounts = $producerModel->getDiscounts()->where(['type' => ['only_picture','prod_assoc','sum_assoc','feast_assoc']])->all();
            $carousel = [];

            foreach ($discounts as $ind => $discount) {
                $carousel[$ind]['content'] = '<img src="uploads/' . $discount->image_1 . '"style="height: 200px;width: 100%;"/>';
                $carousel[$ind]['caption'] = '<h1>' . $discount->name . '</h1><p></p>
                         <p><a href=" ' . \yii\helpers\Url::toRoute
                    (['discount/view', 'id' => $discount->id]) . ' " class="btn btn-primary">' . t('подробнее') . '<span class="glyphicon glyphicon-chevron-right"></a></p>';
                $carousel[$ind]['options'] = [];
            }
            //           var_dump($carousel); die;
            echo Carousel::widget([
                'items' => $carousel,
                'options' => ['class' => 'carousel slide', 'data-interval' => '2000'],
                'controls' => [
                    '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
                    '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'
                ]
            ]);
            ?>
        </div>

        <div id="products" class="row list-group">
            <?php foreach ($dataProvider->getModels() as $product): ?>
                <?php
                $img = $product->image_1 ? ('/uploads/' . $product->image_1) : 'http://placehold.it/400x250/000/fff';
                ?>
                <div class="item  col-xs-12 col-sm-6  col-lg-4">
                    <div class="thumbnail">
                        <a href="<?= \yii\helpers\Url::toRoute(['product/view', 'id' => $product->id]) ?> ">
                            <?php if($product->hasDiscount() == 2) echo Html::img('images/percent.png', ['class' => 'discount-icon']) ?>
                            <?php if($product->hasDiscount() == 3) echo Html::img('images/sale.png', ['class' => 'sale-icon']) ?>
                            <?php if($product->isNew()) echo Html::img('images/new.png', ['class' => 'new-icon']) ?>
                            <? /*Html::img('images/new.jpg', ['class' => 'new-icon']) */ ?>
                            <?= Html::img($img, ['class' => 'group list-group-image']) ?>
                        </a>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading"><?= te($product, 'name'); ?></h4>
                            <p class="group inner list-group-item-text"><?= $product->pack_gram . t(' грамм') ?></p>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <?php if("he-IL" ==  Yii::$app->language){?>
                                    <p class="lead">
                                        <?= $shekel . $product->price ?>
                                    </p>
                                    <?php }
                                    else{ ?>
                                    <p class="lead">
                                    <?= $product->price . ' ' . $shekel ?>
                                    </p>
                                    <?php } ?>

                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <a class="btn btn-success to-cart" data-url="<?= Url::to(['/cart/add', 'id' => $product->id])?>" href="#"><?php echo t('В корзину')?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?php
// display pagination
echo \yii\widgets\LinkPager::widget([
    'pagination' => $dataProvider->getPagination(),
]);
$this->registerJs("
    $(function(){
        $('.to-cart').on('click', function(e){
            var url = $(this).data('url');
            $('#product_count').modal('show').data('url', url);
            e.preventDefault();                
        });
        $('.add-to-cart').on('click', function(e){
            var modal = $('#product_count');
            var amount = modal.find('input[name=\"amount\"]').val();   
            if(amount != 0){
            var url = modal.data('url') + '&amount=' + amount;  
                   
            $.get(url, function(){
                modal.modal('hide');
                // http://bootstrap-growl.remabledesigns.com
                $.notify('Product has added to cart.');
            });  
             }   
            e.preventDefault();
           
            
        });
    });    
");
echo $this->render('_modal');
?>

