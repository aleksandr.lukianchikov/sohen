<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\Kosher;
use app\models\enums\Available;
use app\models\enums\StatusNew;

/* @var $this yii\web\View */
/* @var $model app\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'kosher')->dropDownList(Kosher::getClientValues(), ['prompt' => 'Выберите...']) ?>
    <?= $form->field($model, 'status_new')->dropDownList(StatusNew::getClientValues(), ['prompt' => 'Выберите...']) ?>
    <?= $form->field($model, 'available')->dropDownList(Available::getClientValues(), ['prompt' => 'Выберите...']) ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'product_type_id') -> dropDownList($model->getProductTypeList(), ['prompt' => 'Выберите...']) ?>
    <?= $form->field($model, 'price') ?>

    <?php //echo $form->field($model, 'composition') ?>

    <?php // echo $form->field($model, 'pack_gram') ?>

    <?php // echo $form->field($model, 'calories') ?>

    <?php // echo $form->field($model, 'storage_conditions_id') ?>

    <?php // echo $form->field($model, 'kosher') ?>

    <?php // echo $form->field($model, 'available') ?>

    <?php // echo $form->field($model, 'status_new') ?>

    <?php // echo $form->field($model, 'producer_id') ?>

    <?php // echo $form->field($model, 'image_1') ?>

    <?php // echo $form->field($model, 'image_2') ?>

    <?php // echo $form->field($model, 'image_3') ?>

    <?php // echo $form->field($model, 'shelf_life') ?>

    <?php // echo $form->field($model, 'product_type_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
