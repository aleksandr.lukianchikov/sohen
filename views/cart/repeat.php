<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\models\enums\LocaleEnum;

/**
 * @var \yii\web\View $this
 * @var \app\models\ShopCart[] $cartProductModels
 * @var \app\models\Producer $producerModel
 */


$textOrder = t('ОФОРМИТЬ ЗАКАЗ');
?>

<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">
                    <?php if(LocaleEnum::isRTL()){ ?>


                        <div class="row">
                            <div class="col-xs-6">
                                <?= Html::a('<span class="glyphicon glyphicon-share-alt"></span>'. $str_btn_name, ['product/index', 'producer_id' => $producerModel->id], ['class' => 'btn btn-primary pull-left']) ?>
                            </div>
                            <div class="col-xs-6">
                                <h5><span class="glyphicon glyphicon-shopping-cart"></span> <?= $producerModel->name; ?></h5>
                            </div>
                        </div>

                    <?php } else { ?>

                        <div class="row">
                            <div class="col-xs-6">
                                <h5><span class="glyphicon glyphicon-shopping-cart"></span> <?= $producerModel->name; ?></h5>
                            </div>
                            <div class="col-xs-6">
                                <?= Html::a('<span class="glyphicon glyphicon-share-alt"></span>'. $str_btn_name, ['product/index', 'producer_id' => $producerModel->id], ['class' => 'btn btn-primary pull-right']) ?>
                            </div>
                        </div>

                    <?php } ?>
                </div>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                <? foreach ($cartProductModels as $i => $cartProduct): ?>
                    <div id="cart-<?= $cartProduct->id ?>" class="row cart-item-row">
                        <div class="col-sm-2">
                            <img class="img-responsive"  src="uploads/<?= $cartProduct->product->image_1 ?> "  style="height: 80px; width:80px;"/>
                        </div>
                        <div class="col-sm-4">
                            <h4 class="product-name"><strong><?= te($cartProduct->product, 'name'); ?></strong></h4><h4></h4>
                        </div>
                        <div class="col-sm-6">
                            <div class="col-xs-6 text-right">
                                <h6><strong><?= $cartProduct->product->price ?> <span class="text-muted">x</span></strong></h6>
                            </div>
                            <div class="col-xs-4">
                                <?= Html::activeHiddenInput($cartProduct, "[$i]id"); ?>
                                <?= $form->field($cartProduct, "[$i]amount")->textInput([
                                    'data-price' => $cartProduct->product->price,
                                    'class' => 'form-control amount-input',
                                ])->label(false) ?>
                            </div>
                            <div class="col-xs-2">
                                <button  data-url="<?= Url::to(['delete', 'id' => $cartProduct->id]) ?>" type="button" class="btn btn-link btn-xs trash">
                                    <span class="glyphicon glyphicon-trash"> </span>
                                </button>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
                <div class="row">
                    <div class="text-center">
                        <div class="col-md-9">

                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-default btn-sm btn-block">
                                Обновить корзину
                            </button>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="panel-footer">
                <div class="row text-center">
                    <div class="col-md-9">
                        <h4 class="text-right">Total <strong>шек.<span class="amount-total">0</span></strong></h4>
                    </div>
                    <div class ="col-md-3">
                        <? if (!empty($cartProductModels)): ?>
                            <?= Html::a($textOrder, ['orderproduct/create', 'producer_id' => $producerModel->id], ['class' => 'btn btn-success btn-block']) ?>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?
$this->registerJs("
        $('.trash').on('click', function(){
            var url = $(this).data('url'),
                id = $(this).data('id');        
            $.get(url, function(){
                $('#cart-' + id).remove();
                var firstAmountInput = $('.amount-input').eq(0);                
                if (!firstAmountInput.length) {
                    $('.amount-total').text(0);
                }
                firstAmountInput.trigger('keyup');
            });            
        });
        
        $('.amount-input').on('keyup', function(){
            var sum = 0;
            $('.amount-input').each(function(){
                var el = $(this);
                var price = parseFloat(el.data('price'));                
                if (!el.val() || !price) {
                    return;
                }
                var amount = parseInt(el.val());
                sum += price * amount;         
            }); 
            $('.amount-total').text(sum);
        });
        $('.amount-input').eq(0).trigger('keyup');                    
    ");
?>
