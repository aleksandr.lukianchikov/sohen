<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\enums\OrderStatus;

/* @var $this yii\web\View */
/* @var $model app\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?
$containerOptions = ['class' => 'collapse', 'id' => 'collapseFilters'];
if ($model->isActiveFilter()) {
    Html::addCssClass($containerOptions, 'in');
}
?>
<?= Html::beginTag('div', $containerOptions) ?>
<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>


    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'order_date') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'orderstatus')->dropDownList(\app\models\enums\OrderStatus::getClientValues(), ['prompt'=>t('Выберите нужное')]) ?>

    <?= $form->field($model, 'producer_id')->dropDownList($model->getProducerList(),['prompt'=>t('Не выбрано')])  ?>

    <?= $form->field($model, 'sum') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сброс', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
