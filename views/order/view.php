<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\enums\OrderStatus;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $searchModel app\models\OrderSearch */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => t('Заказы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_date',
            [
                'attribute'=>'user_id',
                'value' => function($model){
                    return $model->getUser()->one()->shop_name;
                }

            ],
            //'orderstatus',
            [
                'attribute' => 'orderstatus',
                'value' => function ($model) {
                    return OrderStatus::getClientValue($model->orderstatus);
                }
            ],

            [
                'attribute'=>'producer_id',
                'value' => function($model){
                    return $model->getProducer()->one()->name;
                }

            ],

            'sum'
        ],
    ]) ?>

</div>
