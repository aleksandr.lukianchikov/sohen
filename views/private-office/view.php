<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\bootstrap\Nav;



/* @var $this yii\web\View */
/* @var $model app\models\Discount */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container">
    <div class="jumbotron text-center">

        <?php if("he-IL" !=  Yii::$app->language){?>
            <h2 id="private-office-header"><?php echo t( 'Личный кабинет представителя магазина')?> "<?= getUserModel()->shop_name ?>"</h2>
        <?php }?>
        <?php if("he-IL" ==  Yii::$app->language){?>
            <h2 id="private-office-header">"<?= getUserModel()->shop_name ?>"<?php echo t( 'Личный кабинет представителя магазина')?> </h2>
        <?php }?>
    </div>

    <h2 id="choose-action"<?php if("he-IL" ==  Yii::$app->language){?>style ="text-align:right";<?php }?> ><?php echo t( 'Выберите действие')?></h2>
    <ul class="list-group"<?php if("he-IL" ==  Yii::$app->language){?>style ="text-align:right";<?php }?>>
        <li id = "click-to-cart" class="list-group-item list-group-item-danger" ><a href="index.php?r=/cart/index"><?php echo t('Текущая корзина')?></a></li>
        <li class="list-group-item list-group-item-warning"><a href="index.php?r=order/index"  ><?php echo t('История заказов')?></a></li>
        <li class="list-group-item list-group-item-success" > <a href="index.php?r=user/view" ><?php echo t( 'Регистрационные данные')?></a></li>
        <li id="producer_is_return" class="list-group-item list-group-item-info"><a href="/index.php?r=productreturn/create" > <?php echo t( 'Оформить возврат товара')?></a></li>
        <li class="list-group-item list-group-item-info"><a href="/index.php?r=productreturn/index" > <?php echo t( 'История возвратов')?></a></li>
        <li id = "producer_is_compen" class="list-group-item list-group-item-warning"> <a href="/index.php?r=compensation/create" ><?php echo t( 'Оформить компенсацию за непроданный товар у которого заканчивается срок хранения')?></a></li>
        <li id = "producer_is_compen" class="list-group-item list-group-item-warning"> <a href="/index.php?r=compensation/index" ><?php echo t( 'История заявок на компенсацию')?></a></li>
        <li class="list-group-item list-group-item-danger"><a href="/index.php?r=default/index"  > <?php echo t('Войти в магазин')?></a></li>

    </ul>

</div>
<?php
$this->registerJs("
    $(function(){
    
            $('#click-to-cart').on('click', function(e){
            var url = $(this).data('url');
            $('#check_producer').modal('show').data('url', url);
            e.preventDefault();
        });
        
        $('.add-producer').on('click', function(e){
        
           
        });
    
    });
");

$this->registerJs("
    $(function(){
    
            $('#producer_is_compen').on('click', function(e){
            var url = $(this).data('url');
            $('#check_producer_comp').modal('show').data('url', url);
            e.preventDefault();
        });
        
        $('.add-producer').on('click', function(e){
          
           
        });
    
    });
");

$this->registerJs("
    $(function(){
    
            $('#producer_is_return').on('click', function(e){
            var url = $(this).data('url');
            $('#check_producer_return').modal('show').data('url', url);
            e.preventDefault();
        });
        
        $('.add-producer').on('click', function(e){
        
           
        });
    
    });
");



    Modal::begin([
        'id' => 'check_producer',
        'header' => t('Выберите производителя'),
        'size' => Modal::SIZE_SMALL,

    ]);

        echo Nav::widget(['items' => $menuItems]);


    Modal::end();

    Modal::begin([
        'id' => 'check_producer_comp',
        'header' => t('Выберите производителя'),
        'size' => Modal::SIZE_SMALL,

    ]);

        echo Nav::widget(['items' => $menuItemsComp]);


    Modal::end();


Modal::begin([
    'id' => 'check_producer_return',
    'header' => t('Выберите производителя'),
    'size' => Modal::SIZE_SMALL,

]);

echo Nav::widget(['items' => $menuItemsReturn]);


Modal::end();

?>
