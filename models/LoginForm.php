<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    //public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            [['email'], 'email'],

            [['email'], 'match', 'pattern' => '/^[a-z0-9\-\_\.]+@[a-z0-9\-]+\.[a-z\.]*$/isu'],
            [['email'], 'exist', 'targetClass' => User::class, 'targetAttribute' => 'email'],
            [['password'], 'string', 'max' => 12, 'min' => 6],
            [['password'], 'match', 'pattern' => '/^[a-z0-9\-\.]*$/iu'],
            // rememberMe must be a boolean value
            // password is validated by validatePassword()
            ['password', 'validatePassword'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'password' => 'Пароль',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            } else {
                //var_dump($user->status); die;
                if( $user->status == 'moderate'){

                    $this->addError($attribute, 'Ваше заявление в стадии рассмотрения');

                }

                if( $user->status == 'inactive'){

                    $this->addError($attribute, 'вы отключены от сайта Сохен');

                }
            }

        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {

            return Yii::$app->user->login($this->getUser());
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->email);
        }

        return $this->_user;
    }
}
