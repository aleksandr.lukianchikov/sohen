<?php

namespace app\models;
use app\models\enums\LocaleEnum;

use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use creocoder\translateable\TranslateableBehavior;
/**
 * This is the model class for table "discount".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property integer $product_id
 * @property integer $min_qnt_product
 * @property integer $percent
 * @property integer $qnt_product_gift
 * @property integer $product_gift_id
 * @property integer $user_id
 * @property integer $sum
 * @property string $date_start
 * @property string $date_end
 * @property integer $producer_id
 * @property resource $image_1
 * @property Producer $producer
 * @property string $discription
 * @property Product product
 */
class Discount extends \yii\db\ActiveRecord
{
    /** @var  UploadedFile */
    public $image_file = [];
    public $discription_he;
    public $name_he;

    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['name'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
        ];
    }

    // Для транзакций, не обязательно.
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    // Настраиваем связь для модели переводов.
    public function getTranslations()
    {
        // Здесь прописываем переводимую таблица
        return $this->hasMany(DiscountTranslation::className(), ['discount_id' => 'id']);
    }

    public static function tableName()
    {
        return 'discount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['type','discription','discription_he'], 'string'],
            [['product_id', 'min_qnt_product', 'percent', 'qnt_product_gift', 'product_gift_id', 'user_id', 'sum'], 'integer'],
            [['date_start', 'date_end'], 'safe'],
            [['image_1'], 'string', 'max' => 255],
            [['name','name_he'], 'string', 'max' => 100],
            [['image_file'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => t('Название акции на русском'),
            'name_he' => t('Название акции на иврите'),
            'type' => t('Тип акции'),
            'product_id' => t('Продукт который нужно купить чтобы участвовать в акции'),
            'min_qnt_product' => t('Количество штук необходимое для участия'),
            'percent' => t('Процент'),
            'qnt_product_gift' => t('Количество продукта которое можно получить по акции'),
            'product_gift_id' => t('Продукт на который скидка'),
            'user_id' => t('Покупатель- обладатель персональной скидки'),
            'sum' => t('Сумма при покупке на которую есть скидка'),
            'date_start' => t('Дата начала'),
            'date_end' => t('Дата окончания'),
            'image_1' => t('Картинка'),
            'discription' => t('Описание акции на русском'),
            'discription_he' => t('Описание акции на иврите'),
            'producer_id' => t('Производитель'),
        ];
    }

    public function getProducer()
    {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }


    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getProductGift()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_gift_id']);
    }

    public function getProductName()
    {
        if("he-IL" !=  Yii::$app->language) {
            return (new \yii\db\Query())
                ->select('name')
                ->from('product')
                ->where(['id' => $this->product_gift_id])->scalar();
        }
        else{
            return (new \yii\db\Query())
                ->select('name')
                ->from('product_translation')
                ->where(['id' => $this->product_gift_id])->scalar();
        }
    }



    public function getProductConditionName()
    {
        if(LocaleEnum::isRTL()){
            return (new \yii\db\Query())
                ->select('name')
                ->from('product')
                ->where(['id' => $this->product_id])->scalar();
        }
        else{
            return (new \yii\db\Query())
                ->select('name')
                ->from('product_translation')
                ->where(['id' => $this->product_id])->scalar();
        }
    }

    public function setImages()
    {
        // Begin Это необходимо для того, чтобы обновлялись картинки
        // и удалялись из базы удаленные картинки
        if (empty($this->image_file)) {
            $this->image_file = [];
        }
        // Если это всего один файл (и больше нельзя), то надо обвернуть в массив.
        if (isset($this->image_file['path'])) {
            $this->image_file = [$this->image_file];
        }

        $countImages = count($this->image_file);
        if ($countImages) {
            foreach (array_values($this->image_file) as $i => $fileData) {
                $this->{'image_' . ($i + 1)} = $fileData['path'];
            }
        }
        for ($i = $countImages; $i < count($this->getImageFields()); $i++) {
            $this->{'image_' . ($i + 1)} = '';
        }
        // End
    }

    public function beforeSave($insert)
    {
        $this->setImages();

        if($this->date_start) {
            $this->date_start = (new \DateTime($this->date_start))->format('Y-m-d');
        }
        if($this->date_end) {
            $this->date_end = (new \DateTime($this->date_end))->format('Y-m-d');
        }
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $fields = $this->getImageFields();
        foreach ($fields as $image) {
            if (empty($this->{$image})) {
                continue;
            }
            $params = [
                'path' => $this->{$image},
                'base_url' => '/uploads',
                'delete_url' => Url::to(['product/delete-file', 'path' => $this->{$image}])
            ];
            if (count($fields) == 1) {
                $this->image_file = $params;
            }
            $this->image_file[] = $params;
        }
        $this->name_he = te($this, 'name', 'he-IL');
        parent::afterFind();
    }

    public function getImageFields()
    {
        return ['image_1'];
    }

    public function getProductList()
    {
        /** @var Producer $producer */
        $producer = Yii::$app->user->identity->producer;
        if (!$producer) {
            return [];
        }
        $productList = $producer->getProducts()->all();
        if(LocaleEnum::isRTL()) {
            return ArrayHelper::map($productList, 'id', 'name_he');
        }
        else{
            return ArrayHelper::map($productList, 'id', 'name');
        }
    }

    public function getUserList()
    {
        $userList = User::find()->where('producer_id is NULL')->all();
        return ArrayHelper::map($userList, 'id', 'shop_name');
    }



    // Сохраняем в таблице переводов заполненные значения по нужным полям перевода.
    public function afterSave($insert, $changedAttributes)
    {
        if (!empty($this->name_he)) {
            $tr = DiscountTranslation::findOne(['discount_id' => $this->id]);
            if (!$tr) {
                $tr = new DiscountTranslation();
                $tr->discount_id = $this->id;
            }
            $tr->name = $this->name_he;
            $tr->discription = $this->discription_he;
            $tr->language = 'he-IL';
            $tr->save();
        }

    }
}
