<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'producer_id'], 'integer'],
            [['order_date', 'orderstatus'], 'safe'],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();
        $query->addSelect(['*', '(IF(orderstatus <> "delivered", 0, 1)) AS order_sort']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        //var_dump($dataProvider->sort->getAttributeOrders(true)); die;
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(Yii::$app->user->can('customer')) {
            $userId = Yii::$app->user->identity->id;
            // grid filtering conditions
            $query->andFilterWhere([
                'user_id' => $userId,
                'producer_id' => $this->producer_id,
            ]);
        }
        else if(Yii::$app->user->can('producer_admin')) {
            $producerId = Yii::$app->user->identity->producer_id;
            // grid filtering conditions
            $query->andFilterWhere([
                'user_id' => $this->user_id,
                'producer_id' => $producerId,
            ]);
        }
        else if(Yii::$app->user->can('super_admin')) {
            // TODO
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'orderstatus' => $this->orderstatus,
            'sum' => $this->sum,
        ]);
        $query->andFilterWhere(['like', 'order_date', $this->order_date]);
        if (!Yii::$app->request->get('sort')) {
            $query->orderBy('order_sort ASC, order_date DESC');
        }
        return $dataProvider;
    }

    public function isActiveFilter()
    {
        $attributes = [
            'user_id',
            'orderstatus',
            'order_date',
            'sum',
            'producer_id',
        ];
        foreach ($attributes as $fieldName) {
            if (!empty($this->{$fieldName})) {
                return true;
            }
        }
        return false;
    }
}
