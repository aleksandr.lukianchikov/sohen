<?php

namespace app\models;

use Yii;
use yii\base\Model;


class PayForm extends Model
{
    public $supplier;
    public $sum;
    public $currency;
    public $ccno;
    public $expdate;
    public $myid;
    public $mycvv;
    public $cred_type;
    public $TranzilaPW;


/*"supplier" => "test233",
"sum" => "18",
"currency" => "1",
"ccno" => "12312312",
"expdate" => "0820",
"myid" => "12312312",
"mycvv" => "mycvv",
"cred_type" => "1",
"TranzilaPW" => "gagarin197",
    */


    public function rules()
    {
        return [
            [['sum', 'ccno', 'expdate', 'myid', 'mycvv'], 'required'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'supplier' => t('supplier'),
            'sum' => 'Сумма',
            'currency' => t('Валюта'),
            'ccno' => t('Номер карты'),
            'expdate' => t('Действительна до'),
            'myid' => t('Теудат Зеут'),
            'mycvv' => t('3 цифры на обратной стороне карты'),
            'cred_type' => t('Тип платежа'),
            'TranzilaPW' => t('TranzilaPW'),

        ];
    }

    public function sendRequestToGateway($id)
    {
        $params = Yii::$app->params['gateway_data'];
        $this->TranzilaPW = $params['TranzilaPW'];
        $this->cred_type = $params['cred_type'];
        $this->currency = $params['currency'];
        $this->supplier = $params['supplier'];

        $query_parameters = array(
            "supplier" => $this->supplier,
            "sum" => $this->sum,
            "currency" => $this->currency,
            "ccno" => $this->ccno,
            "expdate" => $this->expdate,
            "myid" => $this->myid,
            "mycvv" => $this->mycvv,
            "cred_type" => $this->cred_type,
            "TranzilaPW" => $this->TranzilaPW ,
        );

        $query_string = "";
        foreach ($query_parameters as $name => $value) {
            $query_string .= $name . "=" . $value . "&amp;";
        }

        $query_string = substr($query_string, 0, -1); // Remove trailing &#39;&amp;&#39;

        var_dump($query_string); die;
        // Initiate CURL
        $cr = curl_init();

        curl_setopt($cr, CURLOPT_URL, $params['tranzila_api_path']);
        curl_setopt($cr, CURLOPT_POST, 1);
        curl_setopt($cr, CURLOPT_FAILONERROR, true);
        curl_setopt($cr, CURLOPT_POSTFIELDS, $query_string);
        curl_setopt($cr, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cr, CURLOPT_SSL_VERIFYPEER, 0);

        // Execute request
        $result = curl_exec($cr);
        $error = curl_error($cr);

        if (!empty($error)) {
            die ($error);
        }
        curl_close($cr);

        // Preparing associative array with response data
        $response_array = explode("&", $result);
        $response_assoc = array();

        if (count($response_array) > 1) {
            foreach ($response_array as $value) {
                $tmp = explode("=", $value);
                if (count($tmp) > 1) {
                    $response_assoc[$tmp[0]] = $tmp[1];
                }
            }
        }

        // Analyze the result string<br />
        if (!isset($response_assoc["Response"])) {
            die ($response_assoc . "\n");
            /*
      &nbsp;&nbsp; &nbsp; * When there is no &#39;Response&#39; parameter it either means<br />
      &nbsp;&nbsp; &nbsp; * that some pre-transaction error happened (like authentication<br />
      &nbsp;&nbsp; &nbsp; * problems), in which case the result string will be in HTML format,<br />
      &nbsp;&nbsp; &nbsp; * explaining the error, or the request was made for generate token only<br />
      &nbsp;&nbsp; &nbsp; * (in this case the response string will contain only &#39;TranzilaTK&#39;<br />
      &nbsp;&nbsp; &nbsp; * parameter)<br />
      &nbsp;&nbsp; &nbsp; */
        } else if ($response_assoc["Response"] !== "000") {
            die ($response_assoc["Response"]. "\n");
            // Any other than &#39;000&#39; code means transaction failure
            // (bad card, expiry, etc..)
        } else {
            die ("Success\n");
        }
    }
}
