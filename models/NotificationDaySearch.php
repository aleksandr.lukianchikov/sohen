<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NotificationDay;

/**
 * DaySearch represents the model behind the search form about `app\models\Day`.
 */
class NotificationDaySearch extends NotificationDay
{
    public $city;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'producer_id'], 'integer'],
            [['reminder_day', 'order_day', 'city'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NotificationDay::find();

        $query->joinWith('user');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['city'] = [
            'asc' => ['user.city' => SORT_ASC],
            'desc' => ['user.city' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'notification_day.producer_id' => $this->producer_id,
        ]);

        $query->andFilterWhere(['like', 'reminder_day', $this->reminder_day])
            ->andFilterWhere(['like', 'order_day', $this->order_day])
            ->andFilterWhere(['like', 'user.city', $this->city]);

        return $dataProvider;
    }

    public function isActiveFilter()
    {
        $attributes = [
            'user_id',
            'city',
            'reminder_day',
            'order_day',

        ];
        foreach ($attributes as $fieldName) {
            if (!empty($this->{$fieldName})) {
                return true;
            }
        }
        return false;
    }
}
