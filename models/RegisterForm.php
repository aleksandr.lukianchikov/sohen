<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class RegisterForm extends Model
{
    public $shop_name;
    public $email;
    public $password;
    public $password_confirm;
    public $tel;
    public $city;
    public $street;
    public $number_house;
    public $language;
    public $producer_id;


    public function rules()
    {
        return [
            [['shop_name', 'email', 'password', 'password_confirm', 'tel', 'city', 'street', 'number_house'], 'required'],
            [['shop_name'], 'string', 'max' => 50],
            [['email'], 'email'],
            [['email'], 'match', 'pattern' => '/^[a-z0-9\-\_\.]+@[a-z0-9\-]+\.[a-z\.]*$/isu'],
            [['email'], 'isUniqueEmail'],
            [['password'], 'string', 'min' => 6, 'max' => 12],
            [['password'], 'match', 'pattern' => '/^[a-z0-9\-\.]*$/iu'],
            [['password_confirm'], 'compare', 'compareAttribute' => 'password'],
            //[['password_confirm'], 'string', должен быть равен password],
            [['tel'], 'string', 'max' => 17],
           // [['tel'], 'match', 'pattern' => '/[0-9\-\]*$/s'],//не уверена
            [['city'], 'string', 'max' => 50],
            //[['city'], 'match', 'pattern' => '/^[a-zа-яё\s-.]{1}[a-zа-яё\s-.]*[a-zа-яё]{1}$/iu'],
            [['street'], 'string', 'max' => 50],
            //[['street'], 'match', 'pattern' => '/^[a-zа-яё\s-.]{1}[a-zа-яё\s-.]*[a-zа-яё\s]{1}$/iu'],
            [['number_house'], 'string', 'max' => 8],
            //[['number_house'], 'match', 'pattern' => '/^[\d][\d\s-\\][\w]/'],
            [['language','producer_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'shop_name' => t('Название магазина'),
            'email' => 'Email',
            'password' => t('Пароль'),
            'password_confirm' => t('Подтверждение пароля'),
            'tel' => t('Номер телефона'),
            'city' => t('Город'),
            'street' => t('Улица'),
            'number_house' => t('Номер дома'),
            'language' => t('Язык коммуникации'),

        ];
    }

    public function isUniqueEmail()
    {
        $email = $this->email;
        $user = User::findOne(['email' => $email]);
        if ($user && (Yii::$app->user->isGuest || Yii::$app->user->identity->email != $email)) {
            $this->addError('email', 'Пользователь с такой почтой уже существует.');
            return false;
        }
        return true;
    }
}
