<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "billing_return".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $producer_id
 * @property double $amount
 */
class BillingReturn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing_return';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'producer_id'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'producer_id' => 'Producer ID',
            'amount' => 'Amount',
        ];
    }
}
