<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "discount_to_order".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $discount_id
 * @property double $sum
 */
class OrderDiscount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_discount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'discount_id'], 'integer'],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Номер заказа',
            'discount_id' => 'Акция',
            'sum' => 'Сумма',
        ];
    }

    public function getDiscount()
    {
        return $this->hasOne(Discount::className(), ['id' => 'discount_id']);
    }

}
