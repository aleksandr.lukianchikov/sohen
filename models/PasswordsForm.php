<?php

namespace app\models;

use Yii;
use yii\base\Model;

class PasswordsForm extends Model
{
    public $password;
    public $password_confirm;

    public function rules()
    {
        return [
            [[ 'password','password_confirm'], 'required'],
            ['password_confirm', 'compare','compareAttribute' => 'password'],
        ];
    }
    public function attributeLabels()
    {
        return [

            'password' => 'Пароль',
            'password_confirm' => 'Подтвердите пароль',
        ];
    }
}