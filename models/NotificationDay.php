<?php

namespace app\models;
use app\models\enums\City;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "day".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $producer_id
 * @property string $reminder_day
 * @property string $order_day
 */
class NotificationDay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_day';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'producer_id'], 'integer'],
            [['reminder_day', 'order_day'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => t('Покупатель'),
            'producer_id' => t('Производитель'),
            'reminder_day' => t('День накануне'),
            'order_day' => t('День заказа'),
            'city' => t('Город'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserList()
    {

        $users = User::find()->where(['access_rights' => '["customer"]'])->all();
        return ArrayHelper::map($users, 'id', 'shop_name');
    }

    public function getProducer()
    {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }
    public function getCity(){
        $usermodel = $this->getUser()->one();

        $str = City::getClientValue($usermodel->city);
        return $str;

    }



}
