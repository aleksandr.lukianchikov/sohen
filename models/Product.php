<?php

namespace app\models;
use creocoder\translateable\TranslateableBehavior;
use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use app\models\enums\LocaleEnum;
use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property integer $product_type_id
 * @property string $discription
 * @property string $composition
 * @property double $price
 * @property double $pack_gram
 * @property double $calories
 * @property integer $shelf_life
 * @property integer $storage_conditions_id
 * @property integer $kosher
 * @property integer $available
 * @property integer $status_new
 * @property integer $producer_id
 * @property resource $image_1
 * @property resource $image_2
 * @property resource $image_3
 * @property Producer $producer
 * @property ProductType $productType
 * @property integer $pack_type_id
 * @property StorageConditions $storageConditions
 * @property PackType packType
 * @property string barcode
 * @property integer internal_code
 */
class Product extends \yii\db\ActiveRecord
{
    public $image_file = [];
    public $name_he;
    public $discription_he;
    public $composition_he;
    /**
     * @inheritdoc
     */

    // Добавляем поведение из стороннего расширения из composer установленное
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['name','discription','composition'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
        ];
    }

    // Для транзакций, не обязательно.
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    // Настраиваем связь для модели переводов.
    public function getTranslations()
    {
        // Здесь прописываем переводимую таблица
        return $this->hasMany(ProductTranslation::className(), ['product_id' => 'id']);
    }

    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'product_type_id', 'price', 'pack_gram', 'calories',
                'shelf_life', 'storage_conditions_id', 'kosher', 'available', 'pack_type_id'], 'required'],
            [['product_type_id', 'storage_conditions_id', 'kosher', 'available', 'producer_id','pack_type_id','status_new' ], 'integer'],
            [['price', 'pack_gram', 'calories'], 'number'],
            [['shelf_life'], 'number'],
            [['image_1', 'image_2', 'image_3'], 'string', 'max' => 128],
            [['name', 'name_he'], 'string', 'max' => 100],
            [['discription','discription_he', 'composition','composition_he'], 'string', 'max' => 1000],
            [['image_file'], 'safe'],
            [['barcode'], 'string' , 'max' => 13,'tooLong'=>'Должно быть 13 символов'],
            [['barcode'], 'string' , 'min' => 13,'tooShort'=>'Должно быть 13 символов'],
            [['internal_code'], 'string' , 'max' => 3,'tooLong'=>'Должно быть 3 символа'],
            [['internal_code'], 'string' , 'min' => 3,'tooShort'=>'Должно быть 3 символа'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => t('Название продукта на русском'),
            'name_he' => t('Название  продукта на иврите'),
            'barcode' => t('Баркод'),
            'internal_code' => t('Внутренний код'),
            'product_type_id' => t('Тип продукта'),
            'discription' => t('Описание на русском'),
            'discription_he' => t('Описание продукта на иврите'),
            'composition' => t('Состав на русском'),
            'composition_he' => t('Состав продукта на иврите'),
            'price' => t('Цена'),
            'pack_gram' => t('Грамм в упаковке'),
            'calories' => t('Калорийность'),
            'shelf_life' => t('Срок хранения'),
            'storage_conditions_id' => t('Условия хранения'),
            'kosher' => t('Кошерность'),
            'available' => t('В наличии'),
            'status_new' => t('Новинка'),
            'producer_id' => t('Производитель'),
            'image_1' => t('Картинка 1'),
            'image_2' => t('Картинка 2'),
            'image_3' => t('Картинка 3'),
            'pack_type_id' => t('Тип упаковки'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducer()
    {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductType()
    {
        return $this->hasOne(ProductType::className(), ['id' => 'product_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackType()
    {
        return $this->hasOne(PackType::className(), ['id' => 'pack_type_id']);
    }

    public function getStorageConditions()
    {
        return $this->hasOne(StorageConditions::className(),['id' => 'storage_conditions_id']);
    }

/*
    public function getStorageConditionsName()
    {
        return (new \yii\db\Query())
            ->select('name')
           ->from('storage_conditions')
            ->where(['id' => $this->storage_conditions_id])->scalar();
    }

*/
    public function setImages()
    {
        // Begin Это необходимо для того, чтобы обновлялись картинки
        // и удалялись из базы удаленные картинки
        if (empty($this->image_file)) {
            $this->image_file = [];
        }
        // Если это всего один файл (и больше нельзя), то надо обвернуть в массив.
        if (isset($this->image_file['path'])) {
            $this->image_file = [$this->image_file];
        }

        $countImages = count($this->image_file);
        if ($countImages) {
            foreach (array_values($this->image_file) as $i => $fileData) {
                $this->{'image_' . ($i + 1)} = $fileData['path'];

                $imgFilePath = Yii::getAlias('@uploads/').$fileData['path'];
                $imagine = Image::getImagine();
                $imagine = $imagine->open($imgFilePath);
                //$sizes = getimagesize ($imgFilePath);
                /*
                   [0] => 604
                   [1] => 244
                   [2] => 3
                   [3] => width="604" height="244"
                   [bits] => 8
                   [mime] => image/png
                ) */
                $width = 700;
                $height = 700;
                $imagine = $imagine->resize(new Box($width, $height))->save($imgFilePath, ['quality' => 70]);             }
        }
        for ($i = $countImages; $i < count($this->getImageFields()); $i++) {
            $this->{'image_' . ($i + 1)} = '';
        }
        // End
    }

    public function beforeSave($insert)
    {
        $this->setImages();
        return parent::beforeSave($insert);
    }

    public function getImageFields()
    {
        return ['image_1', 'image_2', 'image_3'];
    }

    public function getProductTypeList()
    {
        /** @var Producer $producer */
        $producer = Yii::$app->user->identity->producer;
        if (!$producer) {
            return [];
        }
        $items = ProductType::getChildren($producer->id, 0);
        return ProductType::createNestedMargin($items, '  ');
    }

    public function getStorageConditionsList()
    {
        $storageConditions = StorageConditions::find()->all();
        if(LocaleEnum::isRTL()) {
            return ArrayHelper::map($storageConditions, 'id', 'name_he');
        }
        else{
            return ArrayHelper::map($storageConditions, 'id', 'name');
        }
    }



    public function getPackTypesList()
    {
        $packtypes = PackType::find()->all();
        if(LocaleEnum::isRTL()) {
            return ArrayHelper::map($packtypes, 'id', 'name_he');
        }
        else{
            return ArrayHelper::map($packtypes, 'id', 'name');
        }
    }

    public function hasDiscount()
    {
        $res = 0;
        $visitdate = new \DateTime('now', new \DateTimeZone('UTC'));
        $Result =  Discount::find()->where(['discount.producer_id' => $this->producer_id,'discount.product_id' => $this->id,'discount.type' => 'prod_assoc'])
            ->andwhere(['>=', 'date_end', $visitdate->format('Y-m-d H:i:s')])
            ->andwhere(['<=', 'date_start', $visitdate->format('Y-m-d H:i:s')])->one();

        if($Result){
           if($Result->product_gift_id == $Result->product_id){
               $res = 2;
           }
           else $res = 3;
        }

        return $res;

    }
    public function isNew(){
        $result = $this->status_new;
        return $result;
    }

    public function afterFind()
    {
        $fields = $this->getImageFields();
        foreach ($fields as $image) {
            if (empty($this->{$image})) {
                continue;
            }
            $params = [
                'path' => $this->{$image},
                'base_url' => '/uploads',
                'delete_url' => Url::to(['product/delete-file', 'path' => $this->{$image}])
            ];
            if (count($fields) == 1) {
                $this->image_file = $params;
            }
            $this->image_file[] = $params;
        }
        // Для того чтобы при инициализации модели поля переводов (name_he) проставлялись из другой таблицы-переводов

        $this->name_he = te($this, 'name', 'he-IL');
        $this->discription_he = te($this, 'discription', 'he-IL');
        $this->composition_he = te($this, 'composition', 'he-IL');
        parent::afterFind();
    }




    // Сохраняем в таблице переводов заполненные значения по нужным полям перевода.
    public function afterSave($insert, $changedAttributes)
    {
        if (!empty($this->name_he)) {
            $tr = ProductTranslation::findOne(['product_id' => $this->id]);
            if (!$tr) {
                $tr = new ProductTranslation();
                $tr->product_id = $this->id;
            }


            $tr->name = $this->name_he;
            $tr->discription = $this->discription_he;
            $tr->composition = $this->composition_he;
            $tr->language = 'he-IL';
            $tr->save();
        }

    }
}