<?php

namespace app\models;

use phpDocumentor\Reflection\Types\Null_;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Discount;

/**
 * DiscountSearch represents the model behind the search form about `app\models\Discount`.
 */
class DiscountSearch extends Discount
{
    public $actual;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'min_qnt_product', 'percent', 'qnt_product_gift', 'product_gift_id', 'user_id', 'sum', 'producer_id'], 'integer'],
            [['name', 'type', 'discription', 'date_start', 'date_end', 'image_1', 'actual'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Discount::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $producerId = Yii::$app->user->identity->producer_id;
        if($producerId == NULL) {
            $producerId = Yii::$app->request->get('producer_id');
            $query->andFilterWhere(['type' => 'user_assoc', 'user_id' => Yii::$app->user->identity->id])
                    ->orFilterWhere(['type' => ['sum_assoc','prod_assoc','feast_assoc']]);
        }

        if($this->actual){
            $visitdate = new \DateTime('now', new \DateTimeZone('UTC'));
            $query->where(['>=', 'date_end', $visitdate->format('Y-m-d H:i:s')])
                ->andwhere(['<=', 'date_start', $visitdate->format('Y-m-d H:i:s')]);

        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            'min_qnt_product' => $this->min_qnt_product,
            'percent' => $this->percent,
            'qnt_product_gift' => $this->qnt_product_gift,
            'product_gift_id' => $this->product_gift_id,
            'user_id' => $this->user_id,
            'sum' => $this->sum,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'producer_id' => $producerId,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'image_1', $this->image_1]);

        return $dataProvider;
    }

    public function isActiveFilter()
    {
        $attributes = [
            'user_id',
            'product_id',
            'product_gift_id',
            'date_start',
            'date_end',
            'type',
            'name',

        ];
        foreach ($attributes as $fieldName) {
            if (!empty($this->{$fieldName})) {
                return true;
            }
        }
        return false;
    }
}
