<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notification_text_translation".
 *
 * @property integer $id
 * @property integer $notification_text_id
 * @property string $language
 * @property string $text_1
 * @property string $text_2
 */
class NotificationTextTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_text_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_text_id'], 'integer'],
            [['language'], 'required'],
            [['text_1', 'text_2'], 'string'],
            [['language'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notification_text_id' => 'Notification Text ID',
            'language' => 'Language',
            'text_1' => 'Text 1',
            'text_2' => 'Text 2',
        ];
    }
}
