<?php
/**
 * Created by PhpStorm.
 * User: Bramnik
 * Date: 7/23/2017
 * Time: 9:50 PM
 */

namespace app\models\enums;
use app\models\enums\LocaleEnum;

class Available
{
    const YES = 1;
    const NO = 0;

    /*public static function getValues()
    {
        return [
            self::YES,
            self::NO,
        ];
    }*/

    public static function getClientValues()
    {
       /*return [
            self::YES => 'Да',
            self::NO => 'Нет',
        ];*/
        if(LocaleEnum::isRTL()) {
            return [
                self::YES => 'כן',
                self::NO => 'לא',
            ];
        }
        else {
            return [
                self::YES => 'Да',
                self::NO => 'Нет',
            ];
        }
    }

    public static function getClientValue($key)
    {
        $values = self::getClientValues();
        if (isset($values[$key])) {
            return $values[$key];
        }
        return null;
    }

}