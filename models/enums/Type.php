<?php
/**
 * Created by PhpStorm.
 * User: Bramnik
 * Date: 7/23/2017
 * Time: 8:11 PM
 */

namespace app\models\enums;


class Type
{
    const user_assoc = 'user_assoc';
    const prod_assoc = 'prod_assoc';
    const sum_assoc  = 'sum_assoc';
    const feast_assoc = 'feast_assoc';
    const only_picture = 'only_picture';


    public static function getClientValues()
    {
        if(LocaleEnum::isRTL()) {
            return [
                self::user_assoc => 'לפי הלקוח',
                self::prod_assoc => 'לפי המוצר',
                self::sum_assoc => 'לפי הסכום',
                self::feast_assoc => 'לפי החופשה',
                self::only_picture => 'רק תמונה',

            ];
        }
        else {
            return [
                self::user_assoc => 'По покупателю',
                self::prod_assoc => 'По продукту',
                self::sum_assoc => 'По сумме заказа',
                self::feast_assoc => 'Праздничная акция',
                self::only_picture => 'Только картинка',
            ];
        }
    }

    public static function getClientValue($key)
    {
        $values = self::getClientValues();
        if (isset($values[$key])) {
            return $values[$key];
        }
        return null;
    }
}