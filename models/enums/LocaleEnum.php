<?php
namespace app\models\enums;

use \Yii;

class LocaleEnum {
    const HEBREW = 'he-IL';

    public static function isRTL()
    {
        return in_array(Yii::$app->language, [
            self::HEBREW,
        ]);
    }
}
