<?php
/**
 * Created by PhpStorm.
 * User: Bramnik
 * Date: 7/23/2017
 * Time: 9:50 PM
 */

namespace app\models\enums;


class OrderStatus
{
    const created = 'created';
    const send = 'send';
    const in_the_way = 'in_the_way';
    const delivered = 'delivered';
    const paid = 'paid';

    /*public static function getValues()
    {
        return [
            self::YES,
            self::NO,
        ];
    }*/

    public static function getClientValues()
    {
        /*return [
             self::YES => 'Да',
             self::NO => 'Нет',
         ];*/
        if(LocaleEnum::isRTL()) {
            return [
                self::created => 'נוצרה',
                self::send => 'נשלח',
                self::in_the_way => 'בדרך',
                self::delivered => 'נמסר',
                self::paid => 'שילם',
            ];
        }
        else {
            return [
                self::created => 'Создан',
                self::send => 'Отправлен',
                self::in_the_way => 'В пути',
                self::delivered => 'доставлен',
                self::paid => 'оплачен',
            ];
        }
    }

    public static function getClientValue($key)
    {
        $values = self::getClientValues();
        if (isset($values[$key])) {
            return $values[$key];
        }
        return null;
    }

}