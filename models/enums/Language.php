<?php
/**
 * Created by PhpStorm.
 * User: Bramnik
 * Date: 2/4/2018
 * Time: 7:09 PM
 */

namespace app\models\enums;


class Language
{
    const Russian = 0;
    const Hebrew = 1;
    const English = 2;

    /*public static function getValues()
    {
        return [
            self::YES,
            self::NO,
        ];
    }*/

    public static function getSystemLanguage($key)
    {
        $items = [
            self::Hebrew => 'he-IL',
            self::Russian => 'ru-RU',
            self::English => 'en-US',
        ];
        return isset($items[$key]) ? $items[$key] : $items[self::Russian];
    }

    public static function getClientValues()
    {

        if(LocaleEnum::isRTL()) {
            return [
                self::Hebrew => 'עברית',
                self::Russian => 'רוסית',
            ];
        }
        else {
            return [
                self::Hebrew => 'Иврит',
                self::Russian => 'Русский',
            ];
        }
    }

    public static function getClientValue($key)
    {
        $values = self::getClientValues();
        if (isset($values[$key])) {
            return $values[$key];
        }
        return null;
    }

}