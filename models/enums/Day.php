<?php
/**
 * Created by PhpStorm.
 * User: Bramnik
 * Date: 9/13/2017
 * Time: 12:50 PM
 */

namespace app\models\enums;


class Day
{
    const Monday = 'Monday';
    const Tuesday = 'Tuesday';
    const Wednesday  = 'Wednesday';
    const Thursday = 'Thursday';
    const Friday = 'Friday';
    const Saturday	= 'Saturday';
    const Sunday = 'Sunday';


    public static function getClientValues()
    {
        if(LocaleEnum::isRTL()) {
            return [
                self::Monday => 'יום שני',
                self::Tuesday => 'יום שלישי',
                self::Wednesday => 'יום רביעי',
                self::Thursday => 'יום חמישי',
                self::Friday => 'יום שישי',
                self::Saturday => 'יום שבת',
                self::Sunday => 'יום ראשון'


            ];
        }
        else {
            return [
                self::Monday => 'Понедельник',
                self::Tuesday => 'Вторник',
                self::Wednesday => 'Среда',
                self::Thursday => 'Четверг',
                self::Friday => 'Пятница',
                self::Saturday => 'Суббота',
                self::Sunday => 'Воскресенье'
            ];
        }
    }

    public static function getClientValue($key)
    {
        $values = self::getClientValues();
        if (isset($values[$key])) {
            return $values[$key];
        }
        return null;
    }
}
