<?php
/**
 * Created by PhpStorm.
 * User: Bramnik
 * Date: 10/9/2017
 * Time: 2:48 PM
 */

namespace app\models\enums;


class Status

{
    const moderate = 'moderate';
    const active = 'active';
    const inactive  = 'inactive';



    public static function getClientValues()
    {
        if(LocaleEnum::isRTL()) {
            return [
                self::moderate => 'תוך התחשבות',
                self::active => 'פעיל',
                self::inactive => 'מושבת',


            ];
        }
        else {
            return [
                self::moderate => 'в рассмотрении',
                self::active => 'активен',
                self::inactive => 'дезактивирован',
            ];
        }
    }

    public static function getClientValue($key)
    {
        $values = self::getClientValues();
        if (isset($values[$key])) {
            return $values[$key];
        }
        return null;
    }
}