<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sms_notification', 'email_notification', 'producer_id','language'], 'integer'],
            [['shop_name', 'email', 'password', 'tel', 'delivery_address', 'city', 'access_rights', 'status','street', 'number_house'], 'safe'],
            [['money_box'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'money_box' => $this->money_box,
            'sms_notification' => $this->sms_notification,
            'email_notification' => $this->email_notification,
            'producer_id' => $this->producer_id,
            'status' => $this->status,
            'language' => $this->language,
        ]);

        $query->andFilterWhere(['like', 'shop_name', $this->shop_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'tel', $this->tel])
            ->andFilterWhere(['like', 'delivery_address', $this->delivery_address])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'access_rights', $this->access_rights])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'number_house', $this->number_house]);

        return $dataProvider;
    }
}
