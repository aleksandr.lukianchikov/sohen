<?php

namespace app\models\customer;

use app\models\ProductType;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;
use yii\helpers\ArrayHelper;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_type_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6,
            ],
        ]);
        $this->load($params, '');

        if ($this->product_type_id) {
            $types = [$this->product_type_id];
            $childrenTypes = ProductType::getChildren($this->producer_id, $this->product_type_id, true);
            $types = ArrayHelper::merge($types, $childrenTypes);
            $query->andWhere(['in', 'product_type_id', $types]);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'pack_gram' => $this->pack_gram,
            'calories' => $this->calories,
            'storage_conditions_id' => $this->storage_conditions_id,
            'kosher' => $this->kosher,
            'available' => $this->available,
            'status_new' => $this->status_new,
            'producer_id' => $this->producer_id,
            'shelf_life' => $this->shelf_life,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'discription', $this->discription])
            ->andFilterWhere(['like', 'composition', $this->composition])
            ->andFilterWhere(['like', 'image_1', $this->image_1])
            ->andFilterWhere(['like', 'image_2', $this->image_2])
            ->andFilterWhere(['like', 'image_3', $this->image_3]);

        return $dataProvider;
    }
}
