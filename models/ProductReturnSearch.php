<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductReturn;

/**
 * ProductReturnSearch represents the model behind the search form about `app\models\ProductReturn`.
 */
class ProductReturnSearch extends ProductReturn
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'product_id', 'reason_id', 'producer_id', 'number'], 'integer'],
            [['image_1', 'image_2', 'image_3'], 'safe'],
            [['statement_date', 'approved_date'], 'safe'],
            [['is_approved', 'completed'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductReturn::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        //$UserAccsess = Yii::$app->user->identity->access_rights;
        //var_dump($UserAccsess);die;
        if(Yii::$app->user->can('customer')) {
            $userId = Yii::$app->user->identity->id;
            // grid filtering conditions
            $query->andFilterWhere([
                'id' => $this->id,
                'user_id' => $userId,
               // 'product_id' => $this->product_id,
                'reason_id' => $this->reason_id,
                'producer_id' => $this->producer_id,
                'number' => $this->number,
                'is_approved' => $this->is_approved,
                'completed' => $this->completed,
                //'statement_date' => $this->statement_date,
            ]);
        }
        else if(Yii::$app->user->can('producer_admin')) {
            $producerId= Yii::$app->user->identity->producer_id;
            // grid filtering conditions
            $query->andFilterWhere([
                'id' => $this->id,
                'user_id' => $this->user_id,
                //'product_id' => $this->product_id,
                'reason_id' => $this->reason_id,
                'producer_id' => $producerId,
                'number' => $this->number,
                'is_approved' =>$this->is_approved,
                'completed' =>$this->completed,
                //'statement_date' => $this->statement_date,
            ]);        }
        else if(Yii::$app->user->can('super_admin')) {
            // TBD
        }

        $query->andFilterWhere(['like', 'product_id', $this->product_id])

            ->andFilterWhere(['like', 'statement_date', $this->statement_date]);

        return $dataProvider;
    }

    public function isActiveFilter()
    {
        $attributes = [
            'user_id',
            'product_id',
            'reason_id',
            'is_approved',
            'completed',
            'statement_date',


        ];
        foreach ($attributes as $fieldName) {
            if (!empty($this->{$fieldName})) {
                return true;
            }
        }
        return false;
    }

}
