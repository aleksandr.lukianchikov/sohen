<?php

namespace app\models;
use app\models\enums\Language;
use creocoder\translateable\TranslateableBehavior;
use Yii;

/**
 * This is the model class for table "notification-text".
 *
 * @property integer $id
 * @property integer $producer_id
 * @property string $text_1
 * @property string $text_2
 */
class NotificationText extends \yii\db\ActiveRecord
{
    public $text_1_he;
    public $text_2_he;

    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['text_1',],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
        ];
    }

    // Для транзакций, не обязательно.
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    // Настраиваем связь для модели переводов.
    public function getTranslations()
    {
        // Здесь прописываем переводимую таблица
        return $this->hasMany(NotificationTextTranslation::className(), ['notification_text_id' => 'id']);
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_text';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['producer_id'], 'integer'],
            [['text_1', 'text_2','text_1_he','text_2_he'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'producer_id' => t('Производитель'),
            'text_1' => t('Текст уведомления 1 на русском'),
            'text_2' => t('Текст уведомления 2 на русском'),
            'text_1_he' =>t('Текст уведомления 1 на иврите'),
            'text_2_he' => t('Текст уведомления 2 на иврите'),
        ];
    }

    public function getProducer()
    {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }


    // Для того чтобы при инициализации модели поля переводов (name_he) проставлялись из другой таблицы-переводов
    public function afterFind()
    {
        $this->text_1_he = te($this, 'text_1', Language::Hebrew);
        $this->text_2_he = te($this, 'text_2', Language::Hebrew);
        parent::afterFind();
    }

    // Сохраняем в таблице переводов заполненные значения по нужным полям перевода.
    public function afterSave($insert, $changedAttributes)
    {
        if (!empty($this->text_1_he)) {
            $tr = NotificationTextTranslation::findOne(['notification_text_id' => $this->id]);
            if (!$tr) {
                $tr = new NotificationTextTranslation();
                $tr->notification_text_id = $this->id;
            }
            $tr->text_1 = $this->text_1_he;
            $tr->text_2 = $this->text_2_he;
            $tr->language = Language::Hebrew;
            $tr->save();
        }

    }
}
