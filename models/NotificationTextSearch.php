<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NotificationText;

/**
 * NotificationTextSearch represents the model behind the search form about `app\models\NotificationText`.
 */
class NotificationTextSearch extends NotificationText
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'producer_id'], 'integer'],
            [['text_1', 'text_2'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NotificationText::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'producer_id' => $this->producer_id,
        ]);

        $query->andFilterWhere(['like', 'text_1', $this->text_1])
            ->andFilterWhere(['like', 'text_2', $this->text_2]);

        return $dataProvider;
    }
}
