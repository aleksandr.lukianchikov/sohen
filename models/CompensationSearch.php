<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Compensation;

/**
 * CompensationSearch represents the model behind the search form about `app\models\Compensation`.
 */
class CompensationSearch extends Compensation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'producer_id', 'product_id', 'amount', 'percentReturn', 'sum', 'is_approved', 'completed'], 'integer'],
            [['approved_date', 'statement_date','end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Compensation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $UserAccsess = Yii::$app->user->identity->access_rights;
        //var_dump($UserAccsess);die;
        if($UserAccsess == "[\"customer\"]") {
            $userId = Yii::$app->user->identity->id;
            // grid filtering conditions
            $query->andFilterWhere([
                'id' => $this->id,
                'user_id' => $userId,
                'producer_id' => $this->producer_id,
                'product_id' => $this->product_id,
                'amount' => $this->amount,
                'percentReturn' => $this->percentReturn,
                'sum' => $this->sum,
                'is_approved' => $this->is_approved,
                //'statement_date' => $this->statement_date,
                'approved_date' => $this->approved_date,
                'completed' => $this->completed,
                //'end_date' => $this->end_date,
            ]);
        }

        else if($UserAccsess == "[\"producer_admin\"]"){
            $producerId= Yii::$app->user->identity->producer_id;
            // grid filtering conditions
            $query->andFilterWhere([
                'id' => $this->id,
                'user_id' => $this->user_id,
                'producer_id' => $producerId,
                'product_id' => $this->product_id,
                'amount' => $this->amount,
                'percentReturn' => $this->percentReturn,
                'sum' => $this->sum,
                'is_approved' => $this->is_approved,
                //'statement_date' => $this->statement_date,
                'approved_date' => $this->approved_date,
                'completed' => $this->completed,
               // 'end_date' => $this->end_date,
            ]);        }
        else if($UserAccsess == "[\"super_admin\"]") {
            // TBD
        }

        $query->andFilterWhere(['like', 'statement_date', $this->statement_date])
            ->andFilterWhere(['like', 'product_id', $this->product_id])
            ->andFilterWhere(['like', 'end_date', $this->end_date]);


        return $dataProvider;
    }

    public function isActiveFilter()
    {
        $attributes = [
            'user_id',
            'product_id',
            'statement_date',
            'is_approved',
            'completed',
            'end_date',
            'percentReturn',
            'producer_id',


        ];
        foreach ($attributes as $fieldName) {
            if (!empty($this->{$fieldName})) {
                return true;
            }
        }
        return false;
    }
}
