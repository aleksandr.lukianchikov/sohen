<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_translation".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $language
 * @property string $name
 * @property string $discription
 * @property string $composition
 */
class ProductTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['language', 'name'], 'required'],
            [['language'], 'string', 'max' => 6],
            [['name'], 'string', 'max' => 255],
            [['discription', 'composition'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'language' => 'Language',
            'name' => 'Name',
            'discription' => 'Description',
            'composition' => 'Composition',
        ];
    }
}
