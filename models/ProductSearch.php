<?php

namespace app\models;

use app\models\ProductType;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;
use yii\helpers\ArrayHelper;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'storage_conditions_id', 'kosher', 'available', 'status_new', 'producer_id', 'shelf_life', 'product_type_id'], 'integer'],
            [['name', 'discription', 'composition', 'barcode','internal_code','image_1', 'image_2', 'image_3'], 'safe'],
            [['price', 'pack_gram', 'calories'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $this->load($params);

        $producerId = Yii::$app->user->identity->producer_id;

        if ($this->product_type_id) {
            $types = [$this->product_type_id];
            $childrenTypes = ProductType::getChildren($producerId, $this->product_type_id, true);
            $types = ArrayHelper::merge($types, $childrenTypes);
            $query->andWhere(['in', 'product_type_id', $types]);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'pack_gram' => $this->pack_gram,
            'calories' => $this->calories,
            'storage_conditions_id' => $this->storage_conditions_id,
            'kosher' => $this->kosher,
            'available' => $this->available,
            'status_new' => $this->status_new,
            'producer_id' => $producerId,
            'shelf_life' => $this->shelf_life,
            //'product_type_id' => $this->product_type_id,
            'barcode' => $this->barcode,
            'internal_code' => $this->internal_code,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);



        return $dataProvider;
    }

    public function isActiveFilter()
    {
        $attributes = [
            'name',
            'price',
            'available',
            'kosher',
            'date_start',
            'status_new',
            'prouct_type',
            'barcode',
            'internal_code',

        ];
        foreach ($attributes as $fieldName) {
            if (!empty($this->{$fieldName})) {
                return true;
            }
        }
        return false;
    }

}
