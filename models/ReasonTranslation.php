<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reason_translation".
 *
 * @property integer $id
 * @property integer $reason_id
 * @property string $language
 * @property string $name
 */
class ReasonTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reason_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reason_id'], 'integer'],
            [['language', 'name'], 'required'],
            [['language'], 'string', 'max' => 6],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reason_id' => 'Reason ID',
            'language' => 'Language',
            'name' => 'Name',
        ];
    }


}
