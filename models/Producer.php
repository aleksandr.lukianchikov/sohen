<?php

namespace app\models;

use Yii;
use creocoder\translateable\TranslateableBehavior;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "producer".
 *
 * @property integer $id
 * @property string $name
 * @property string $tel
 * @property string $address
 * @property string $site
 * @property string $account
 * @property string $email
 * @property string $slogan
 * @property boolean $compensation
 * @property resource $image_1
 * @property string about
 * @property ProductType $productTypes
 * @property User $user
 */
class Producer extends \yii\db\ActiveRecord
{
    /** @var  UploadedFile */
    public $image_file = [];

    /**
     * @inheritdoc
     */
    public $name_he;
    public $address_he;
    public $slogan_he;
    public $about_he;

    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['name','address','slogan','about'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
        ];
    }

    // Для транзакций, не обязательно.
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    // Настраиваем связь для модели переводов.
    public function getTranslations()
    {
        // Здесь прописываем переводимую таблица
        return $this->hasMany(ProducerTranslation::className(), ['producer_id' => 'id']);
    }
    public static function tableName()
    {
        return 'producer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'tel', 'address', 'account', 'email'], 'required'],
            [['image_1'], 'string'],
            [['compensation'], 'boolean'],
            [['name','name_he', 'tel', 'address','address_he', 'site','account', 'email', 'slogan', 'slogan_he'], 'string', 'max' => 255],
            [['about','about_he'], 'string', 'max' => 2000],
            [['image_file'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название фирмы-производителя на русском',
            'name_he' => 'Название фирмы-производителя на иврите',
            'tel' => 'Телефон',
            'address' => 'Адрес фирмы-производителя на русском',
            'address_he' => 'Адрес фирмы-производителя на иврите',
            'site' => 'Сайт',
            'account' => 'Аккаунт',
            'email' => 'Email',
            'slogan' => 'Слоган на русском',
            'slogan_he' => 'Слоган на иврите',
            'image_1' => 'Картинка лого',
            'compensation' => 'Согласен ли на компенсацию в случае если товар еще не продан, а срок хранения близится к концу',
            'about' => 'Информация о фирме на русском',
            'about_he' => 'Информация о фирме на иврите',
        ];
    }


    public function setImages()
    {
        // Begin Это необходимо для того, чтобы обновлялись картинки
        // и удалялись из базы удаленные картинки
        if (empty($this->image_file)) {
            $this->image_file = [];
        }
        // Если это всего один файл (и больше нельзя), то надо обвернуть в массив.
        if (isset($this->image_file['path'])) {
            $this->image_file = [$this->image_file];
        }

        $countImages = count($this->image_file);
        if ($countImages) {
            foreach (array_values($this->image_file) as $i => $fileData) {
                $this->{'image_' . ($i + 1)} = $fileData['path'];
            }
        }
        for ($i = $countImages; $i < count($this->getImageFields()); $i++) {
            $this->{'image_' . ($i + 1)} = '';
        }
        // End
    }

    public function beforeSave($insert)
    {
        $this->setImages();
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $fields = $this->getImageFields();
        foreach ($fields as $image) {
            if (empty($this->{$image})) {
                continue;
            }
            $params = [
                'path' => $this->{$image},
                'base_url' => '/uploads',
                'delete_url' => Url::to(['product/delete-file', 'path' => $this->{$image}])
            ];
            if (count($fields) == 1) {
                $this->image_file = $params;
            }
            $this->image_file[] = $params;
        }
        $this->name_he = te($this, 'name', 'he-IL');
        $this->address_he = te($this, 'address', 'he-IL');
        $this->slogan_he = te($this, 'slogan', 'he-IL');
        $this->about_he = te($this, 'about', 'he-IL');

        parent::afterFind();
    }

    public function getImageFields()
    {
        //return ['image_file'];
        return ['image_1'];
    }


    public function getProductTypes()
    {
        return $this->hasMany(ProductType::className(), ['producer_id' => 'id']);
    }

    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['producer_id' => 'id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['producer_id' => 'id']);
    }
    public function getDiscounts()
    {
        return $this->hasMany(Discount::className(), ['producer_id' => 'id']);
    }

    public function getProducerList()
    {
        $producers = Producer::find()->all();
         return ArrayHelper::map($producers, 'id', 'name');
    }



    // Сохраняем в таблице переводов заполненные значения по нужным полям перевода.
    public function afterSave($insert, $changedAttributes)
    {
        if (!empty($this->name_he)) {
            $tr = ProducerTranslation::findOne(['producer_id' => $this->id]);
            if (!$tr) {
                $tr = new ProducerTranslation();
                $tr->producer_id = $this->id;
            }
            $tr->name = $this->name_he;
            $tr->address = $this->address_he;
            $tr->slogan = $this->slogan_he;
            $tr->about = $this->about_he;
            $tr->language = 'he-IL';
            $ret = $tr->save();
            //var_dump($ret); die;
        }

    }
}
