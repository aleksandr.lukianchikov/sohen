<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pack_type_translation".
 *
 * @property integer $id
 * @property integer $pack_type_id
 * @property string $language
 * @property string $name
 */
class PackTypeTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pack_type_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pack_type_id'], 'integer'],
            [['language', 'name'], 'required'],
            [['language'], 'string', 'max' => 6],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pack_type_id' => 'Pack Type ID',
            'language' => 'Language',
            'name' => 'Name',
        ];
    }
}
