<?php
namespace app\models;

use yii\helpers\Json;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $shop_name
 * @property string $email
 * @property string $password
 * @property string $tel
 * @property string $delivery_address
 * @property status
 * @property double $money_box
 * @property integer $sms_notification
 * @property string $city
 * @property integer $producer_id
 * @property string $access_rights
 * @property Producer $producer
 * @property string street
 * @property string number_hause
 * @property integer language
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const ROLE_CUSTOMER = 'customer';
    const ROLE_PRODUCER_ADMIN = 'producer_admin';
    const SUPER_ADMIN = 'super_admin';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['producer_id','language'], 'integer'],
            [['money_box'], 'number'],
            [['sms_notification', 'email_notification'], 'integer'],
            [['shop_name','status'], 'string', 'max' => 100],
            [['email', 'password'], 'string', 'max' => 128],
            [['email'], 'unique', 'targetClass' => 'app\models\User', 'targetAttribute' => 'email'],
            [['tel'], 'string', 'max' => 200],
            [['delivery_address'], 'string', 'max' => 255],
            [['city'], 'string', 'max' => 50],
            [['street'], 'string', 'max' => 50],
            [['number_house'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shop_name' => t('Название магазина'),
            'email' => 'Email',
            'password' => t('Пароль'),
            'tel' => t('Телефон'),
            'delivery_address' => t('Адрес доставки'),
            'user_status' => t('Статус юзера'),
            'money_box' => t('Виртуальный кошелек'),
            'sms_notification' => t('Разрешение на получение смс'),
            'email_notification' => t('Разрешение на получение email'),
            'city' => t('Город'),
            'street' => t('Улица'),
            'number_house' => t('Номер дома'),
            'producer_id' => t('Производитель'),
            'access_rights' => t('Права доступа'),
            'status' => t('Статус'),
            'language' => t('Язык коммуникации'),
        ];
    }


    /***
     * Эти поля не относятся к модели Базы данных, они нужны для авторизации
     * @param int|string $id
     * @return static
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }



    public static function findByUsername($email)
    {
        return static::findOne(['email' => $email]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getShopName()
    {
        return $this->shop_name;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getAuthKey()
    {
    }

    public function validateAuthKey($authKey)
    {
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
    }

    public function getProducer()
    {
        return $this->hasOne(Producer::className(), ['id'  => 'producer_id']);
    }

    public function getAccessRightsAsArray()
    {
        if(is_string($this->access_rights) && !empty($this->access_rights)) {
            return Json::decode($this->access_rights);
        }
        return $this->access_rights;
    }

    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password);
    }

    public function setPass($password)
    {
        $this->password = \Yii::$app->getSecurity()->generatePasswordHash($password);
    }
}