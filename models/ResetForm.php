<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ResetForm extends Model
{
    public $email;

    public function rules()
    {
        return [
            [[ 'email'], 'required'],
            [['email'], 'email'],
        ];
    }
    public function attributeLabels()
    {
        return [

            'email' => 'Email',
        ];
    }

}