<?php

namespace app\models;

use creocoder\translateable\TranslateableBehavior;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use\app\models\enums\LocaleEnum;

/**
 * This is the model class for table "product_type".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property integer $producer_id
 */
class ProductType extends \yii\db\ActiveRecord
{
    public $name_he;

    // Добавляем поведение из стороннего расширения из composer установленное
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['name'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
        ];
    }

    // Для транзакций, не обязательно.
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    // Настраиваем связь для модели переводов.
    public function getTranslations()
    {
        // Здесь прописываем переводимую таблица
        return $this->hasMany(ProductTypeTranslation::className(), ['product_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['parent_id', 'producer_id'], 'integer'],
            [['name', 'name_he'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => t('Название типа продукта на русском'),
            'name_he' => t('Название типа продукта на иврите'),
            'parent_id' => t('Родительский тип'),
            'producer_id' => t('Производитель'),
        ];
    }

    public function beforeSave($insert)
    {
        if (!$this->parent_id) {
            $this->parent_id = 0;
        }
        return parent::beforeSave($insert);
    }

    public function getProductTypeList()
    {
        /** @var Producer $producer */
        $producer = Yii::$app->user->identity->producer;
        if (!$producer) {
            return [];
        }
        $productTypes = $producer->getProductTypes()->all();
        if(LocaleEnum::isRTL()){
             return ArrayHelper::map($productTypes, 'id', 'name_he');
         }
         else{
             return ArrayHelper::map($productTypes, 'id', 'name');
         }
    }

    public static function createNestedMargin($items, $marginSign = ' ', $margin = '')
    {
        $result = [];
        foreach ($items as $item) {
            $result[$item['id']] = $margin . $item['label'];
            if (!empty($item['items'])) {
                $result = ArrayHelper::merge($result, self::createNestedMargin($item['items'], $marginSign, $margin . $marginSign));
            }
        }
        return $result;
    }

    public static function getChildren($producerId, $productType, $onlyIds = false)
    {
        $rows = ProductType::findAll(['producer_id' => $producerId]);
        if ($onlyIds) {
            return self::buildTreeIds($producerId, $rows, $productType);
        } else {
            return self::buildTree($producerId, $rows, $productType);
        }
    }

    private static function hasChildren($rows, $id)
    {
        foreach ($rows as $row) {
            if ($row->parent_id == $id)
                return true;
        }
        return false;
    }

    private static function buildTree($producerId, $rows, $parent = 0)
    {
        $result = [];
        foreach ($rows as $row) {
            $item = [];
            if ($row->parent_id == $parent) {
                $item['label'] = te($row, 'name');
                $item['id'] = $row['id'];
                $item['url'] =  ['product/index', 'producer_id' => $producerId, 'product_type_id' => $row['id']];
                if (self::hasChildren($rows, $row['id'])) {
                    $item['items'] = self::buildTree($producerId, $rows, $row['id']);
                }
                $result[] = $item;
            }

        }
        return $result;
    }

    private static function buildTreeIds($producerId, $rows, $parent = 0)
    {
        $result = [];
        foreach ($rows as $row) {
            if ($row['parent_id'] == $parent) {
                $result[] = $row['id'];
                if (self::hasChildren($rows, $row['id'])) {
                    $result = ArrayHelper::merge($result, self::buildTreeIds($producerId, $rows, $row['id']));
                }

            }
        }
        return $result;
    }

    public function getProductTypeListMenu()
    {
        /** @var Producer $producer */
        $producer = Yii::$app->user->identity->producer;
        if (!$producer) {
            return [];
        }
        $items = ProductType::getChildren($producer->id, 0);
        return ProductType::createNestedMargin($items, '  ');
    }

    // Для того чтобы при инициализации модели поля переводов (name_he) проставлялись из другой таблицы-переводов
    public function afterFind()
    {
        $this->name_he = te($this, 'name', 'he-IL');
        parent::afterFind();
    }

    // Сохраняем в таблице переводов заполненные значения по нужным полям перевода.
    public function afterSave($insert, $changedAttributes)
    {
        if (!empty($this->name_he)) {
            $tr = ProductTypeTranslation::findOne(['product_type_id' => $this->id]);
            if (!$tr) {
                $tr = new ProductTypeTranslation();
                $tr->product_type_id = $this->id;
            }
            $tr->name = $this->name_he;
            $tr->language = 'he-IL';
            $tr->save();
        }

    }


    public function getProducer()
    {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }

    public function getProductType()
    {
        return $this->hasOne(ProductType::className(), ['id' => 'parent_id']);
    }
}
