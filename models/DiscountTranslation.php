<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "discount_translation".
 *
 * @property integer $id
 * @property integer $discount_id
 * @property string $language
 * @property string $name
 * @property string $discription
 */
class DiscountTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discount_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['discount_id'], 'integer'],
            [['language', 'name'], 'required'],
            [['language'], 'string', 'max' => 6],
            [['name'], 'string', 'max' => 255],
            [['discription'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discount_id' => 'Discount ID',
            'language' => 'Language',
            'name' => 'Name',
            'discription' => 'Discription',
        ];
    }
}
