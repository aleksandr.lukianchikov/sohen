<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Producer;
use app\models\enums\LocaleEnum;

/**
 * This is the model class for table "compensation".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $producer_id
 * @property integer $product_id
 * @property integer $amount
 * @property integer $percentReturn
 * @property integer $sum
 * @property integer $is_approved
 * @property string $approved_date
 * @property string $end_date
 * @property string $statement_date
 * @property integer $completed
 */
class Compensation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'compensation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'producer_id', 'product_id', 'amount', 'percentReturn', 'sum', 'is_approved', 'completed'], 'integer'],
            [['approved_date','statement_date','end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => t('№ заявки'),
            'user_id' => t('ОТ КОГО'),
            'producer_id' => t('КОМУ'),
            'product_id' => t('Продукт'),
            'end_date' =>t('Срок хранения заканчивается'),
            'amount' => t('Количество упаковок'),
            'percentReturn' => t('Процент возврата'),
            'sum' => t('Сумма'),
            'statement_date' => t('Дата подачи заявки'),
            'is_approved' => t('Подтверждено'),
            'approved_date' => t('Дата подтверждения'),
            'completed' => t('Возврат состоялся'),
        ];
    }


    public function beforeSave($insert)
    {


        if($this->end_date) {
            $this->end_date = (new \DateTime($this->end_date))->format('Y-m-d');
        }


        return parent::beforeSave($insert);
    }

    public function getProducer()
    {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

//    public function getProductList()
//    {
//
//        //$products = $this->getProducer()->one()->getProducts()->all();
//        //return ArrayHelper::map($products, 'id', 'name');
//
//        $producer_id = Yii::$app->user->identity->producer_id;
//        var_dump(count($producer_id)); die;
//        $productList = Product::find()->where(['producer_id' => $producer_id])->all();
//        //var_dump(count($productList)); die;
//        return ArrayHelper::map($productList, 'id', 'name');
//    }

    public function getProductList()
    {
        //$producer = Yii::$app->user->identity->producer;

        $productList = Product::find()->where(['producer_id'=> $this->producer_id])->all();
        if(LocaleEnum::isRTL()){
            return ArrayHelper::map($productList, 'id', 'name_he');
        }
        else{
            return ArrayHelper::map($productList, 'id', 'name');
        }
    }


    public function getUserList()
    {

        $users = User::find()->where(['access_rights' => '["customer"]'])->all();
        return ArrayHelper::map($users, 'id', 'shop_name');
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getProducerList()
    {
        $producerList = Producer::find()->all();
        return ArrayHelper::map($producerList, 'id', 'name');
    }


}
