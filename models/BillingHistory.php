<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "billing_history".
 *
 * @property integer $id
 * @property string $date
 * @property integer $billing_type_id
 * @property integer $user_id
 * @property integer $producer_id
 * @property double $amount
 */
class BillingHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['billing_type_id', 'user_id', 'producer_id'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'billing_type_id' => 'Billing Type ID',
            'user_id' => 'User ID',
            'producer_id' => 'Producer ID',
            'amount' => 'Amount',
        ];
    }
}
