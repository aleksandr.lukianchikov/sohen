<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_type_translation".
 *
 * @property integer $product_type_id
 * @property string $language
 * @property string $name
 */
class ProductTypeTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_type_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_type_id', 'language', 'name'], 'required'],
            [['product_type_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_type_id' => 'Product Type ID',
            'language' => 'Language',
            'name' => 'Name',
        ];
    }
}
