<?php

namespace app\models;
use creocoder\translateable\TranslateableBehavior;
use Yii;

/**
 * This is the model class for table "storage_conditions".
 *
 * @property integer $id
 * @property string $name
 */
class StorageConditions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $name_he;

    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['name'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
        ];
    }

    // Для транзакций, не обязательно.
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    // Настраиваем связь для модели переводов.
    public function getTranslations()
    {
        // Здесь прописываем переводимую таблица
        return $this->hasMany(StorageConditionsTranslation::className(), ['storage_conditions_id' => 'id']);
    }


    public static function tableName()
    {
        return 'storage_conditions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','name_he'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Условия хранения на русском',
            'name_he' => 'Название условия хранения на иврите',
        ];
    }

    // Для того чтобы при инициализации модели поля переводов (name_he) проставлялись из другой таблицы-переводов
    public function afterFind()
    {
        $this->name_he = te($this, 'name', 'he-IL');
        parent::afterFind();
    }

    // Сохраняем в таблице переводов заполненные значения по нужным полям перевода.
    public function afterSave($insert, $changedAttributes)
    {
        if (!empty($this->name_he)) {
            $tr = StorageConditionsTranslation::findOne(['storage_conditions_id' => $this->id]);
            if (!$tr) {
                $tr = new StorageConditionsTranslation();
                $tr->storage_conditions_id = $this->id;
            }
            $tr->name = $this->name_he;
            $tr->language = 'he-IL';
            $tr->save();
        }

    }

}
