<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producer_translation".
 *
 * @property integer $id
 * @property integer $producer_id
 * @property string $language
 * @property string $name
 * @property string $address
 * @property string $slogan
 * @property string $about
 */
class ProducerTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'producer_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['producer_id'], 'integer'],
            [['language', 'name'], 'required'],
            [['about'], 'string'],
            [['language', 'address', 'slogan'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'producer_id' => 'Producer ID',
            'language' => 'Language',
            'name' => 'Name',
            'address' => 'Address',
            'slogan' => 'Slogan',
            'about' => 'About',
        ];
    }
}
