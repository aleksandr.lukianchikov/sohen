<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use\app\models\enums\LocaleEnum;

/**
 * This is the model class for table "product_return".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $product_id
 * @property integer $reason_id
 * @property integer $producer_id
 * @property integer $number
 * @property string statement_date
 * @property string approved_date
 * @property boolean is_approved
 * @property boolean completed
 * @property string $image_1
 * @property string $image_2
 * @property string $image_3
 */
class ProductReturn extends \yii\db\ActiveRecord
{
    /** @var  UploadedFile */
    public $image_file = [];
    public static function tableName()
    {
        return 'product_return';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reason_id','user_id', 'product_id','number'], 'required'],
            [['user_id', 'product_id', 'reason_id', 'producer_id', 'number'], 'integer'],
            [['image_1', 'image_2', 'image_3'], 'string', 'max' => 255],
            [['image_file'], 'safe'],
            [['statement_date','approved_date'], 'safe'],
            [['is_approved','completed'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => t('№ заявки'),
            'user_id' => t('Покупатель'),
            'product_id' => t('Продукт'),
            'reason_id' => t('Причина'),
            'statement_date' => t('Дата подачи заявки'),
            'approved_date' => t('Дата подтверждения'),
            'is_approved' => t('Подтверждено'),
            'completed' => t('Возвращено'),
            'producer_id' => t('Производитель'),
            'number' => t('Количество упаковок'),
            'image_1' => t('Картинка 1'),
            'image_2' => t('Картинка 2'),
            'image_3' => t('Картинка 3'),
        ];
    }

    public function setImages()
    {
        // Begin Это необходимо для того, чтобы обновлялись картинки
        // и удалялись из базы удаленные картинки
        if (empty($this->image_file)) {
            $this->image_file = [];
        }
        // Если это всего один файл (и больше нельзя), то надо обвернуть в массив.
        if (isset($this->image_file['path'])) {
            $this->image_file = [$this->image_file];
        }

        $countImages = count($this->image_file);
        if ($countImages) {
            foreach (array_values($this->image_file) as $i => $fileData) {
                $this->{'image_' . ($i + 1)} = $fileData['path'];
            }
        }
        for ($i = $countImages; $i < count($this->getImageFields()); $i++) {
            $this->{'image_' . ($i + 1)} = '';
        }
        // End
    }

    public function beforeSave($insert)
    {

        $this->setImages();

        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $fields = $this->getImageFields();
        foreach ($fields as $image) {
            if (empty($this->{$image})) {
                continue;
            }
            $params = [
                'path' => $this->{$image},
                'base_url' => '/uploads',
                'delete_url' => Url::to(['product/delete-file', 'path' => $this->{$image}])
            ];
            if (count($fields) == 1) {
                $this->image_file = $params;
            }
            $this->image_file[] = $params;
        }
        parent::afterFind();
    }

    public function getImageFields()
    {
        return ['image_1', 'image_2', 'image_3'];
    }

    public function getReasonList()
    {
        $reasonList = Reason::find()->all();
        if(LocaleEnum::isRTL()){
            return ArrayHelper::map($reasonList, 'id', 'name_he');
        }
        else {
            return ArrayHelper::map($reasonList, 'id', 'name');
        }
    }

    public function getProductList()
    {
        //$producer = Yii::$app->user->identity->producer;

        $productList = Product::find()->where(['producer_id'=> $this->producer_id])->all();
        if(LocaleEnum::isRTL()){
            return ArrayHelper::map($productList, 'id', 'name_he');
        }
        else{
            return ArrayHelper::map($productList, 'id', 'name');
        }
    }


    public function getProducerList()
    {
        $producerList = Producer::find()->all();
        return ArrayHelper::map($producerList, 'id', 'name');
    }

    public function getUserList()
    {

        $users = User::find()->where(['access_rights' => '["customer"]'])->all();
        return ArrayHelper::map($users, 'id', 'shop_name');
    }


    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }


    public function getReason()
    {

        return $this->hasOne(Reason::className(), ['id' => 'reason_id']);

    }

    public function getProducer()
    {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }

    public function isApproved(){
        $result = $this->is_approved;
        return $result;
    }

    public function isCompleted(){
        $result = $this->completed;
        return $result;
    }


}
