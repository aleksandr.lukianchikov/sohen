<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $order_date
 * @property integer $user_id
 * @property string $orderstatus
 * @property integer $producer_id
 * @property double $sum;
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_date'], 'safe'],
            [['user_id'], 'integer'],
            [['orderstatus'], 'string'],
            [['producer_id'], 'integer'],
            [['sum'], 'double'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => t('№ заказа'),
            'order_date' => t('Дата заказа'),
            'user_id' => t('Покупатель'),
            'orderstatus' => t('Статус заказа'),
            'producer_id' => t('Производитель'),
            'sum' => t('Сумма')

        ];
    }

    public function getProducer()
    {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }


    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getProducerList()
    {
        $producerList = Producer::find()->all();
        return ArrayHelper::map($producerList, 'id', 'name');
    }


    public function getUserList()
    {

        $users = User::find()->where(['access_rights' => '["customer"]'])->all();
        return ArrayHelper::map($users, 'id', 'shop_name');
    }



}
