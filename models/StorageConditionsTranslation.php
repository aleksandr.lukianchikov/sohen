<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "storage_conditions_translation".
 *
 * @property integer $id
 * @property integer $storage_conditions_id
 * @property string $language
 * @property string $name
 */
class StorageConditionsTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'storage_conditions_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storage_conditions_id'], 'integer'],
            [['language', 'name'], 'required'],
            [['language'], 'string', 'max' => 6],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'storage_conditions_id' => 'Storage Conditions ID',
            'language' => 'Language',
            'name' => 'Name',
        ];
    }
}
