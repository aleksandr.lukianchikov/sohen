<?php
use \yii\helpers\ArrayHelper;

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true); // false
defined('YII_ENV') or define('YII_ENV', 'dev'); // prod

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');


$config = require(__DIR__ . '/../config/web.php');

$customConfigPath = __DIR__ . '/../.env/web.php';

require_once __DIR__ . '/../helpers/functions.php';

if (file_exists($customConfigPath)) {
    $customConfig = require($customConfigPath);
    $config = ArrayHelper::merge($config, $customConfig);
}

(new yii\web\Application($config))->run();



